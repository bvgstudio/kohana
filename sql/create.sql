SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `bvg` ;
CREATE SCHEMA IF NOT EXISTS `bvg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bvg` ;

-- -----------------------------------------------------
-- Table `bvg`.`AUDIOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`AUDIOS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`AUDIOS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT COMMENT 'ID аудио файла. Уникален только в рамках данной таблицы.' ,
  `file_uri` TEXT NULL COMMENT 'полный путь до файла. Не важно где он находится, на локале или в интернете на другом хосте.' ,
  `title` VARCHAR(250) NULL COMMENT 'название' ,
  `description` TEXT NULL ,
  `teaser` TEXT NULL ,
  `date` DATE NULL COMMENT 'дата события. Например дата, когда лекция была прочитана.' ,
  `venue` VARCHAR(45) NULL COMMENT 'Место где было событие. Город, название местности, в общем месторасположение.' ,
  `podcast` TINYINT(1) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `audio_file_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'Аудио объекты.';


-- -----------------------------------------------------
-- Table `bvg`.`VIDEOS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`VIDEOS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`VIDEOS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `file_uri` TEXT NULL ,
  `title` VARCHAR(250) NULL ,
  `description` TEXT NULL ,
  `teaser` TEXT NULL ,
  `venue` VARCHAR(45) NULL COMMENT 'Место где было событие. Город, название местности, в общем месторасположение.' ,
  `podcast` TINYINT(1) NOT NULL DEFAULT 0 ,
  `date` DATE NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `video_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'Видео объекты';


-- -----------------------------------------------------
-- Table `bvg`.`ARTICLES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`ARTICLES` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`ARTICLES` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `title` TEXT NULL COMMENT 'Заголовок статьи.' ,
  `text` TEXT NULL COMMENT 'Собственно текст статьи.' ,
  `teaser` TEXT NULL ,
  `date` DATE NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `article_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'Статьи, эссе, заметки.';


-- -----------------------------------------------------
-- Table `bvg`.`NEWS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`NEWS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`NEWS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `title` TEXT NULL ,
  `text` TEXT NULL ,
  `name` VARCHAR(45) NULL COMMENT 'подпись под фотографией.' ,
  `teaser` TEXT NULL ,
  `date` DATE NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `news_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'Новостные объекты';


-- -----------------------------------------------------
-- Table `bvg`.`TAGS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`TAGS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`TAGS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT COMMENT 'ID тега (тематики).' ,
  `text` VARCHAR(250) NULL COMMENT 'Текст, само значение тега (тематики).' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `tag_id_UNIQUE` (`id` ASC) )
ENGINE = MyISAM
COMMENT = 'Таблица с самими значениями тегов (тематик, ключевых слов)';


-- -----------------------------------------------------
-- Table `bvg`.`GLOBAL`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`GLOBAL` ;

CREATE TABLE IF NOT EXISTS `bvg`.`GLOBAL` (
  `sequence` DOUBLE NOT NULL COMMENT 'ID continuous numbering in all database to make tags point to various content globally (audio, video, etc...) Не понадобился пока.' ,
  PRIMARY KEY (`sequence`) )
ENGINE = InnoDB
COMMENT = 'Глобальные настройки';


-- -----------------------------------------------------
-- Table `bvg`.`MEDIA_OBJECTS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`MEDIA_OBJECTS` ;

CREATE TABLE IF NOT EXISTS `bvg`.`MEDIA_OBJECTS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `media_object_id` DOUBLE NOT NULL COMMENT 'Это ID описываемого объекта. Например audio_file_id из таблицы AUDIO' ,
  `media_object_type_id` DOUBLE NOT NULL COMMENT 'Тип контента. Связан с MEDIA_CONTENT_TYPE.media_object_type_id' ,
  `alias` VARCHAR(128) NULL COMMENT 'имя объекта для подставления в URL-ы страниц, которые бы кушали хорошо поисковики. Что то вроде /news/vyasa-pudja-2012' ,
  `tag_set_id` DOUBLE NULL ,
  `published` TINYINT(1) NULL,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `MEDIA_CONTENT_ID_UNIQUE` (`id` ASC) )
ENGINE = MyISAM
COMMENT = 'Общая таблица для работы со всем содержимым сайта.\nГлавное в этой таблице это связь тегов с самими объектами (видео, аудио, статьями и т.д.).
Теги это тематика медиа контента. Через эту таблицу, задавая определенные ключевые слова (теги, темы) мы можем найти все что связано с выбранной тематикой: аудио, видео и т.д.';


-- -----------------------------------------------------
-- Table `bvg`.`SEARCH_WORDS`
-- -----------------------------------------------------
CREATE TABLE `SEARCH_WORDS` (
  `word` varchar(64) COLLATE utf8_bin NOT NULL,
  `media_object_id` double NOT NULL,
  `weight` tinyint(1) NOT NULL,
  PRIMARY KEY (`word`,`media_object_id`)
) ENGINE=MyISAM;

-- -----------------------------------------------------
-- Table `bvg`.`MEDIA_OBJECTS_TYPES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`MEDIA_OBJECTS_TYPES` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`MEDIA_OBJECTS_TYPES` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `table_name` VARCHAR(45) NULL ,
  `lang` CHAR(3) NULL ,
  `code` VARCHAR(8) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `MEDIA_OBJECT_TYPE_ID_UNIQUE` (`id` ASC) )
ENGINE = MyISAM
COMMENT = 'Типы меда объектов.\n1. Аудио файлы\n2. Видео\n3. Фотографии\n4. Статьи
Далее можно добавлять
Через эту таблицу идет связь между MEDIA_CONTENT с определенной таблицей типа контента.';


-- -----------------------------------------------------
-- Table `bvg`.`PICTURES`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`PICTURES` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`PICTURES` (
  `id` DOUBLE UNSIGNED NOT NULL AUTO_INCREMENT ,
  `picture_uri` TEXT NULL COMMENT 'путь к картинке' ,
  `title` VARCHAR(45) NULL COMMENT 'подпись под фотографией.' ,
  `date` DATE NULL ,
  `teaser` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `picture_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'Фотографии';


-- -----------------------------------------------------
-- Table `bvg`.`DISKS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`DISKS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`DISKS` (
  `id` DOUBLE NOT NULL ,
  `description` TEXT NULL ,
  `teaser` TEXT NULL ,
  `date` DATE NULL ,
  `cover_img_uri` TEXT NULL ,
  `title` VARCHAR(250) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
COMMENT = 'Описание выпускаемых дисков. Лекции или видео могут входить в определенный диск.';


-- -----------------------------------------------------
-- Table `bvg`.`DISKS_MEDIA_OBJECTS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`DISKS_MEDIA_OBJECTS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`DISKS_MEDIA_OBJECTS` (
  `id` DOUBLE UNSIGNED NOT NULL AUTO_INCREMENT ,
  `disk_id` DOUBLE NOT NULL ,
  `media_object_id` DOUBLE NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `disk_item_id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB
COMMENT = 'В таблице содержится разбивка файлов по дискам. Т.е. тут указано на каком диске какой файл находится. В разных дисках может быть один файл.';


-- -----------------------------------------------------
-- Table `bvg`.`TAG_SETS_TAGS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`TAG_SETS_TAGS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`TAG_SETS_TAGS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `tag_id` DOUBLE NOT NULL COMMENT 'ID тэга или ключевого слова из таблицы TAG' ,
  `tag_set_id` DOUBLE NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
COMMENT = 'У одного медиа объекта может быть много тэгов (тематик). В этой таблице хранятся наборы тегов. Можно один набор тэгов присваивать нескольким медиа объектам.';


-- -----------------------------------------------------
-- Table `bvg`.`TAG_SETS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`TAG_SETS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`TAG_SETS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bvg`.`NEWS_MEDIA_OBJECTS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`NEWS_MEDIA_OBJECTS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`NEWS_MEDIA_OBJECTS` (
  `id` DOUBLE NOT NULL ,
  `news_id` DOUBLE NOT NULL ,
  `media_object_id` DOUBLE NOT NULL ,
  `major` TINYINT(1) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bvg`.`BOOKS`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bvg`.`BOOKS` ;

CREATE  TABLE IF NOT EXISTS `bvg`.`BOOKS` (
  `id` DOUBLE NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(250) NULL ,
  `description` TEXT NULL ,
  `teaser` TEXT NULL ,
  `date` DATE NULL ,
  `cover_img_uri` TEXT NULL ,
  `download_uri` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


CREATE  TABLE `bvgstudio`.`venues` (
  `id` DOUBLE NOT NULL ,
  `name` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;

USE `bvg` ;

-- -----------------------------------------------------
-- Placeholder table for view `bvg`.`content_search`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bvg`.`content_search` (`name` INT, `teaser` INT, `title` INT, `date` INT);

-- -----------------------------------------------------
-- function nextval
-- -----------------------------------------------------

USE `bvg`;
DROP function IF EXISTS `bvg`.`nextval`;

DELIMITER $$
USE `bvg`$$
CREATE function `bvg`.`nextval` ()
returns double
BEGIN

declare v_nextval double;

select sequence + 1 into v_nextval from GLOBAL;
update GLOBAL set sequence = v_nextval;
return v_nextval;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `bvg`.`content_search`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `bvg`.`content_search` ;
DROP TABLE IF EXISTS `bvg`.`content_search`;
USE `bvg`;
CREATE OR REPLACE VIEW `bvg`.`content_search` AS


select mo.*,

case	when	mot.code = 'AUDIO'		then au.teaser
		when	mot.code = 'VIDEO'		then v.teaser
		when	mot.code = 'BOOK'		then b.teaser
		when	mot.code = 'DISK'		then d.teaser
		when	mot.code = 'PICTURE'	then p.teaser
		when	mot.code = 'NEWS'		then n.teaser
		when	mot.code = 'ARTICLE'	then a.teaser	end as teaser,


case	when	mot.code = 'AUDIO'		then au.title
		when	mot.code = 'VIDEO'		then v.title
		when	mot.code = 'BOOK'		then b.title
		when	mot.code = 'DISK'		then d.title
		when	mot.code = 'PICTURE'	then p.title
		when	mot.code = 'NEWS'		then n.title
		when	mot.code = 'ARTICLE'	then a.title	end as title,

case	when	mot.code = 'AUDIO'		then au.date
		when	mot.code = 'VIDEO'		then v.date
		when	mot.code = 'BOOK'		then b.date
		when	mot.code = 'DISK'		then d.date
		when	mot.code = 'PICTURE'	then p.date
		when	mot.code = 'NEWS'		then n.date
		when	mot.code = 'ARTICLE'	then a.date		end as date

from media_objects mo

left join articles a on mo.media_object_id = a.id
left join audios au on mo.media_object_id = au.id
left join books b  on mo.media_object_id = b.id
left join disks d on mo.media_object_id = d.id
left join news n on mo.media_object_id = n.id
left join pictures p on mo.media_object_id = p.id
left join videos v on mo.media_object_id = v.id
join  media_objects_types mot on mot.id = mo.media_object_type_id;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bvg`.`MEDIA_OBJECTS_TYPES`
-- -----------------------------------------------------
START TRANSACTION;
USE `bvg`;
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (1, 'Аудио', 'AUDIOS', 'ru', 'AUDIO');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (2, 'Видео', 'VIDEOS', 'ru', 'VIDEO');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (3, 'Книги', 'BOOKS', 'ru', 'BOOK');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (4, 'Диск', 'DISKS', 'ru', 'DISK');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (5, 'Фотографии', 'PICTURES', 'ru', 'PICTURE');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (6, 'Новости', 'NEWS', 'ru', 'NEWS');
INSERT INTO `bvg`.`MEDIA_OBJECTS_TYPES` (`id`, `name`, `table_name`, `lang`, `code`) VALUES (7, 'Статьи', 'ARTICLES', 'ru', 'ARTICLE');

COMMIT;
