/*
select concat('insert into tags (tag) values (\'', value, '\');')
from sys_values
*/


insert into tags (tag) values ('Власть');
insert into tags (tag) values ('Города и районы');
insert into tags (tag) values ('Государственные предприятия и фонды');
insert into tags (tag) values ('Информационные ресурсы');
insert into tags (tag) values ('Культура');
insert into tags (tag) values ('Образование');
insert into tags (tag) values ('Общественные организации');
insert into tags (tag) values ('Спорт');
insert into tags (tag) values ('Средства массовой информации');
insert into tags (tag) values ('1. Каталог дисков');
insert into tags (tag) values ('2. Новое на сайте');
insert into tags (tag) values ('3. Лекции');
insert into tags (tag) values ('4. Статьи');
insert into tags (tag) values ('5. Фотоархив');
insert into tags (tag) values ('6. Госвами Букс');
insert into tags (tag) values ('Архив');
insert into tags (tag) values ('Вопросы-ответы');
insert into tags (tag) values ('Даршан');
insert into tags (tag) values ('Дневник');
insert into tags (tag) values ('Истории');
insert into tags (tag) values ('Лекции');
insert into tags (tag) values ('Ачарьи');
insert into tags (tag) values ('Бхагавад-гита');
insert into tags (tag) values ('Гаудия Вайшнавская Сампрадая');
insert into tags (tag) values ('Даршан');
insert into tags (tag) values ('Дхама');
insert into tags (tag) values ('Живые Эфиры на Кришналоке');
insert into tags (tag) values ('Йога, Психология и Аюрведа');
insert into tags (tag) values ('Манах Шикша');
insert into tags (tag) values ('Махабхарата');
insert into tags (tag) values ('Музыка');
insert into tags (tag) values ('Обсуждения и ответы на вопросы');
insert into tags (tag) values ('Организация ISKCON');
insert into tags (tag) values ('Основы');
insert into tags (tag) values ('Праздничные лекции');
insert into tags (tag) values ('Пураны');
insert into tags (tag) values ('Ретрит');
insert into tags (tag) values ('Фестивали');
insert into tags (tag) values ('Чайтанья Чаритамрита');
insert into tags (tag) values ('Шикшаштака');
insert into tags (tag) values ('Шримад Бхагаватам');
