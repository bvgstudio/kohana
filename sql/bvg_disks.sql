/*
select concat
('insert into media (type, alias, title, publisher, file_uri, size) values (\'collection\', \'disc_', 
LPAD(disk_number, 3, '0') , 
'\', \'Диск № ', 
LPAD(disk_number, 3, '0'), 
'\', \'Студия БВГ\', \'', LPAD(disk_number, 3, '0'), '.iso\', ', sum(size), ');'
)
from user_archived_tracks
group by disk_number

*/

select ID into pub_id from publishers where name = 'Студия БВГ';

insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_001', 'Диск № 001', pub_id, '001.iso', 731655102);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_002', 'Диск № 002', pub_id, '002.iso', 680929060);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_003', 'Диск № 003', pub_id, '003.iso', 680628299);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_004', 'Диск № 004', pub_id, '004.iso', 680053006);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_005', 'Диск № 005', pub_id, '005.iso', 683394494);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_006', 'Диск № 006', pub_id, '006.iso', 684100688);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_007', 'Диск № 007', pub_id, '007.iso', 675394310);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_008', 'Диск № 008', pub_id, '008.iso', 682104794);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_009', 'Диск № 009', pub_id, '009.iso', 686239307);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_010', 'Диск № 010', pub_id, '010.iso', 686882028);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_011', 'Диск № 011', pub_id, '011.iso', 681327092);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_012', 'Диск № 012', pub_id, '012.iso', 683664470);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_013', 'Диск № 013', pub_id, '013.iso', 673777819);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_014', 'Диск № 014', pub_id, '014.iso', 681969021);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_015', 'Диск № 015', pub_id, '015.iso', 676384069);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_016', 'Диск № 016', pub_id, '016.iso', 675742646);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_017', 'Диск № 017', pub_id, '017.iso', 664837240);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_018', 'Диск № 018', pub_id, '018.iso', 686188981);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_019', 'Диск № 019', pub_id, '019.iso', 733822901);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_020', 'Диск № 020', pub_id, '020.iso', 685936748);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_021', 'Диск № 021', pub_id, '021.iso', 726789994);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_022', 'Диск № 022', pub_id, '022.iso', 660537021);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_023', 'Диск № 023', pub_id, '023.iso', 673667565);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_024', 'Диск № 024', pub_id, '024.iso', 683199158);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_025', 'Диск № 025', pub_id, '025.iso', 731817075);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_026', 'Диск № 026', pub_id, '026.iso', 685117275);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_027', 'Диск № 027', pub_id, '027.iso', 732224972);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_028', 'Диск № 028', pub_id, '028.iso', 684766268);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_029', 'Диск № 029', pub_id, '029.iso', 675434808);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_030', 'Диск № 030', pub_id, '030.iso', 734068511);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_031', 'Диск № 031', pub_id, '031.iso', 735801568);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_032', 'Диск № 032', pub_id, '032.iso', 720528938);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_033', 'Диск № 033', pub_id, '033.iso', 709574156);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_034', 'Диск № 034', pub_id, '034.iso', 733360646);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_035', 'Диск № 035', pub_id, '035.iso', 663702232);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_036', 'Диск № 036', pub_id, '036.iso', 719433967);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_037', 'Диск № 037', pub_id, '037.iso', 719741759);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_038', 'Диск № 038', pub_id, '038.iso', 729582665);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_039', 'Диск № 039', pub_id, '039.iso', 709722531);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_040', 'Диск № 040', pub_id, '040.iso', 724301390);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_041', 'Диск № 041', pub_id, '041.iso', 726573788);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_042', 'Диск № 042', pub_id, '042.iso', 727516832);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_043', 'Диск № 043', pub_id, '043.iso', 728390656);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_044', 'Диск № 044', pub_id, '044.iso', 724770712);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_045', 'Диск № 045', pub_id, '045.iso', 735556566);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_046', 'Диск № 046', pub_id, '046.iso', 727242733);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_047', 'Диск № 047', pub_id, '047.iso', 718189491);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_048', 'Диск № 048', pub_id, '048.iso', 734883129);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_049', 'Диск № 049', pub_id, '049.iso', 731514087);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_050', 'Диск № 050', pub_id, '050.iso', 709562619);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_051', 'Диск № 051', pub_id, '051.iso', 726473033);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_052', 'Диск № 052', pub_id, '052.iso', 724901792);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_054', 'Диск № 054', pub_id, '054.iso', 725363893);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_055', 'Диск № 055', pub_id, '055.iso', 715044686);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_056', 'Диск № 056', pub_id, '056.iso', 703642608);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_057', 'Диск № 057', pub_id, '057.iso', 660842338);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_058', 'Диск № 058', pub_id, '058.iso', 725399378);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_059', 'Диск № 059', pub_id, '059.iso', 708833885);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_060', 'Диск № 060', pub_id, '060.iso', 702999184);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_061', 'Диск № 061', pub_id, '061.iso', 708200999);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_062', 'Диск № 062', pub_id, '062.iso', 632710631);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_063', 'Диск № 063', pub_id, '063.iso', 606253680);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_064', 'Диск № 064', pub_id, '064.iso', 500621750);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_065', 'Диск № 065', pub_id, '065.iso', 728613754);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_066', 'Диск № 066', pub_id, '066.iso', 667947008);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_067', 'Диск № 067', pub_id, '067.iso', 728907061);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_068', 'Диск № 068', pub_id, '068.iso', 677146784);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_069', 'Диск № 069', pub_id, '069.iso', 730456016);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_070', 'Диск № 070', pub_id, '070.iso', 701547554);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_071', 'Диск № 071', pub_id, '071.iso', 676696971);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_072', 'Диск № 072', pub_id, '072.iso', 693651497);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_073', 'Диск № 073', pub_id, '073.iso', 726219061);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_074', 'Диск № 074', pub_id, '074.iso', 625739386);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_075', 'Диск № 075', pub_id, '075.iso', 643304907);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_076', 'Диск № 076', pub_id, '076.iso', 507160162);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_077', 'Диск № 077', pub_id, '077.iso', 729083087);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_078', 'Диск № 078', pub_id, '078.iso', 731560173);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_079', 'Диск № 079', pub_id, '079.iso', 603224189);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_080', 'Диск № 080', pub_id, '080.iso', 655787103);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_081', 'Диск № 081', pub_id, '081.iso', 643764448);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_082', 'Диск № 082', pub_id, '082.iso', 716828537);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_083', 'Диск № 083', pub_id, '083.iso', 667136084);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_084', 'Диск № 084', pub_id, '084.iso', 704263247);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_085', 'Диск № 085', pub_id, '085.iso', 620439655);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_086', 'Диск № 086', pub_id, '086.iso', 506560147);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_087', 'Диск № 087', pub_id, '087.iso', 614394829);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_088', 'Диск № 088', pub_id, '088.iso', 687597442);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_089', 'Диск № 089', pub_id, '089.iso', 731843384);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_090', 'Диск № 090', pub_id, '090.iso', 655228752);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_091', 'Диск № 091', pub_id, '091.iso', 623817177);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_092', 'Диск № 092', pub_id, '092.iso', 783327234);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_093', 'Диск № 093', pub_id, '093.iso', 694080399);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_094', 'Диск № 094', pub_id, '094.iso', 614708079);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_095', 'Диск № 095', pub_id, '095.iso', 661069572);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_096', 'Диск № 096', pub_id, '096.iso', 646407254);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_097', 'Диск № 097', pub_id, '097.iso', 709857950);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_098', 'Диск № 098', pub_id, '098.iso', 673172627);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_099', 'Диск № 099', pub_id, '099.iso', 697525366);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_100', 'Диск № 100', pub_id, '100.iso', 681225050);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_101', 'Диск № 101', pub_id, '101.iso', 719555567);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_102', 'Диск № 102', pub_id, '102.iso', 726001471);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_103', 'Диск № 103', pub_id, '103.iso', 726733550);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_104', 'Диск № 104', pub_id, '104.iso', 692751083);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_105', 'Диск № 105', pub_id, '105.iso', 706486264);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_106', 'Диск № 106', pub_id, '106.iso', 678350917);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_107', 'Диск № 107', pub_id, '107.iso', 646513614);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_108', 'Диск № 108', pub_id, '108.iso', 753088919);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_109', 'Диск № 109', pub_id, '109.iso', 658292846);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_110', 'Диск № 110', pub_id, '110.iso', 712282472);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_111', 'Диск № 111', pub_id, '111.iso', 731340813);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_112', 'Диск № 112', pub_id, '112.iso', 608404987);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_113', 'Диск № 113', pub_id, '113.iso', 718347879);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_114', 'Диск № 114', pub_id, '114.iso', 651106246);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_115', 'Диск № 115', pub_id, '115.iso', 457781672);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_116', 'Диск № 116', pub_id, '116.iso', 702422691);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_117', 'Диск № 117', pub_id, '117.iso', 669981041);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_118', 'Диск № 118', pub_id, '118.iso', 521134245);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_119', 'Диск № 119', pub_id, '119.iso', 478460836);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_120', 'Диск № 120', pub_id, '120.iso', 684006996);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_121', 'Диск № 121', pub_id, '121.iso', 630919748);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_122', 'Диск № 122', pub_id, '122.iso', 602383017);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_123', 'Диск № 123', pub_id, '123.iso', 703232549);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_124', 'Диск № 124', pub_id, '124.iso', 521181001);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_125', 'Диск № 125', pub_id, '125.iso', 630965272);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_126', 'Диск № 126', pub_id, '126.iso', 631202396);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_127', 'Диск № 127', pub_id, '127.iso', 659929284);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_128', 'Диск № 128', pub_id, '128.iso', 509144947);
insert into media (type, alias_uri, title, publisher_id, file_uri, size) values ('collection', 'disc_129', 'Диск № 129', pub_id, '129.iso', 655127446);




