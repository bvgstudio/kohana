﻿var map = null;

$('#rep').click(function(){
    $('#filial').toggle({
        complete:function (){
            google.maps.event.trigger(map, 'resize');
        },
        duration:'slow'
    });
})

function initMap() {
  map = new google.maps.Map (document.getElementById('map'), {
      zoom: 3,
      center: {lat: 76.4584001, lng: 20.0975261}
  });
  setMarkers(map);
}


// Adds markers to the map.
// http://stackoverflow.com/questions/7044587/adding-multiple-markers-with-infowindows-google-maps-api
// does not work as described here https://developers.google.com/maps/documentation/javascript/examples/event-closure
function setMarkers(map) {

    var wnd = new google.maps.InfoWindow();

    for (var i = 0; i < representatives.length; i++) {
        var representative = representatives[i];

        var location = representative[0].split(",");
	// create marker
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(location[0], location[1]),
            title: representative[1],
        });

	// add info window to marker
        google.maps.event.addListener(marker, 'click', function (marker, content, infowindow) {
                return function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            }( marker, representative[1], wnd ) );
    } // for (var i = 0; i < retailers.length; i++)
}

