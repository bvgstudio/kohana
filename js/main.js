jQuery(document).ready(function($) {		
        onUserPanelLoaded();
    
	$('.slider-images').noosSlider({autoAnimate:0});
	
	$('.filters-container .nav .categories').on('click', 'a', function(e){
                // Нажатие кнопки типа контента
		e.preventDefault();
                
                $(this).toggleClass('active');
                
                $('.filters-container').trigger('filters:change');
	});

	$('.filters-container').each(function(i, fc) {
		var $fc = $(fc);
		$('.add-filter-list', fc).each(function(i2, fl) {
                        // Форимруем списки добавления критериев поиска
			var w = 0;
			var els = $('a.tag', fl).size();
                        
			$(fl).append('<div class="scroll"><ul><li></li></ul></div>');
                        
			$('a.tag', fl).each(function(i3, tag) {
				$(tag).attr('rel',i3);
				if(!$(tag).hasClass('active')){
					$('ul li:last-child', fl).append(tag);
				}
				if($('ul li:last-child a', fl).size() === 4 && i3 !== els-1) {
					$('ul', fl).append('<li></li>');
				}
			});
                        
			$('ul li', fl).each(function(i3, li) {
				w += $(li).outerWidth(true) + 1;
			});
                        
			$('ul', fl).css('width', w + 'px');
		}).on('click', 'a.tag', function(e){
			e.preventDefault();
                        
			$(this).clone().append('<b></b>').insertBefore($(this).closest('.add-filter-list').prev().find('.add'));
			$(this).addClass('selected');
                        
			updateList.call($(this).closest('.add-filter-list').get(0));
                        
                        $('.filters-container').trigger('filters:change');
		});
		
		$fc.on('click', '.filter a.tag', function(e){
                        // Обработчик клика на выбранном критерии. Убираем его из списка выбранных, обновляем список для выбора.
			e.preventDefault();
                        
			var $c = $(this).closest('.filter').next();
                        
			$('a[rel='+$(this).attr('rel')+']', $c).removeClass('selected');
                        
			$(this).remove();
			
			updateList.call($c.get(0));
                        
                        $('.filters-container').trigger('filters:change');
		}).on('click', '.filter a.add', function(e){
			e.preventDefault();
                        
			$('.add-filter-list.show', fc).removeClass('show');
                        
			if(!$(this).hasClass('active')){
                                $('.filter a.add.active', fc).removeClass('active');
                                
				$(this).closest('.filter').next().addClass('show');
				$(this).addClass('active');
                                
				if($(this).position().left === 0) {
					$(this).closest('.filter').next().addClass('remove-ltr');
				} else {
					$(this).closest('.filter').next().removeClass('remove-ltr');
				}
			}
                        else {
                            $('.add-filter-list.show').removeClass('show');
                            $('.filter .add.active').removeClass('active');
                        }
		}).on('keyup', '.add-filter-list input[name=search]', function(){
			var value = $(this).val();
			var r = new RegExp(value, 'i');
			var $context = $(this).closest('.add-filter-list');
			if(value){
				$('a.tag', $context).each(function(index, element) {
					if(r.test($(this).text())){
						$(this).removeClass('hide');
					} else {
						$(this).addClass('hide');
					}
				});
			} else {
				$('a.tag', $context).removeClass('hide');
			}
			updateList.call($context.get(0));
		});
		
		$('.scroll').mCustomScrollbar({
			axis:'x'
		});
	});

	var date = new Date();
	$('.slider').slider({
		range: true,
		min: 1990,
		max: date.getUTCFullYear(),
		values: [ 1990, date.getUTCFullYear() ],
		change: function( event, ui ) {
			$('.ui-slider-handle', this).eq(0).text(ui.values[ 0 ]);
			$('.ui-slider-handle', this).eq(1).text(ui.values[ 1 ]);
		}, 
		slide: function( event, ui ) {
			$('.ui-slider-handle', this).eq(0).text(ui.values[ 0 ]);
			$('.ui-slider-handle', this).eq(1).text(ui.values[ 1 ]);
		},
		create: function() {
			$('.ui-slider-handle', this).eq(0).text(1990);
			$('.ui-slider-handle', this).eq(1).text(date.getUTCFullYear());
		},
                stop: function() {
			$('.filters-container').trigger('filters:change');
		}
	});
	
	$('.filter-date').each(function() {
                var $dateFilterRoot = $(this);
                
		var w = 0;
		$('.l, .r, .m', $dateFilterRoot).each(function() {
			$(this).css({'width':$(this).outerWidth()});
			w += $(this).outerWidth();
		});
                
                $('.r, .m', $dateFilterRoot).each(function() {
			$(this).css({'width':$(this).outerWidth()});
			w += $(this).outerWidth();
		});
                
                $('.exact-dates .confirm', $dateFilterRoot).on('click', function(){
			$('.filters-container').trigger('filters:change');
		});
                
		$('.w', $dateFilterRoot).css({'width':w});
                
                // Переключает режим ввода даты между слайдером и вводом тоных дат.
		$('.m', $dateFilterRoot).on('click', function(){
			if($(this).parent().position().left === 0 ) {
                                //  Показываем инпуты для ввода точных дат
				$(this).parent().css({'left':$(this).parent().find('.l').width()*(-1)});
                                
                                $('.exact-dates', $dateFilterRoot).addClass('active');
			} else {
                                // Показываем слайдер
				$(this).parent().css({'left':0});
                                
                                $('.exact-dates', $dateFilterRoot).removeClass('active');
			}
		});
	});
        
        $('#text-search input').on('keyup', function(e) {
            if (e.key === 'Enter' || e.charCode === 13 || e.keyCode === 13) {
                $('.filters-container').trigger('filters:change');
            }
        });
	
        var searchVars = getUrlVars();
        if(searchVars.page) delete searchVars.page;

        if(!$.isEmptyObject(searchVars)) {
           renderFiltersState(searchVars); 
        }
        
        $('.filters-container').on('filters:change', onFiltersStateChanged);
        
	$('.filters-container .filters a').each(function() {
		var id = $(this).attr('href');
		var $f = $(id);
		if($(this).hasClass('active')){
			$f.css({ display: 'block' });
		} else {
			$f.css({ display: 'none' });
		}
	});
	$('.filters-container .filters a').on('click', function(e){
                e.preventDefault();
                // Обработчик для кнопок критериев фильтрации
		var id = $(this).attr('href');
		var $f = $(id);                
                $(this).toggleClass('active');
                
		if($(this).hasClass('active')){
                        // Если мы включили кнопку, отображаем блок с выбором критериев
			$f.css({'display':'block'});
		} else {
                        // Если мы выключили кнопку, убираем список критериев, возвращаем список для их выбора в исходное состояние
			$f.css({'display':'none'});
			if($('a.tag', $f).size()) {
				$('a.tag', $f).each(function() {
					$('a[rel='+$(this).attr('rel')+']', $f.next() ).removeClass('selected');
					$(this).remove();
				});
				updateList.call($f.next().get(0));
			} else if($f.hasClass('filter-date')) {
				$('.slider').slider('values', 0, 1990 );
				$('.slider').slider('values', 1, date.getUTCFullYear() );
			}
                        
                        $('.filters-container').trigger('filters:change');
		}
	});
	
	$('.filters-container .filter .close').on('click', function(e){
                // Обработчик для кнопки сброса фильтра (красный крестик)
		e.preventDefault();
                
		var $f = $(this).closest('.filter');
		var id = $f.attr('id');
                
                // Убираем подсветку с кнопки, включающей фильтрацию по данному критерию
		$('.filters-container .filters a[href=#'+id+']').removeClass('active');
                
                // Убираем список критериев, возвращаем список для их выбора в исходное состояние
		if($('a.tag', $f).size()) {
			$('a.tag', $f).each(function() {
				$('a[rel='+$(this).attr('rel')+']', $f.next()).removeClass('selected');
				$(this).remove();
			});
                        
			updateList.call($f.next().get(0));
		} else if($f.hasClass('filter-date')) {
			$('.slider').slider('values', 0, 1990 );
			$('.slider').slider('values', 1, date.getUTCFullYear() );
		}
		$f.css({'display':'none'});
                
                $('.filters-container').trigger('filters:change');
	});
	
	$('.filters-container .sort').each(function() {
		$('.order', this).each(function() {
			var $a = $('a.active', this);
			var $bg = $('.active-bg', this);
                        
			$bg.css({
				left: $a.position().left + parseInt($a.css('margin-left')),
				width: $a.outerWidth()
			});
		}).on('click', 'a', function(e){
			e.preventDefault();
                        
                        // Обрабтываем клик на элементах, отвечающих за сортировку
			var $p = $(this).closest('.order');
                        
			var $bg = $('.active-bg', $p);
                        
			$('a.active', $p).removeClass('active');
			$(this).addClass('active');
                        
			$bg.css({
				left:$(this).position().left + parseInt($(this).css('margin-left')),
				width:$(this).outerWidth()
			});
                        
                        $('.filters-container').trigger('filters:change');
		});
	});
        
        onContentLoaded();
        
        $('.content.main').on('click', '.pagination a', function(e){
            e.preventDefault();
            History.pushState('ajax', $('.content.main').hasClass('news-list') ? 'Новости': 'Поиск по каталогу', $(this).prop('href'));
        });
	
	$(document).on('mousedown', function(e){
		if($('.add-filter-list.show').size()){
			var f = true;
			if($(e.target).closest('.add-filter-list').size()) f = false;
			if($(e.target).closest('.add').size()) f = false;
			if($(e.target).hasClass('add')) f = false;
			if(!$(e.target).hasClass('close')) {
				if($(e.target).closest('.filter').size()){
					if($(e.target).closest('.filter').next().hasClass('show')) f = false;
				}
			}
			if(f) {
				$('.add-filter-list.show').removeClass('show');
				$('.add.active').removeClass('active');
			}
		}
	});
        
        /*  Oбработчик изменения состояния страницы */
        History.Adapter.bind(window ,'statechange', function() { 
            var state = History.getState();

            var $content = $('.content.main'); 
            toggleLoader($content);
            /* Получаем URL нового состояния. Это URL, который мы передали
            в .pushState() */
            var url = state.url + (state.url.indexOf('?') > 0 ? '&' : '?') + 'ajax=1';

            $content.load(url, function () {
                onContentLoaded();
                toggleLoader();
            });
        });
});

function renderFiltersState(searchVars) {
    if(searchVars.q) {
        $('.filters-container #text-search [name=q]').val(searchVars.q);
        $('.filters-container .nav .filters [href=#text-search]').addClass('active');
    }
        
    if(searchVars.content_types){
       var contentTypesArr = searchVars.content_types.split(',');
       $.each(contentTypesArr, function(i, contentType){
           $('.filters-container .nav .categories a[href=#' + contentType + ']').addClass('active');
       });
    }
    
    if(searchVars.tags){
        var tagsArr =  searchVars.tags.split(',');
        $.each(tagsArr, function(i, tag){
            renderSearchFilterItem('tags', tag);
        });
        
        $('.filters-container .nav .filters a[href=#tags]').addClass('active');

        updateList.call($('.filters-container #tags').next().get(0));
    }
    
    if(searchVars.venues){
       var venuesArr =  searchVars.venues.split(',');
       $.each(venuesArr, function(i, venue){
           renderSearchFilterItem('venues', venue);
       });
       
       $('.filters-container .nav .filters a[href=#venues]').addClass('active');

       updateList.call($('.filters-container #venues').next().get(0));
    }
    
    if(searchVars.authors){
       var authorsArr =  searchVars.authors.split(',');
       $.each(authorsArr, function(i, author){
           renderSearchFilterItem('authors', author);
       });
       
       $('.filters-container .nav .filters a[href=#authors]').addClass('active');

       updateList.call($('.filters-container #authors').next().get(0));
    }
    
    if(searchVars.from_date) {
        if(searchVars.from_date.indexOf('.') > 0) {
            $('.filter-date .m').click();
            jQuery('.filters-container #date [name=from]').datepicker('setDate', searchVars.from_date);
            jQuery('.filters-container #date [name=to]').datepicker('setDate', searchVars.to_date || new Date());
            
            jQuery('.filters-container #date .slider').slider('values', [
                                                                         jQuery('.filters-container #date [name=from]').datepicker('getDate').getUTCFullYear(),
                                                                         jQuery('.filters-container #date [name=to]').datepicker('getDate').getUTCFullYear()
                                                                        ]);
        }
        else {
            jQuery('.filters-container #date .slider').slider('values', [ searchVars.from_date, (searchVars.to_date || new Date().getUTCFullYear())]);
        }
        
        $('.filters-container .nav .filters [href=#date]').addClass('active');
    }
    
    if(searchVars.order_by) {
        $('.filters-container .sort .order a').removeClass('active');
        
        if(searchVars.order_by === 'occurrence_date') {
            $('.filters-container .sort .order .by-date').addClass('active');
            // Сортировка по умолчанию для даты - от самого свежего к самому старому, поэтому все наоборот
            $(searchVars.order_dir === 'ASC' ? '.down' : '.up' ,'.filters-container .sort .order').addClass('active');
        }
        else {
            $('.filters-container .sort .order .by-alphabet').addClass('active');
            // Сортировка по умолчанию для даты - от самого свежего к самому старому, поэтому все наоборот
            $(searchVars.order_dir === 'DESC' ? '.down' : '.up' ,'.filters-container .sort .order').addClass('active');
        }
    }
}

function renderSearchFilterItem(type, value) {
    var $container = $('#' + type);
    
    $container.next().find('a.tag').each(function() {
        if($('span', this).text() === value) {
            $(this).clone().append('<b></b>').insertBefore($container.find('.add'));
            $(this).addClass('selected');
        }
    });
}

function onFiltersStateChanged() {
    var searchCriteria = {};
    if($('.filters-container .nav .categories .active').length > 0 &&
       $('.filters-container .nav .categories .active').length !== $('.filters-container .nav .categories > a').length ) {
        var types = [];
        $('.filters-container .nav .categories .active').each(function() {
            types.push($(this).attr('href').replace('#', ''));
        });
        
        searchCriteria.content_types = types.join(',');
    }
    
    $('.filters-container .nav .filters .active').each(function() {
        var href = $(this).attr('href');
        // Обрабатываем фильтры даты и полнотекстовый поиск ниже 
        if(href !== '#date' && href !== '#text-search') {
            var items = [];
            $('.tag span', '.filters-container ' + href).each(function() {
                items.push($.trim($(this).text()));
            });
            
            if(items.length > 0) {
                searchCriteria[href.replace('#', '')] = items.join(',');
            }
        }
    });
    
    if($('.filters-container .nav .filters [href=#date]').hasClass('active')) {
        if($('.filters-container #date .exact-dates').hasClass('active')) {
            searchCriteria.from_date = $('.filters-container #date [name=from]').val();
            searchCriteria.to_date = $('.filters-container #date [name=to]').val();
        }
        else {
            searchCriteria.from_date = jQuery('.filters-container #date .slider').slider('values', 0);
            searchCriteria.to_date = jQuery('.filters-container #date .slider').slider('values', 1);
        }
    }
    
    if($('.filters-container .nav .filters [href=#text-search]').hasClass('active') &&
       $('.filters-container #text-search [name=q]').val()) {
        searchCriteria.q = $('.filters-container #text-search [name=q]').val();
    }
    
    if($('.sort .order .by-date').hasClass('active')) {
        searchCriteria.order_by = 'occurrence_date';
        // Сортировка по умолчанию для даты - от самого свежего к самому старому, поэтому все наоборот
        searchCriteria.order_dir = $('.sort .order .up').hasClass('active') ? 'DESC' : 'ASC';
    }
    else {
        searchCriteria.order_by = 'title';
        searchCriteria.order_dir = $('.sort .order .up').hasClass('active') ? 'ASC' : 'DESC';
    }
    
    var queryString = $.param(searchCriteria);
    
    var url = [location.protocol, '//', location.host, '/catalogue/'].join('') + (queryString ? '?' + queryString : '');
    
    if (History.enabled)
	History.pushState('ajax', 'Поиск по каталогу', url);
    else location.href = url;
}

function onContentLoaded() {
    audiojs.createAll();
    
    $('.post').each(function(i, el) {
		if($('.info-content', el).size() && $('li.inf', el).size()) {

            $('.info-content', el).css('height', '0');
            $('footer', el).css('height', '0');
            if($('.disc', el).size()){
                $('.disc .images', el).css('height', '0');
                $('.disc .title', el).css('height', $('.disc .title', el).get(0).scrollHeight);
            }


/*
                        $('li.inf', el).addClass('active')
                        $('.disc .title', el).css('height', '0');
                        $('footer', el).css('height', $('footer', el).height());
                        $('.info-content', el).css('height', $('.info-content', el).height());
                        if($('.disc .images img', el).size()) {
                                $('.disc .images img', el).on('load', function(){
                                        $('.disc .images', el).css('height', $('.disc .images', el).height());
                                });


                        } else {
                                $('.disc .images', el).css('height', $('.disc .images', el).height());
                        }
*/
			$('li.inf', el).on('click', function(e){
				e.preventDefault();
				if($(this).hasClass('active')) {
					$('.info-content', el).css('height', '0');
					$('footer', el).css('height', '0');
					if($('.disc', el).size()){
						$('.disc .images', el).css('height', '0');
						$('.disc .title', el).css('height', $('.disc .title', el).get(0).scrollHeight);
					}
				} else {
					$('.info-content', el).css('height', $('.info-content', el).get(0).scrollHeight);
					$('footer', el).css('height', $('footer', el).get(0).scrollHeight);
					if($('.disc', el).size()){
						$('.disc .images', el).css('height', $('.disc .images', el).get(0).scrollHeight);
						$('.disc .title', el).css('height', 0);
					}
				}
				if($('li.text', el).hasClass('active')) $('.text-content', el).css('height', $('.text-content', el).get(0).scrollHeight);
				$(this).toggleClass('active');
			});
		}
                
		if($('.text-content', el).size() && $('li.text', el).size()) {
                        $('.text-content', el).css('height', '0');
                        
			$('li.text', el).on('click', function(e){
				e.preventDefault();
				if($(this).hasClass('active')) {
					$('.text-content', el).css('height', '0');
				} else {
					$('.text-content', el).css('height', $('.text-content', el).get(0).scrollHeight);
				}
				$(this).toggleClass('active');
			});
			
		}
	});
	
	$('.tabs').each(function(i, tab) {                
		$(tab).on('click', 'header a', function(e){
                        var $this = $(this);
                        var tabContainer = $this.parents('.tabs').get(0)
                        
			e.preventDefault();
                        
			var flag = $this.hasClass('active');
                        
			var $c = $('.content .tab', tabContainer);
                        
                        // Clear active state of previous item
			var $previousActiveTab = $('.tabs header a.active');
                        if($previousActiveTab.size()) {
                            $previousActiveTab.removeClass('active');
                            
                            var $previousActiveTabContainer = $previousActiveTab.parents('.tabs').get(0);

                            $('.content .tab', $previousActiveTabContainer).css({
                                    'height':0
                            });
                            
                            if($previousActiveTabContainer !== tabContainer) {
				$('.arrow', $previousActiveTabContainer).css({'display':'none'});
                            }
                        }
                        
			if(!flag){
				$this.addClass('active');
                                var $content = $c.filter($this.attr('href'));
                                
                                if($content.size()) {
                                    $content.css({'height':$content.get(0).scrollHeight});
                                }
                                else {
                                    $.get('/catalogue/item_details', 
                                          {
                                              id: $this.attr('href').replace('#', ''),
                                              type: $this.hasClass('book') ? 'book' : 'collection'
                                          }, function(data) {
                                              $('.content', tabContainer).append(data);
                                              
                                              $content = $c.filter($this.attr('href'));
                                              $content.css({'height':$content.scrollHeight});
                                          });
                                }
			}
                        
			if($('header a.active', tab).size()) {
				$('.arrow', tab).css({
					'display':'block',
					'left':$('header a.active', tab).offset().left + $('header a.active', tab).width()/2 - $('.arrow', tab).outerWidth()/2 + 'px'
				});
			}
		}).on('click', '.close', function(e){
			e.preventDefault();
			$(this).closest('.tab').css({'height':0});
			$(this).closest('.tabs').find('header a.active').removeClass('active');
			$(this).closest('.tabs').find('.arrow').css({'display':'none'});
		});
	});
};

function onUserPanelLoaded() {
    $('.login-panel input[type=file], .user-panel input[type=file]').noosFile({
                url: '/account/avatar/',
                dnd: true,
                maxFileSize: 4, // mb
                defaultText: 'Файл не выбран',
                btnText: 'Выбрать файл',
                currentFile: ('/content/avatars/' + $('.user-panel input[name=avatar]').val()),
		upload: function(data){
			console.log(data);
			var $f = $(this).closest('.file');
			if($f.hasClass('drag-and-drop')) {
				$('.progress span', $f).css({
					'background-image':'url('+data.url+')'
				});
				$('input[name='+$(this).attr('name')+'_drop]', $f).val(data.id);
			}
                        
                        $(this).parents('form').find('input[name=avatar]').val(data.filename);
		}
	});
	$('input.date').datepicker({
		dateFormat : "dd.mm.yy",
		changeYear:true,
		changeMonth:true,
		firstDay:1,
		minDate: new Date(1950, 0, 1),
		maxDate: "y m d",
                yearRange: "1950:+0",
		dayNamesMin:['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
		monthNamesShort:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']
	});
        
        $('header.main .block').css({
		'height':0,
		'overflow':'hidden'
	});
	$('header.main .step').each(function(i){
		if(i !== 0)
			$(this).css({
				'height':0,
				'overflow':'hidden'
			});
	});
	$('header.main .buttons').on('click', 'a[href^=#]', function(e){
		e.preventDefault();
		var $block = $($(this).attr('href')); 
		var show = $(this).hasClass('active');
		if($block.size()){
			
			$('header.main .block').css({
				'height':0,
				'overflow':'hidden'
			});
			$('a.active', $(this).parent()).removeClass('active');

			if(!show) {
				$block.css({
					height: $block.get(0).scrollHeight
				});
				setTimeout(function(){
					$block.css({'overflow':'visible'});
				}, 300);
				$(this).addClass('active');
			}
		}
	});

	$('header.main').on('click', '.toggle-step', function(){
		var $s = $(this).closest('.step').next();
		if(!$s.size()) $s = $(this).closest('.step').prev();
		$(this).closest('.step').css({
			'height':0,
			'overflow':'hidden'
		});
		$s.css({
			height: $s.get(0).scrollHeight
		});
		setTimeout(function(){
			$s.css({'overflow':'visible'});
		}, 300);
	});
	
        $('.login-panel, .user-panel').on('change', 'input', function() { $(this).removeClass('error'); });
        
        $('.login-panel, .user-panel').on('submit', 'form', function(e) {
           e.preventDefault();
           
           $form = $(this);

           $form.find('input.error').removeClass('error');
           $form.find('.errors').empty();
           
           $.ajax({
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: $form.serialize(),
                success: function(result) {                    
                    var resultType = 'plainSuccess';
                    if(result.errors) 
                        resultType = 'errors';
                    else if(result.message)
                        resultType = 'message';
                    else if(result && typeof result === 'string' || result instanceof String || result instanceof jQuery)
                        resultType = 'panelDom';
                    
                    switch(resultType) {
                        case 'errors':
                            for (var fieldname in result.errors) {
                                var err = result.errors[fieldname];
                                if(typeof err === 'string' || err instanceof String) {
                                    $form.find('.errors').append($('<li />').text(err));
                                    $form.find('[name=' + fieldname + ']').addClass('error');
                                }
                            }
                            break;
                        case 'panelDom':
						    location.reload();
                            //$('.login-panel').replaceWith(result);
                            //onUserPanelLoaded();
                            break;
                        case 'message':
                            $form.addClass('message');
                            $form.append($('<p />').addClass('message').text(result.message));
                            break;
                        default:
                            $('#block-profile').css({ 'height': 0, 'overflow': 'hidden' });
                            $('.user-panel .buttons .account').removeClass('active');
                         break;
                    }           
                }
           });
        });
}

 function updateList() {
        var $c = $(this);
        var $el;
        var w = 0;
        $('a.tag', $c).appendTo($c);
        $('ul', $c).css({ width: 'auto' }).empty().append('<li></li>').parent().css({ width: 'auto' });
        for(var j = 0; j < $('a.tag', $c).size(); j++){
                $el = $('a.tag[rel=' + j + ']', $c);
                if(!$el.hasClass('hide') && !$el.hasClass('selected')){
                        if($('ul li:last-child a', $c).size() === 4) {
                                $('ul', $c).append('<li></li>');
                        }
                        $('ul li:last-child', $c).append($el);
                }
        }
        $('ul li', $c).each(function(i3, li) {
                w += $(li).outerWidth(true) + 1;
        });
        $('ul', $c).css('width', w+'px');
        jQuery('.scroll', $c).mCustomScrollbar('update');

        if($('a.add', $c.prev()).position().left === 0) {
                $c.addClass('remove-ltr');
        } else {
                $c.removeClass('remove-ltr');
        }
}

function toggleLoader($el) {
    if($el) {
        var pos = $el.offset();
        var w = $el.outerWidth();
        var h = $el.outerHeight();
        $('.loader').css({
            width: w+'px',
            height: h+'px',
            left: pos.left+'px',
            top: pos.top+'px'
        });
        $('.loader').show('fast'); 
    }
    else {
        $('.loader').hide('fast'); 
    }
}

function getUrlVars(url) {
        var vars = [], hash, uri = url || window.location.href;
        
        if(window.location.href.indexOf('?') > 0) {
            var hashes = uri.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars[decodeURIComponent(hash[0])] = decodeURIComponent(hash[1].replace('+', ' '));
            }
        }
        return vars;
}


