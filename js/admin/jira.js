jQuery(document).ready(function($) {
       $('.filters-container .nav .categories').on('click', 'a', function(e){
                // Нажатие кнопки типа контента
		e.preventDefault();
                
                $(this).toggleClass('active');
                
                $('.filters-container').trigger('filters:change');
       });
       
       var searchVars = getUrlVars();

       if(!$.isEmptyObject(searchVars)) {
          renderFiltersState(searchVars); 
       }
       
       $('.filters-container').on('filters:change', onFiltersStateChanged);
       
       // Accept item
       $('.jira-list, #page h2').on('click', '.jira-buttons .jira-accept, .jira-buttons .jira-accept-all', function(e) {
           e.preventDefault();
           
           var $item = $(this).parents('.jira-item');
           
           var url = $(this).attr('href');
           
           if(!url.indexOf('PUBLISH')) {
                $.post(url, null, function() {
                    $item.addClass('accepted');

                    if(!($item.find('.related:visible').length > 0)) {
                        $item.fadeOut('fast').remove();
                    }
                    else {
                        $item.children().not('.related').remove();
                    }

                    if($('.jira-list li').length == 0) {
                        var url = [location.protocol, '//', location.host, '/admin/jira/'].join('');

                        if (History.enabled)
                            History.pushState('ajax', 'Jira', url);
                        else location.href = url;
                    }
                });
           }
           else {               
                var $container = $('.jira-disk-accept-container');
               
                if($(this).hasClass('active')) {
                    return;
                }

                $(this).addClass('active');
                
                var buttonOffset = $(this).offset();

                $container.css({
                    top: buttonOffset.top + $(this).outerHeight(),
                    right: $(document).width() - buttonOffset.left - $(this).outerWidth()
                }).show('fast');

                $('.jira-accept', $container).one('click', function() {
                    var $images = $container.find('.disk-images-upload img')
                    var data = $images.length ? { images: JSON.stringify($images.map(function() { return $(this).attr('src'); }).get()) } : null;

                    $.post(url, data, function() {
                        $item.addClass('accepted');

                        if(!($item.find('.related:visible').length > 0)) {
                            $item.fadeOut('fast').remove();
                        }
                        else {
                            $item.children().not('.related').remove();
                        }

                        if($('.jira-list li').length == 0) {
                            var url = [location.protocol, '//', location.host, '/admin/jira/'].join('');

                            if (History.enabled)
                                History.pushState('ajax', 'Jira', url);
                            else location.href = url;
                        }
                    });

                    $container.trigger('close');
                });

                $('.jira-cancel', $container).one('click', function() {
                    $container.trigger('close');
                });

                $(document).bind('popup.click', function(e) {
                    if(!$(e.target).hasClass('.jira-accept') &&
                       $(e.target).parents('.jira-accept').length === 0 &&
                       !$(e.target).hasClass('.jira-disk-accept-container') &&
                       $(e.target).parents('.jira-disk-accept-container').length === 0) {
                        $(document).unbind('popup.click');
                        $container.trigger('close');
                    }
                });
           }
       });
       
       // Decline item
       $('.jira-list, #page h2').on('click', '.jira-decline', function(e) {
           e.preventDefault();
           
           var $container = $('.jira-comment-container');
           
           if($(this).hasClass('active')) {              
              return;
           }
           
            $(this).addClass('active');

            var $item = $(this).parents('.jira-item');
            
            var url = $(this).attr('href');
           
            var buttonOffset = $(this).offset();

            $container.css({
                top: buttonOffset.top + $(this).outerHeight(),
                right: $(document).width() - buttonOffset.left - $(this).outerWidth()
            }).show('fast');

            $('.jira-comment-container .jira-accept').one('click', function() {                
                var comment = $container.find('textarea').val() !== '' ? $container.find('textarea').val() : null;
                
                $.post(url, { comment: comment },
                       function() {
                         $item.addClass('declined');
                         $item.find('.jira-buttons').remove();
                });
                
                $container.trigger('close');
            });

            $('.jira-comment-container .jira-cancel').one('click', function() {
                $container.trigger('close');
            });

            $(document).bind('popup.click', function(e) {
                if(!$(e.target).hasClass('.jira-comment-container') &&
                   $(e.target).parents('.jira-comment-container').length === 0) {
                    $(document).unbind('popup.click');
                    $container.trigger('close');
                }
            });
       });
       
       $('.jira-list').on('click', '.related .filter', function() {
           var url = [location.protocol, '//', location.host, '/admin/jira/'].join('') + $(this).attr('href');

            if (History.enabled)
                History.pushState('ajax', 'Jira', url);
            else location.href = url;
       });
       
       $('.jira-list').on('click', '.related li a', function() {
           if($($(this).attr('href')).length) return true;
           
           var url = [location.protocol, '//', location.host, '/admin/jira/'].join('') + '?key=' + $(this).attr('href').replace('#', '');

            if (History.enabled)
                History.pushState('ajax', 'Jira', url);
            else location.href = url;
       });
       
       $('.jira-list').on('click', '.related .close-button', function() {
           var $item = $(this).parents('.jira-item');
            if ($item.children().not('.related').length == 0) {
                $item.remove();
            }
            else {
                $item.find('.relative').remove();
            }
           
           if ($('.jira-list li').length == 0) {
               var url = [location.protocol, '//', location.host, '/admin/jira/'].join('');

               if (History.enabled)
                   History.pushState('ajax', 'Jira', url);
               else location.href = url;
           }
       });
       
       $('.jira-comment-container').bind('close', function() {
           $('textarea', this).val('');
           $('.jira-buttons .active').removeClass('active');
           $(this).hide('fast');
       });
       
       $('.jira-disk-accept-container').bind('close', function() {
           $('.disk-images-upload input', this).val('');
           $('.disk-images-upload img').remove();
           $('.jira-buttons .active').removeClass('active');
           $(this).hide('fast');
       });
       
       $('.jira-disk-accept-container input[type=file]').noosFile({
                url: '/collection/upload/',
                dnd: true,
                maxFileSize: 4, // mb
                defaultText: 'Файл не выбран',
                btnText: 'Выбрать файл',
		upload: function(data){
                    console.log(data);
                    var $f = $(this).closest('.file');
                    
                    $f.removeClass('loaded').find('.progress').css('width', '0px');
                    
                    $f.before('<img src="' + data.url + '" height="140px" />');
		}
	});
       
       onContentLoaded();
       
       /*  Oбработчик изменения состояния страницы */
       History.Adapter.bind(window ,'statechange', function() { 
            var state = History.getState();

            /* Получаем URL нового состояния. Это URL, который мы передали
            в .pushState() */
            var url = state.url + (state.url.indexOf('?') > 0 ? '&' : '?') + 'ajax=1';

            $('.jira-list').load(url, function () {
                onContentLoaded();
            });
       });
});

function renderFiltersState(searchVars) {        
    if(searchVars.content_types){
       var contentTypesArr = searchVars.content_types.split(',');
       $.each(contentTypesArr, function(i, contentType){
           $('.filters-container .nav .categories a[href=#' + contentType + ']').addClass('active');
       });
    }
}

function onFiltersStateChanged() {
    var searchCriteria = {};
    if($('.filters-container .nav .categories .active').length > 0 &&
       $('.filters-container .nav .categories .active').length !== $('.filters-container .nav .categories > a').length ) {
        var types = [];
        $('.filters-container .nav .categories .active').each(function() {
            types.push($(this).attr('href').replace('#', ''));
        });
        
        searchCriteria.content_types = types.join(',');
    }
    var queryString = $.param(searchCriteria);
    
    var url = [location.protocol, '//', location.host, '/admin/jira/'].join('') + (queryString ? '?' + queryString : '');
    
    if (History.enabled)
	History.pushState('ajax', 'Jira' + (searchCriteria.content_types ? ', ' + searchCriteria.content_types + ' only' : ''), url);
    else location.href = url;
}

function onContentLoaded() {
    audiojs.createAll();
};

function getUrlVars(url) {
        var vars = [], hash, uri = url || window.location.href;
        
        if(window.location.href.indexOf('?') > 0) {
            var hashes = uri.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars[decodeURIComponent(hash[0])] = decodeURIComponent(hash[1].replace('+', ' '));
            }
        }
        return vars;
}
