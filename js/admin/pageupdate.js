$(function()
{
    $('#body').redactor({
        imageUpload: '/admin/page/upload'
    });
    
    $('#alias').on('input', function() {
        var $this = $(this);
        $this.removeClass('invalid').parent().find('label.error').remove();
        
        $.get('/admin/page/check_alias', { id: $('#id').val(),  alias: $this.val() }, function(result) {
            if(result.valid === false) {
                $this.addClass('invalid');
                $this.after('<label class="error">' + result.message + '</label>');
            }
        }, 'json');
    });
    
    $('[type=submit]').click(function(e) {
        if($(this).parents('form').find('input.invalid').length > 0 || !$('#alias').val() || !$('#title').val()) {
            e.preventDefault();
        }
        
        if(!$('#alias').val()) {
            $('#alias').addClass('invalid');
        }
        
        if(!$('#title').val()) {
            $('#title').addClass('invalid');
        }
    });
    
    $('#is_in_main_menu').change(function() {
        $('#parent_id').prop('disabled', $(this).is(':checked'));
    });
    $('#parent_id').prop('disabled', $('#is_in_main_menu').is(':checked'));
});