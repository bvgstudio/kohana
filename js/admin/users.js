jQuery(document).ready(function($) { 
       $('.users-list').on('click', '.user-delete', function() {
           var $item = $(this).parents('tr');
           $.get($(this).attr('href'), function() {
               $item.fadeOut('fast').remove();
           });
       });
       
       $('.users-list').on('click', '.user-confirm', function() {
           $(this).parents('tr').trigger('user:edited');
       });
       
       $('.users-list').on('keypress', 'td', function(eventObject){
           if(eventObject.which === 13)
               $(this).blur();
       });
       
       $('.users-list').on('blur', 'td', function(){
           $(this).trigger('user:edited');
       });
       
       $('.users-list').on('blur', 'td.roles a', function(){
           $(this).toggleClass('active');
           
           $(this).parents('tr').trigger('user:edited');
       });
       
       $('.users-list').on('user:edited', 'tr', function() {
           var $item = $(this);
           
           var data = {};
           
           $item.find('td:not(.controls, .roles)').each(function() {               
               data[$(this).attr('class')] = $(this).html();
           });
           
           data.roles = [];
           if($item.find('.roles .filial').hasClass('active')) data.roles.push('representatives');
           if($item.find('.roles .team').hasClass('active')) data.roles.push('team');

           
           $.post('/admin/users/edit/' + $item.attr('id'), data, function() {
               $item.fadeOut('fast').remove();
           });
       });
       
       $('.content.main').on('click', '.pagination a', function(e){
            e.preventDefault();
            History.pushState('ajax', document.title, $(this).prop('href'));
        });
});