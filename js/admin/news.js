jQuery(document).ready(function($) {
       $('.filters-container .nav .categories').on('click', 'a', function(e){
                // Нажатие кнопки типа контента
		e.preventDefault();
                
                $(this).toggleClass('active');
                
                $('.filters-container').trigger('filters:change');
       });
       
       var searchVars = getUrlVars();

       if(!$.isEmptyObject(searchVars)) {
          renderFiltersState(searchVars); 
       }
       
       $('.filters-container').on('filters:change', onFiltersStateChanged);
       
       $('.news-list').on('click', '.jira-decline', function() {
           var $item = $(this).parents('article.post');
           $.get($(this).attr('href'), function() {
                   $item.fadeOut('fast').remove();
           });
       });
       
       onContentLoaded();
});

function renderFiltersState(searchVars) {        
    if(searchVars.content_types){
       var contentTypesArr = searchVars.content_types.split(',');
       $.each(contentTypesArr, function(i, contentType){
           $('.filters-container .nav .categories a[href=#' + contentType + ']').addClass('active');
       });
    }
}

function onFiltersStateChanged() {
    var searchCriteria = {};
    if($('.filters-container .nav .categories .active').length > 0 &&
       $('.filters-container .nav .categories .active').length !== $('.filters-container .nav .categories > a').length ) {
        var types = [];
        $('.filters-container .nav .categories .active').each(function() {
            types.push($(this).attr('href').replace('#', ''));
        });
        
        searchCriteria.content_types = types.join(',');
    }
    var queryString = $.param(searchCriteria);
    
    var url = [location.protocol, '//', location.host, '/admin/news/'].join('') + (queryString ? '?' + queryString : '');
    
    if (History.enabled)
	History.pushState('ajax', 'Новости ' + (searchCriteria.content_types ? ', ' + searchCriteria.content_types + ' only' : ''), url);
    else location.href = url;
}

function onContentLoaded() {
    audiojs.createAll();
};

function getUrlVars(url) {
        var vars = [], hash, uri = url || window.location.href;
        
        if(window.location.href.indexOf('?') > 0) {
            var hashes = uri.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars[decodeURIComponent(hash[0])] = decodeURIComponent(hash[1].replace('+', ' '));
            }
        }
        return vars;
}

/*  Oбработчик изменения состояния страницы */
History.Adapter.bind(window ,'statechange', function() { 
    var state = History.getState();

    /* Получаем URL нового состояния. Это URL, который мы передали
    в .pushState() */
    var url = state.url + (state.url.indexOf('?') > 0 ? '&' : '?') + 'ajax=1';

    $('.news-list').load(url, function () {
        onContentLoaded();
    });
});
