$(function()
{
    $('#teaser, #body').redactor({
        imageUpload: '/admin/news/upload'
    });
    
    $('#tags').tagEditor({
        autocomplete: {
            delay: 0,
            minLength: 2,
            position: { collision: 'flip' },
            source: '/tags/list'
        },
        forceLowercase: false
    });
    
    $('.news .objects').on('click', 'li a:not(.add)', function(e){
            // Обработчик клика на выбранном элементе. Убираем его из списка выбранных, обновляем список для выбора.
            e.preventDefault();

            $('.add-items-list a[data-id='+ $(this).data('id') +']').parent().removeClass('selected');
            $(this).parent().remove();
    });
    
    $('.news .objects li a.add').click(function(e){
        e.preventDefault();
       
        if(!$(this).hasClass('active')){
                if($('.add-items-list ul li').length == 0) $('.add-items-list').trigger('filters:change');
                
                $('.add-items-list').addClass('show');
                $(this).addClass('active');

                if($(this).position().left === 0) {
                        $(this).closest('.filter').next().addClass('remove-ltr');
                } else {
                        $(this).closest('.filter').next().removeClass('remove-ltr');
                }
        }
        else {
            $('.add-items-list.show').removeClass('show');
            $(this).removeClass('active');
        }
    });
    
    $('.add-items-list .items').on('click', 'a', function(e){
            e.preventDefault();
            var $item = $(this).parent();
            if (!$item.hasClass('selected')) {
                $item.clone().prependTo($('.news .objects'));
                $item.addClass('selected');
            }
            else {
                $(this).removeClass('selected');
                $('.news .objects li a[data-id='+ $(this).data('id') +']').parent().remove();
            }
    });
    
    $('.add-items-list .filters').on('click', 'a', function(e){
                // Нажатие кнопки типа контента
		e.preventDefault();
                
                $(this).toggleClass('active');
                
                $('.add-items-list').trigger('filters:change');
    });
    
    $('.add-items-list .search input').on('keyup', function(e){
        $('.add-items-list').trigger('filters:change');
    });
    
    $('input.date').datepicker({
		dateFormat : 'dd.mm.yy',
		firstDay:1,
		dayNamesMin:['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
		monthNamesShort:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']
    }).datepicker('setDate', new Date());
    
    $('form :submit').click(function() {        
        var contentIds = $('.news .objects li a[data-id]').map(function () {
                return $(this).data('id');
        }).get();

        $('input[name=content_items]').val(contentIds.join(','));
        
        return true;
    });
    
    $('.add-items-list').on('filters:change', function() {
        var searchCriteria = {};
        
        if($('.add-items-list .filters .active').length > 0 &&
           $('.add-items-list .filters .active').length !== $('.add-items-list .filters > a').length ) {
            var types = [];
            $('.add-items-list .filters .active').each(function() {
                types.push($(this).attr('href').replace('#', ''));
            });

            searchCriteria.content_types = types.join(',');
        }
        
        if($('.add-items-list .search input').val() != '') {
            searchCriteria.title = $('.add-items-list .search input').val();
        }
        
        var queryString = $.param(searchCriteria);
        
        $.get('/admin/news/medialist', queryString, function(list) {
            $('.add-items-list .items').empty().append(list);
            $('.news .objects .item').each(function() {
                $('.add-items-list ul [data-id='+ $(this).data('id') +']').parent().addClass('.selected');
            });
        }, 'html');
    });
    
    $('[type=submit]').click(function(e) {
        if(!$('#title').val()) {
            e.preventDefault();
            $('#title').addClass('invalid');
        }
    });
});