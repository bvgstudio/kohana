jQuery(document).ready(function($) { 
       $('.pages-list, .news-list').on('click', '.jira-decline', function() {
           var $item = $(this).parents('article.post');
           $.get($(this).attr('href'), function() {
                   $item.fadeOut('fast').remove();
           });
       });
       
       onContentLoaded();
});

function onContentLoaded() {
    audiojs.createAll();
};