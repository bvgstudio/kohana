jQuery(document).ready(function($) {
       $('.page .teaser, .page .o-h > div,  .page .o-h > p, .page .o-h blockquote').each(function() {
           var hasColumnedChildren = $(this).find('.columned').length > 0;
           
           if($(this).text().length > 650 && !hasColumnedChildren && !$(this).hasClass('uncolumned')) {
               $(this).addClass('columned');
           }
       });
       
       $('.content header .subnav a, .content .service li a , .page header h3 a').click(function() {
           if($(this).attr('href').indexOf('#') !== 0 || $(this).attr('href').length === 1) return;
           
           var contentId = $(this).attr('href');
           
           $('.content header .subnav a, .page h3 a').removeClass('active');
           $('.content header .subnav a[href=' + contentId + '], .page h3 aa[href=' + contentId + ']').addClass('active');
           
           
           $('.subcontent-container > ' + contentId).show();
           $('.subcontent-container > :not(' + contentId + ')').hide();
           
           $('.content header ' + contentId + '-subtitle').show();
           $('.content header :header[id*="-subtitle"]:not(' + contentId + '-subtitle)').hide();
      });
      
      if(window.location.hash) {
           var hiddenContentId = window.location.hash;
           
           $('.content header .subnav a[href='+ hiddenContentId + '], .page h3 a[href='+ hiddenContentId + ']').click();
      }
});