// JavaScript Document

// by noos

if(typeof jQuery != "undefined") {
	(function($) {
		var mobile;
		
		var methods = {
			init:function(options){
				return this.each(function(){
					if(!$(this).hasClass('noos-slider')) {
						var s = $.extend({
							'z-index':999,
							
							'mobile':mobile,
						}, options);
						s = $.extend(s, $(this).data()); $(this).data(s);
						
					}
				})
			},
			show:function(){
				
			},
			hide:function(){
				
			}
		};
		
		
		
		$.fn.noosNotice = function( method ) {
			if ( methods[method] ) {
				return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
				return methods.init.apply( this, arguments );
			} else {
				$.error( 'Метод с именем ' +  method + ' не существует для jQuery.tooltip' );
			}    
		};
	})(jQuery);
}