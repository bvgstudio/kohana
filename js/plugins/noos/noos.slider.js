// JavaScript Document

// by noos

if(typeof jQuery != "undefined") {
	(function($) {
		var mobile = false;
		var notice = false;
		var showError = function(){
			
		}
		var animations = {
			'slide-h':{
				prepear:function(){
					var s = $(this).data();
					var $o = {
						t: $(this),
						s: $('.'+s.slides, this),
						p: $('.'+s.pagination, this),
						n: $('.'+s.prevButton+', .'+s.nextButton, this),
					}
					
					/*$('> li', $o.s).css({
						'left':'100%'
					});
					$('> li.active', $o.s).css({
						'left':'0'
					});*/
				},
				action:function(d,i){
					
				}
			},
			'slide-v':function(){
				
			},
			'slide-d-l':function(){
				
			},
			'slide-d-r':function(){
				
			},
			'fade':function(){
				
			},
			'fade-zoom':function(){
				
			},
		}
		var methods = {
			init:function(options){
				return this.each(function(){
					if(!$(this).hasClass('noos-slider')) {
						var t = this;
						var s = $.extend({
							autoAnimate:5000,
							startDelay:0,
							slides:'s', //class
							pagination:'p', //class
							autoPagination:false,
							prevButton:'prev',
							nextButton:'next',
							animateType:'slide-h', // css animation
							'animations':animations,
							width:'100%',
							height:'auto',
							errorsType:'console', // notice/console
							
							'errors':{
								'no-slides-container':'Не знайдено контейнер слайдів',
								'no-slides':'Не знайдено слайди',
								'no-enoth-slides':'Не достатньо слайдів',
							},
							'mobile':mobile,
							'notice':notice,
						}, options);
						s = $.extend(s, $(this).data()); $(this).data(s);
						var temp = '';
						
						var $o = {
							t: $(this),
							s: $('.'+s.slides, this),
							p: $('.'+s.pagination, this),
							n: $('.'+s.prevButton+', .'+s.nextButton, this),
						}
						
						$o.t.addClass('noos-slider '+s.animateType);
						$o.s.addClass('ns-s');
						
						if($o.p.size()) {
							$o.p.addClass('ns-p');
						} else {
							if(s.autoPagination && s.pagination){
								temp = '<ul class="'+s.pagination+'">';
								for(var i=0; i < $('> li', $o.s).size(); i++){
									temp += '<li></li>';
								}
								temp += '</ul>';
								$o.t.append(temp);
								temp = '';
								$o.p = $('.'+s.pagination, $o.t);
							} else {
								$o.p = false;
							}
						}
						if(!$o.n.size() && (s.prevButton && s.nextButton)){
							$o.t.append('<span class="'+s.prevButton+'"></span><span class="'+s.nextButton+'"></span>');
							$o.n = $('.'+s.prevButton+', .'+s.nextButton, $o.t);
						}
						
						if($('> li', $o.s).size() < 2) {
							
						}
						if(!$('> li.active', $o.s).size()) {
							if($o.p && $('> li.active', $o.p).size()) {
								$('> li:eq('+$('> li.active', $o.p).index()+')', $o.s).addClass('active');
							} else {
								$('> li:first-child', $o.s).addClass('active');
								if($o.p) $('> li:first-child', $o.p).addClass('active');
							}
						}
						if($o.s.find('li.active').children().size()==1 && $o.s.find('li.active').children().is('img')) {
							$o.s.find('li.active').children('img').on('load', function(){
								$o.s.css({
									'height':s.height=='auto'?$('> li.active', $o.s).outerHeight()+'px':s.height
								});
							});
						} else {
							$o.s.css({
								'height':s.height=='auto'?$('> li.active', $o.s).outerHeight()+'px':s.height
							});
						}
						
						//s.animations[s.animateType].prepear.apply(t);
						
						// events
						if($o.p)
							$o.t.on('click', '.'+s.pagination+' li', function(){
								methods.animate.apply($(this).closest('.noos-slider'), ['to', $(this).index()]);
							});
						if($o.n)
							$o.n.on('click', function(){
								var d = 'next';
								if($(this).hasClass(s.prevButton)) d = 'prev';
								methods.animate.apply($(this).closest('.noos-slider'), [d]);
							});
							
						if(s.autoAnimate) {
							$o.t.data('timeId', setTimeout(function(){
								methods.animate.apply($o.t, ['next']);
							},s.autoAnimate + s.startDelay));
							$o.t.on('mouseenter', function(){
								$(this).addClass('hovered');
								methods.stop.apply($o.t);
							}).on('mouseout', function(){
								$(this).removeClass('hovered');
								methods.start.apply($o.t);
							});
						}
						
						// events ens
						
						$o.t.trigger('init');
					}
				})
			},
			animate:function(d,i){
				var a = arguments;
				return this.each(function(){
					$(this).trigger('animationStart');
					
					var s = $(this).data();
					var $o = {
						t: $(this),
						s: $('.'+s.slides, this),
						p: $('.'+s.pagination, this),
						n: $('.'+s.prevButton+', .'+s.nextButton, this),
					}
					var $active = $('> li.active', $o.s);
					var active = $active.index();
					var transition = {
						delay:$('> li', $o.s).css('transition-delay'),
						duration:$('> li', $o.s).css('transition-duration'),
						property:$('> li', $o.s).css('transition-property'),
					}
					var $show = false;
					var time = 0;
					
					methods.stop.apply($o.t)
					
					time = parseInt(transition.delay) * (/\d+s/.test(transition.delay)?1000:1) + parseInt(transition.duration) * (/\d+s/.test(transition.duration)?1000:1);
					
					if(d=='to') {
						if(i!==false) {
							if(active > i) {
								d = 'next';
							} else {
								d = 'prev';
							}
							$show = $('> li', $o.s).eq(i);
						}
					}
					if(!$show || !$show.size()) {
						if(d=='next'){
							if($active.next().size()) {
								$show = $active.next();
							} else {
								$show = $('> li:first-child', $o.s);
							}
						} else if(d=='prev'){
							if($active.prev().size()) {
								$show = $active.prev();
							} else {
								$show = $('> li:last-child', $o.s);
							}
						}
					}
					if(!$show.hasClass('active')) {
						if(d=='next'){
							$show.addClass('next-show');
							$('>li', $o.s).removeClass('next-hide').removeClass('prev-hide').removeClass('prev-show')
							setTimeout(function(){
								$show.css('left');
								$show.addClass('active').removeClass('next-show');
								$active.removeClass('active').addClass('next-hide');
								$o.s.css({
									'height':s.height=='auto'?$('> li.active', $o.s).outerHeight()+'px':s.height
								});
							}, 1);
						}
						if(d=='prev'){
							$show.addClass('prev-show');
							$('>li', $o.s).removeClass('prev-hide').removeClass('next-hide').removeClass('next-show');
							
							setTimeout(function(){
								$show.css('left');
								$show.addClass('active').removeClass('prev-show');
								$active.removeClass('active').addClass('prev-hide');
								$o.s.css({
									'height':s.height=='auto'?$('> li.active', $o.s).outerHeight()+'px':s.height
								});
							}, 1);
						}
						
						if($o.p.size()) {
							setTimeout(function(){
								$o.p.find('.active').removeClass('active');
								$o.p.find('li').eq($('>li.active', $o.s).index()).addClass('active');
								$o.s.css({
									'height':s.height=='auto'?$('> li.active', $o.s).outerHeight()+'px':s.height
								});
							}, 3);
						}
						if(!time) time = 5;
						setTimeout(function(){
							methods.start.apply($o.t)
							
							$(this).trigger('animationEnd');
						}, time);
					}
				});
			},
			resize:function(){
				
			},
			start:function(){
				return this.each(function(){
					var s = $(this).data();
					var $o = {
						t: $(this),
						s: $('.'+s.slides, this),
						p: $('.'+s.pagination, this),
						n: $('.'+s.prevButton+', .'+s.nextButton, this),
					}
					if(s.autoAnimate && !$o.t.hasClass('hovered')) {
						clearTimeout($o.t.data('timeId'));
						$o.t.data('timeId', setTimeout(function(){
							methods.animate.apply($o.t, ['next']);
						}, s.autoAnimate));
					}
				});
			},
			stop:function(){
				return this.each(function(){
					var s = $(this).data();
					var $o = {
						t: $(this),
						s: $('.'+s.slides, this),
						p: $('.'+s.pagination, this),
						n: $('.'+s.prevButton+', .'+s.nextButton, this),
					}
				});
				if(s.autoAnimate)
					clearTimeout($o.t.data('timeId'));
			}
		};
		
		$.fn.noosSlider = function( method ) {
			if ( methods[method] ) {
				return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
				return methods.init.apply( this, arguments );
			} else {
				$.error( 'Метод с именем ' +  method + ' не существует для jQuery.noosSlider' );
			}    
		};
	})(jQuery);
}