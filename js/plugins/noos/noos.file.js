// JavaScript Document

// by noos

if(typeof jQuery != "undefined") {
	(function($) {
		var device = $(window).width() <= 1024 && $(window).width() >= 768 ? 'tablet' : $(window).width() < 768 ? 'mobile' : 'desctop';
		var notice = false;
		var handleFileUpload = function(files){
			
			var s = $('input[type=file]', this).data();
			for (var i = 0; i < files.length; i++) {
				var fd = new FormData();
				fd.append('file', files[i]);
				//console.log(files[i].size, s, s.maxFileSizeBytes);
				if(files[i].size <= s.maxFileSizeBytes) {
					$(this).removeClass('error bigFile');
					sendFileToServer.call(this,fd);
				} else {
					$(this).addClass('error bigFile');
				}
			}
		}
		function sendFileToServer(formData){
			var t = this;
			var $progress = $('.progress', this);
			var s = $('input[type=file]', this).data();
			var jqXHR=$.ajax({
				xhr: function() {
					var xhrobj = $.ajaxSettings.xhr();
					if (xhrobj.upload) {
						xhrobj.upload.addEventListener('progress', function(event) {
							var percent = 0;
							var position = event.loaded || event.position;
							var total = event.total;
							if (event.lengthComputable) {
								percent = Math.ceil(position / total * 100);
							}
							$progress.css({'width':percent+'%'});
							//Set progress
							//status.setProgress(percent);
						}, false);
					}
					return xhrobj;
				},
				url: s.url,
				type: "POST",
				contentType:false,
				processData: false,
				cache: false,
				data: formData,
				dataType:"json",
				success: function(data){
					$progress.css({'width':'100%'});
					$(t).addClass('loaded');
					s.upload.call($('input[type=file]', t).get(0), data);
				}
			});
		 
			//status.setAbort(jqXHR);
		}
		var methods = {
			init:function(options){
				this.each(function(){
					if($(this).parent().parent().hasClass('noos-file')) return;
                                        
                                        var t = this;
                                        var s = $.extend({
                                                dnd:false,
                                                maxFileSize:4, // mb
                                                defaultText:'Файл не обрано',
                                                btnText:'Обрати файл',
                                                upload:function(){

                                                }
                                        }, options);
                                        var $temp;
                                        //console.log(s, $(t).data());
                                        s = $.extend(s, $(t).data());
                                        s.maxFileSizeBytes = Math.round(parseInt(s.maxFileSize)*1048576*100000)/100000;
                                        $(t).data(s);

                                        $(t).after('<div class="file noos-file'+(s.dnd?' drag-and-drop':'')+'"><div class="field"></div><div class="btn">'+s.btnText+(s.dnd?'<span class="progress"><span></span></span>':'')+'</div><div class="v">'+($(t).val()?$(t).val():s.defaultText)+'</div>'+(s.dnd?'<input type="hidden" name="'+$(t).attr('name')+'_drop" value="">':'')+'</div>');
                                        var $o = {
                                                f:$(t).next(),
                                                v:$(t).next().find('.v'),
                                                b:$(t).next().find('.btn')
                                        }
                                        $temp = $(t).next();
                                        $(t).appendTo($(t).next().find('.field'));

                                        if(s.dnd && typeof(window.FileReader) != 'undefined') {
                                                $o.f.addClass(s.dragAndDropClass);
                                        }

                                        if(s.dnd && s.currentFile){
                                                $temp.addClass('loaded');
                                                $('.progress', $temp).css({'width':'100%'})
                                                $('.progress span', $temp).css({
                                                        'background-image':'url('+s.currentFile+')'
                                                });
                                        }
				});
				if(!$('body').hasClass('noos-file-init')){
					$('body').addClass('noos-file-init');
					$(document).on('click', '.noos-file .btn', function(e){
						if($('.progress', this).size()){
							var $f = $(this).closest('.noos-file');
							$('.progress', $f).css({
								'width':0,
								'background-image':'none'
							});
							$('input[name='+$('input[type=file]',$f).attr('name')+'_drop]', $f).val('');
						}
						$('input[type=file]', $(this).parent()).trigger('click');
					}).on('change', '.noos-file input[type=file]', function(e){
						$(this).closest('.noos-file').find('.v').text($(this).val());
                                                handleFileUpload.call($(this).closest('.noos-file'), this.files);
					}).on('mouseenter', '.noos-file input[type=file]', function(){
						$(this).closest('.noos-file').addClass('hover');
					}).on('mouseleave', '.noos-file input[type=file]', function(){
						$(this).closest('.noos-file').removeClass('hover');
					}).on('focus', '.noos-file input[type=file]', function(){
						$(this).closest('.noos-file').addClass('focus');
					}).on('blur', '.noos-file input[type=file]', function(){
						$(this).closest('.noos-file').removeClass('focus');
					});
					
					$(document).on('dragenter', function(e){
						e.stopPropagation();
						e.preventDefault();
						$('.noos-file.drag-and-drop').addClass('file-in-window');
						if(!$(e.target).hasClass('drag-and-drop') && !$(e.target).closest('.drag-and-drop').size()) {
							$('.noos-file.drag-and-drop').removeClass('file-in');
						}
						//$(this).removeClass('file-in');
					}).on('dragover', function(e){
						e.stopPropagation();
						e.preventDefault();
					}).on('dragleave', function(e){
						e.stopPropagation();
						e.preventDefault();
					}).on('drop', function(e){
						e.stopPropagation();
						e.preventDefault();
						$('.noos-file.drag-and-drop.file-in-window').removeClass('file-in-window');
					}).on('dragenter', '.noos-file.drag-and-drop', function(e){
						e.stopPropagation();
						e.preventDefault();
						$(this).addClass('file-in').removeClass('file-in-window');
					}).on('dragover', '.noos-file.drag-and-drop', function(e){
						e.stopPropagation();
						e.preventDefault();
					}).on('dragleave', '.noos-file.drag-and-drop', function(){
						//$(this).removeClass('file-in');
					}).on('mouseenter', '.noos-file.drag-and-drop', function(){
						
					}).on('drop', '.noos-file.drag-and-drop', function(e){
						e.stopPropagation();
						e.preventDefault();
						$(this).removeClass('file-in');
						handleFileUpload.call(this, e.originalEvent.dataTransfer.files);
					}).on('click', function(){
						$('.noos-file.drag-and-drop.file-in-window').removeClass('file-in-window');
					});
				}
			},

		};
		
		$.fn.noosFile = function(method){
			if ( methods[method] ) {
				return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
			} else if ( typeof method === 'object' || ! method ) {
				return methods.init.apply( this, arguments );
			} else {
				$.error( 'Метод с именем ' +  method + ' не существует для jQuery.noosFile' );
			}
		};
	})(jQuery);
}