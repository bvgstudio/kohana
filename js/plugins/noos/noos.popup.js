// JavaScript Document

// by noos

if(typeof jQuery != "undefined") {
	(function($) {
		$.fn.noosPopUp = function(options){
			//if($.fn.jquery < "1.3.1") {return false;}

			var settings = $.extend({}, $.fn.noosPopUp.defaults, options);
			return this.each(function(){
				var $t = $(this);
				var s = settings;
				var $mask;
				var $popUp;
				
				var f = {
					createPopUp:function(url){
						
						var width = s.width;
						var height = 'auto';
						var content = '';
						var imgWidth = 'auto';
						
						if(url == '#') {
							height = '200px';
						}
						
						$('body')
							.append('<div id="popUpMask" />')
							.append('<div id="popUp" style="top:'+($(window).scrollTop() + $(window).height() * 0.5)+'px; width:1px; heigth:1px; position:absolute; opacity:0;"><div class="wrapper"><a class="close close_button" href="#"></a><div id="popUpContent"></div></div></div>');
						$mask = $('#popUpMask');
						$popUp = $('#popUp');
						
						var $content = $('#popUpContent', $popUp);
						
						$mask
							.css({
								position:'fixed',
								left:0,
								top:0,
								right:0,
								bottom:0,
								opacity:0,
								background:s.bg,
								zIndex:s.zindex
							})
						$mask.css({
							opacity:s.opacity
						}, 200);
							
						$popUp.css({
							'height':'auto',
							'width':width,
							'z-index':s.zindex + 1,
						});
						f.centeredPopUp();
						
						$popUp.css({
							opacity:1
						});
						$('#popUpMask, #popUp .close').on('click', function(){
							f.destroyPopUp();
						});
						if(url){
							if(url.match(/\/$|\.html$|\.php$/)){
								$content.html('<div id="ajax-loader" style="text-align:center; padding: 20px 0; clear:both;"><div class="spinner"><div class="circle1 circle"></div><div class="circle2 circle"></div><div class="circle3 circle"></div><div class="circle4 circle"></div><div class="circle5 circle"></div><div class="circle6 circle"></div><div class="circle7 circle"></div><div class="circle8 circle"></div><div class="circle9 circle"></div><div class="circle10 circle"></div><div class="circle11 circle"></div><div class="circle12 circle"></div></div></div>');
								f.centeredPopUp();
								$.ajax({
									url:url,
									type:'POST',
									dataType:'html',
									success:function(data){
										$content.html(data);
										f.centeredPopUp();
										s.ajaxSuccess.call($t);
									}
								});
							} else if(url.match(/\.gif$|\.jpg$|\.jpeg$|\.png$/)){
								$('body').append('<img id="tempImage" alt=="" src="'+url+'" style="position:absolute;top:0;right:100%;" />');
								$content.html('<div id="ajax-loader" style="text-align:center; padding: 20px 0; clear:both;"><div class="spinner"><div class="circle1 circle"></div><div class="circle2 circle"></div><div class="circle3 circle"></div><div class="circle4 circle"></div><div class="circle5 circle"></div><div class="circle6 circle"></div><div class="circle7 circle"></div><div class="circle8 circle"></div><div class="circle9 circle"></div><div class="circle10 circle"></div><div class="circle11 circle"></div><div class="circle12 circle"></div></div></div>');
								f.centeredPopUp();
								$('#tempImage')
									.load(function(){
										if($(this).width() > $('body').width()){
											imgWidth = ($('body').width() - 50) + 'px';
										} else {
											imgWidth = $(this).width()+'px';
										}
										$content.html('<img alt=="" src="'+url+'" style="display:block; width:'+imgWidth+'" />');
										s.ajaxSuccess.call($t);
										$popUp.css({width:'auto'});
										f.centeredPopUp();
										$(this).remove();
									});
								
							} else if(url.match(/youtu/i)){
								var v_id;
								v_id = url.match(/=(\w+)/i);
								v_id = v_id[1];
								if($(window).width()>1600){
									$popUp.css({
										width:'1300px'
									});
								}
								f.centeredPopUp();
								$content.html('<iframe width="'+($(window).width()>1600?1280:853)+'" height="'+($(window).width()>1600?720:480)+'" src="//www.youtube.com/embed/'+v_id+'" frameborder="0" allowfullscreen></iframe>');
								f.centeredPopUp();
								s.ajaxSuccess.call($t);
							} else {
								if(content == '') {
									content = s.content;
								}
								$content.css({'height':height}).html(content);
							}
						}
					},
					centeredPopUp:function(){
						var re = /^\d+px$/i;
						var r = 0;
						if(typeof(s.top) == 'integer' || re.test(s.top) ) {
							t = parseInt(s.top) + $(window).scrollTop();
						} else {
							t = $(window).scrollTop() + ($(window).height() - $popUp.height()) / 2;
							if(t<0) t = 0;
						}
						$popUp.css({
							left: $('body').width() * 0.5 - $popUp.width() * 0.5,
							top: t + 'px'
						});
					},
					destroyPopUp:function(){
						$mask
							.css({
								opacity:0
							})
							setTimeout(function(){
								$mask.remove();
							}, 250);
						$popUp
							.css({
								opacity:0
							});
							setTimeout(function(){
								$popUp.remove();
							}, 250);
					}
				}
				
				$t.on('click', function(){
					if(!$(this).hasClass('disabled')){
						var url = $(this).attr('href');
						f.createPopUp(url);
					}
					return false;
				}).data('f', f);
			});
		};
		$.fn.noosPopUp.defaults = {
			top:'150px',
			width:'830px',
			content:'',
			bg:'#000',
			opacity:0.6,
			zindex:99999,
			ajaxSuccess:function(){}
		}
	})(jQuery);
}