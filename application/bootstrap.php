<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;

if (is_file(APPPATH.'classes/Kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/Kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/Kohana'.EXT;
}



require_once DOCROOT.'vendor/autoload.php';



// Load Mustache https://github.com/bobthecow/mustache.php/wiki#setup
// php composer.phar require modulargaming/kostache -d ./modules/kostache/
//require MODPATH.'/vendor/mustache/mustache/src/Mustache/Autoloader.php';
//require DOCROOT.'/vendor/mustache/mustache/src/Mustache/Autoloader'.EXT;
Mustache_Autoloader::register();

// Load phpmorphy http://sono-design.ru/blog/programming/php_mysql
//require_once APPPATH.'/vendors/phpmorphy/src/common'.EXT;
//require_once DOCROOT.'/vendor/nqxcode/phpmorphy/src/common'.EXT;


/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('Europe/Moscow');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('ru-RU');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => FALSE
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'), Kohana_Log::DEBUG);

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/*
 * Создаем соль для куков http://forum.kohanaframework.org/discussion/8310/ko-3-1-pravilnoe-mesto-dlya-opredeleniya-cookiesalt-foobar/p1
 */
Cookie::$salt = 'goswami123456...';

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
	'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	'database'   => MODPATH.'database',   // Database access
	// 'database'   => DOCROOT.'vendor/kohana-postgresql/',   // Database access
	'image'      => MODPATH.'image',      // Image manipulation
	'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
    'ulogin'  => DOCROOT.'vendor/ulogin-Kohana',  // uLogin
    'kostache'  => DOCROOT.'vendor/zombor/kostache', // template engine
  	'userguide'  => MODPATH.'userguide',  // User guide and API documentation
    'pagination'  => DOCROOT.'vendor/shadowhand/pagination',
	'feedzeta' => DOCROOT.'vendor/zetacomponents/feed'
	));





/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */

Route::set('pages', '<alias>')
        ->filter(function($route, $params, $request)
        {
            return ORM::factory('Page', array('alias' => $params['alias']))->loaded();
        })
        ->defaults(array(
		'controller' => 'page',
		'action'     => 'index',
	));

Route::set('rss', 'podcast(/<action>)')
    ->defaults(array(
        'action' => 'audio',
        'controller' => 'podcast',
    ));

Route::set('catalogue', 'catalogue(/<page>)', array('page' => '\d{1,10}'))
	->defaults(array(
                'action' => 'index',
		'controller' => 'catalogue',
	));

Route::set('item', '(<controller>(/<id>))', array('id' => '\d{1,10}'))
	->defaults(array(
		'controller' => 'main',
		'action'     => 'index',
	));


Route::set('lang_default', '(<language>(/<controller>(/<action>(/<id>))))', array('language' => '(?:rus|eng)'))
	->defaults(array(
                'language' => 'rus',
		'controller' => 'main',
		'action'     => 'index',
	));

Route::set('admin_item', 'admin(/<controller>(/<id>))', array('id' => '\d{1,10}'))
	->defaults(array(
                'directory' => 'admin',
		'controller' => 'news',
		'action'     => 'index',
	));

Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
	->defaults(array(
                'directory' => 'admin',
		'controller' => 'jira',
		'action'     => 'index',
	));

Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'controller' => 'main',
		'action'     => 'index',
	));



