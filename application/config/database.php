<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'default' => array
	(
                'type'       => 'PDO_Postgre',
		'connection' => array(
			/**
			 * The following options are available for MySQL:
			 *
			 * string   hostname     server hostname, or socket
			 * string   database     database name
			 * string   username     database username
			 * string   password     database password
			 * boolean  persistent   use persistent connections?
			 * array    variables    system variables as "key => value" pairs
			 *
			 * Ports and sockets may be appended to the hostname
			 */
			'dsn'        => 'pgsql:host=localhost;port=5432;dbname=bvg',
                        'username'   => 'goswami.ru',
                        'password'   => '',
                        'persistent' => FALSE, 
			'charset'    => 'utf8',
		)
	),
	'alternate' => array(
		'type'       => 'PDO',
		'connection' => array(
			/**
			 * The following options are available for PDO:
			 *
			 * string   dsn         Data Source Name
			 * string   username    database username
			 * string   password    database password
			 * boolean  persistent  use persistent connections?
			 */
			'dsn'        => 'pgsql:host=goswami.bvgm.org;dbname=bvg',
			'username'   => 'goswami.ru',
			'password'   => '',
			'persistent' => FALSE,
		)
	),
);
