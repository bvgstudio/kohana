<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Media_Object extends ORM
{
    protected $_table_name = 'media';

    protected $_belongs_to = array(
    'tag_set' => array(
        'model'       => 'Tag_Set',
        'foreign_key' => 'tag_set_id'
    ),
    'venue' => array(
        'model'       => 'Venue',
        'foreign_key' => 'venue_id'
    ));

    protected $_has_many = array(
    'news' => array(
        'model'       => 'News',
        'through' => 'news_media_objects',
        'foreign_key' => 'media_object_id'
    ),
    'linked_medias' => array(
        'model'       => 'Media_Object',
        'through' => 'xref_media',
        'foreign_key' => 'media_id',
        'far_key' => 'linked_media_id',
    ),
    'master_medias' => array(
        'model'       => 'Media_Object',
        'through' => 'xref_media',
        'foreign_key' => 'linked_media_id',
        'far_key' => 'media_id',
    ),
    'authors' => array(
        'model'       => 'Author',
        'through' => 'media_authors',
        'foreign_key' => 'media_id'
    ));
    
    protected $_language = 'RUS';
    
    protected $_tags = NULL;

    public function __construct($id = NULL) {

        if(is_string($id) && !is_numeric($id))
        {
            parent::__construct(NULL);

            $this->_language = $id;

            $this->where('language', '=', $this->_language);
        }
        else
        {
            parent::__construct($id);
        }
        
        if(isset($this->_content_type)) {            
            $this->where('type', '=', $this->_content_type);
        }
    }
    
    public function get($column)
    {
        if($column == 'tags')
        {
            if(is_null($this->_tags) && !is_null($this->tag_set_id)) {
                $this->_tags = ORM::factory('Tag')->
                               join('tag_sets_tags')->on('tag.id', '=', 'tag_sets_tags.tag_id')->
                               where('tag_sets_tags.tag_set_id', '=', $this->tag_set_id);
            }
            
            return $this->_tags;
                
        }
        if($column == 'related_items')
        {
            /*  
             *  SELECT c.tag_set_id, count(*) as cnt
                FROM "public"."tag_sets_tags" c,
                (select tag_set_id, tag_id from "public"."tag_sets_tags" where tag_set_id = 2) t
                where c.tag_id = t.tag_id
                group by c.tag_set_id
                order by cnt desc
            */
            if(!is_null($this->tag_set_id)) {
                $related_tag_sets_ids = DB::select('c.tag_set_id', array(DB::expr('COUNT(*)'), 'cnt'))
                                            ->from(array('tag_sets_tags', 'c'),
                                                   array(DB::expr('(select tag_set_id, tag_id from tag_sets_tags where tag_set_id ='.$this->tag_set_id.')'), 't'))
                                            ->where('c.tag_id', '=', DB::expr('t.tag_id'))
                                            ->group_by('c.tag_set_id')
                                            ->order_by('cnt', 'DESC')->execute()->as_array(NULL, 'tag_set_id');
            
                if (count($related_tag_sets_ids) > 0) {
                    return ORM::factory('Media_Object')->where('tag_set_id', 'IN', $related_tag_sets_ids)
                                    ->and_where($this->primary_key(), '<>', $this->pk());
                }
            }
            
            return ORM::factory('Media_Object')->where('id', '<', 0);
        }
        
        // Если в базе нет размера файла, получаем его на основе url файла и сохраняем в базу.
        if($column == 'size' && !parent::get('size'))
        {
            $file_uri = parent::get('file_uri');
            if($file_uri != null) {
                $file_size = $this->_get_remote_file_size(parent::get('file_uri'));
                parent::set('size', $file_size);
                $this->save();

                return $file_size;
            }
            else {
                Kohana::$log->add(Kohana_Log::WARNING, 'Trying to get size of file for media with file_uri = NULL; OBJECT ID = '.$this->pk());
                return 0;
            }
        }
        
        return parent::get($column);
    }

    public function as_array() {
        $arr = parent::as_array();

        $arr['authors'] = $this->authors->as_array();
        
        if(isset($this->_content_type))
        {
            $arr['content_type'] = $this->_content_type;
        }
        
        $arr['tags'] = array();
        if(!is_null($this->tag_set_id)) {
            $tags = $this->tags->find_all();
            foreach($tags as $tag)
            {
                $arr['tags'][] = $tag->as_array();
            }
        }
        
        return $arr;
    }
    
    public function save(Validation $validation = NULL) {
        if(isset($this->_content_type))
        {
            $this->type = $this->_content_type;
        }
        
        parent::save($validation);
    }


    public function search($criteria_array) {
        // Make query to media objects table itself         
        foreach (array_keys($this->_table_columns) as $name)
        {
            if($name == $this->_primary_key) continue;

            $value = Arr::get($criteria_array, $name, FALSE);
            if ($value !== FALSE AND $value != '')
            {
                $this->where($name, 'ILIKE', '%'.$value.'%');
            }
        }

        $from_date = Arr::get($criteria_array, 'from_date', FALSE);
        $to_date = Arr::get($criteria_array, 'to_date', date('Y-m-d'));
        if($from_date !== FALSE) {
            $this->where('occurrence_date', 'BETWEEN', array($from_date, $to_date));
        }

        $content_types = Arr::get($criteria_array, 'content_types', FALSE);
        if($content_types !== FALSE) {
            $this->where('type', count($content_types) > 1 ? 'IN' : '=', $content_types);
        }
               
        if(key_exists('unwords', $criteria_array)) {
            $text_columns = array('title', 'body', 'teaser');
            
            foreach($criteria_array['unwords'] as $unword) {
                $this->where_open();
                for ($col_i = 0; $col_i < count($text_columns); $col_i++) {
                    if($col_i == 0) {
                        $this->where($text_columns[$col_i], 'LIKE', '%'.$unword.'%');
                    }
                    else {
                        $this->or_where($text_columns[$col_i], 'LIKE', '%'.$unword.'%');
                    }
                }
                $this->where_close();
            }
        }
        
        // Aggregating queries to other tables
        $queries = array();

        // Сначала проводим поиск с использованием "сторонних" таблиц search_words и tags
        $words = Arr::get($criteria_array, 'words', FALSE);
        if($words !== FALSE && count($words) > 0) {
            $queries[] = DB::select(array(DB::expr('SUM(search_words.weight)'), 'weight_sum'), 'media.id')
                            ->from('search_words')
                            ->join('media', 'INNER')->on('media.id', '=', 'search_words.media_id')
                            ->where('search_words.word', count($words) > 1 ? 'IN' : '=', $words)
                            ->group_by('media.id')
                            ->order_by('weight_sum', 'DESC');
        }

        $tags = Arr::get($criteria_array, 'tags', FALSE);
        if($tags !== FALSE && count($tags) > 0) {
            // SELECT media_objects.id
            // FROM `bvgstudio`.`media_objects` INNER JOIN `tag_sets_tags` ON `media_objects`.`tag_set_id` = tag_sets_tags.tag_set_id
            // JOIN tags on tag_sets_tags.tag_id = tags.id where tags.text IN ('first', 'second', 'КРИШНА', 'Книга')
            $queries[] = DB::select('media.id')
                            ->from('media')
                            ->join('tag_sets_tags', 'INNER')->on('media.tag_set_id', '=', 'tag_sets_tags.tag_set_id')
                            ->join('tags')->on('tag_sets_tags.tag_id', '=', 'tags.id')
                            ->where('tags.tag', count($tags) > 1 ? 'IN' : '=', $tags)->group_by('media.id');
        }

        $tag_ids = Arr::get($criteria_array, 'tag_ids', FALSE);
        if($tag_ids !== FALSE && count($tag_ids) > 0) {
            // SELECT media_objects.id
            // FROM `bvgstudio`.`media_objects` INNER JOIN `tag_sets_tags` ON `media_objects`.`tag_set_id` = tag_sets_tags.tag_set_id
            // where tag_sets_tags.tag_id IN (3,4) group by media_objects.id
            $queries[] = DB::select('media.id')
                            ->from('media')
                            ->join('tag_sets_tags', 'INNER')->on('media.tag_set_id', '=', 'tag_sets_tags.tag_set_id')
                            ->where('tag_sets_tags.tag_id', count($tags) > 1 ? 'IN' : '=', $tag_ids)->group_by('media.id');
        }

        $venues = Arr::get($criteria_array, 'venues', FALSE);
        if($venues !== FALSE && count($venues)) {
            // SELECT media_objects.id
            // FROM `bvgstudio`.`media_objects` INNER JOIN `venues` ON `media_objects`.`venue_id` = venue.id
            // where venue.name IN ()
            $queries[] = DB::select('media.id')
                            ->from('media')
                            ->join('venues', 'INNER')->on('media.venue_id', '=', 'venues.id')
                            ->where('venues.name', count($venues) > 1 ? 'IN' : '=', $venues);
        }
        
        $authors = Arr::get($criteria_array, 'authors', FALSE);
        if($authors !== FALSE && count($authors)) {
            // SELECT media.id
            // FROM media INNER JOIN media_authors ON media_authors.author_id = media.id
            // INNER JOIN authors ON authors.id = media_authors.author_id
            $queries[] = DB::select('media.id')
                            ->from('media')
                            ->join('media_authors', 'INNER')->on('media.id', '=', 'media_authors.media_id')
                            ->join('authors', 'INNER')->on('media_authors.author_id', '=', 'authors.id')
                            ->where('authors.name', count($authors) > 1 ? 'IN' : '=', $authors);
        }
        
        for ($index = 0; $index < count($queries); $index++) {
            $this->join(array($queries[$index], 'query'.$index), 'INNER')->on('media_object'.'.id', '=', 'query'.$index.'.id');
        }
        
        return $this;
    }
    
    protected function _get_remote_file_size($url)
    {

	    $parts = parse_url($url);
        $parts['path'] = rawurldecode($parts['path']);

	    if (!empty($parts['path'])) {
	    	$parts['path'] = implode('/', array_map('rawurlencode', explode('/', $parts['path']))   );
        }
            
	    $url = Arr::get($parts, 'scheme', 'http')."://".Arr::get($parts, 'host', 'bvgm.org').$parts['path'];
	    // https://p1ratrulezzz.me/2014/03/rasshirenie-pecl_http-v-2-bolshe-ne-podderzhivaet-http_build_url-reshaem-problemu-bystro.html
	    // http_build_url is removed from pecl_http
            //$url = http_build_url($parts);
            
            try {
               $head = array_change_key_case(get_headers($url, 1));

                // content-length of download (in bytes), read from Content-Length: field
                $clen = isset($head['content-length']) ? $head['content-length'] : 0;

                // cannot retrieve file size
                if (!$clen || strpos($head[0], '200') == false) {
                    return 0;
                } 
	    
                return $clen; // return size in bytes
            } catch (Exception $exc) {
                return 0;
            }
    }
}
