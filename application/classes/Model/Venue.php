<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Venue extends ORM
{
    protected $_table_name = 'venues';

    protected $_has_many = array(
    'media_objects' => array(
        'model'       => 'Media_Object',
        'foreign_key' => 'venue_id',
    ));
}
