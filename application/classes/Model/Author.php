<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Author extends ORM
{
    protected $_table_name = 'authors';
    
    protected $_has_many = array(
    'content_items' => array(
        'model'       => 'Media_Object',
        'through' => 'media_authors',
        'foreign_key' => 'author_id',
        'far_key' => 'media_id',
    ));


    public  function getList()    {

        $arrayAuthors = $this->find_all()->as_array();

        foreach ($arrayAuthors as $author)   {
            $arrayNames[] = $author->name;
        }

        if (! empty ($arrayNames) ) {
            return implode(',', $arrayNames);
        }
    }
}

