<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_News extends ORM
{
    protected $_table_name = 'news';
    
    protected $_belongs_to = array(
    'tag_set' => array(
        'model'       => 'Tag_Set',
        'foreign_key' => 'tag_set_id'
    ));
    
    protected $_has_many = array(
    'content_items' => array(
        'model'       => 'Media_Object',
        'through' => 'news_media_objects',
        'foreign_key' => 'news_id',
        'far_key' => 'media_id',
    ));
    
    public function get($column) {
        if($column == 'media_objects')
        {
            return ORM::factory('Media_Object')->select('media_object.*')->
                    join('news_media_objects')->on('media_object.id', '=', 'news_media_objects.media_id')->
                    where('news_media_objects.news_id', '=', $this->pk());
        }
        
        if($column == 'minor_media_objects')
        {
            return $this->media_objects->where('news_media_objects.major', '!=', '1');
        }
        
        if($column == 'major_media_object')
        {
            return $this->media_objects->where('major', '=', '1')->find();
        }
        
        if($column == 'tags')
        {
            if(!is_null($this->tag_set_id)) {
                return ORM::factory('Tag')->select('tag.*')->
                        join('tag_sets_tags')->on('tag.id', '=', 'tag_sets_tags.tag_id')->
                        where('tag_sets_tags.tag_set_id', '=', $this->tag_set_id);
            }
            
            return new ORM();
        }
        
        return parent::get($column);
    }
    
    public function as_array() {
        $arr = parent::as_array();
        
        $arr['tags'] = array();
        if(!is_null($this->tag_set_id)) {
            $tags = $this->tags->find_all();
            foreach($tags as $tag)
            {
                $arr['tags'][] = $tag->as_array();
            }
        }
        
        foreach($this->minor_media_objects->find_all() as $item)
        {
            $arr['minor_media_objects'][] = $item->as_array();
        }
        $arr['major_media_object'] = $this->major_media_object->as_array();
        
        return $arr;
    }
}

