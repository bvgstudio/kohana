<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Collection extends Model_Media_Object
{
    protected $_content_type  = 'collection';

    public function as_array() {
        $arr = parent::as_array();

        foreach($this->linked_medias->find_all() as $item)
        {
            $arr['linked_medias'][] = $item->as_array();
        }

        return $arr;
    }
    
    public function get($column)
    {
        if($column == 'linked_medias')
        {
            /*
             select media.*, col_media.sequence_number from media
             join xref_media on xref_media.linked_media_id = media.id
             left join (select * from collection_media_seq where collection_id = 6) as col_media on col_media.media_id = media.id
             where xref_media.media_id = 6
             order by col_media.sequence_number
             */
            $children_model = ORM::factory('Media_Object');
            
            $col_seq_query = DB::select()->from('collection_media_seq')->where('collection_id', '=', $this->pk());
            
            return $children_model->select('media_object.*', 'col_media.sequence_number')
                                  ->join('xref_media')->on('xref_media.linked_media_id', '=', 'media_object.id')
                                  ->join(array($col_seq_query, 'col_media'), 'LEFT')->on('col_media.media_id', '=', 'media_object.id')
                                  ->where('xref_media.media_id', '=', $this->pk())
                                  ->order_by('col_media.sequence_number');
                    
        }
        return parent::get($column);
    }
}

