<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Audio extends Model_Media_Object
{
    protected $_content_type  = 'audio';
    
    public function as_array() {
        $arr = parent::as_array();

        foreach($this->master_medias->where('type', '=', 'collection')->find_all() as $item)
        {
            $arr['collections'][] = $item->item->as_array();
        }

        return $arr;
    }
}

