<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Auth_User_Data extends ORM
{
    protected $_table_name  = 'user_data';
    
    protected $_belongs_to = array('user' => array('model' => 'Auth_User'));
    
    public function get($column)
    {
        if($column == 'birth_date') {
            $value = parent::get('birth_date');
            
            $dt = DateTime::createFromFormat('d.m.Y', $value);
            if($dt) $value = $dt->format('Y-m-d');
            
            return $value;
        }
    
        return parent::get($column);
    }
    
    public function set($column, $value)
    {
        if($column == 'birth_date') {
            if(!$value) {
                parent::set($column, null);
                return;
            }
            
            $dt = DateTime::createFromFormat('d.m.Y', $value);
            if($dt) $value = $dt->format('Y-m-d');
        }
    
        parent::set($column, $value);
    }
}

