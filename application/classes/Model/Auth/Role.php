<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Auth_Role extends ORM
{
    protected $_table_name  = 'roles';

    protected $_has_many = array( 'users' => array( 'model' => 'Auth_User', 'through' => 'roles_users', 'foreign_key' => 'role_id' ) );
}
