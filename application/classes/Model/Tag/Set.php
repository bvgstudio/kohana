<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Tag_Set extends ORM
{
    protected $_table_name = 'tag_sets';
    
    protected $_has_many = array(
    'content_items' => array(
        'model'       => 'Media_Object',
        'foreign_key' => 'tag_set_id',
    ),
    'tags' => array(
        'model' => 'Tag',
        'through' => 'tag_sets_tags',
        'foreign_key' => 'tag_set_id'
    ));
}
