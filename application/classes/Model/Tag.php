<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Tag extends ORM
{
    protected $_table_name = 'tags';
    
    protected $_has_many = array(
    'tag_sets' => array(
        'model'       => 'Tag_Set',
        'through' => 'tag_sets_tags',
        'foreign_key' => 'tag_id'
    ));
    
     /**
     * Handles getting of column
     * Override this method to add custom get behavior
     *
     * @param   string $column Column name
     * @throws Kohana_Exception
     * @return mixed
    */
    public function get($column)
    {
        if($column == 'media_objects')
        {
            return ORM::factory('Media_Object')->select('media.*')->
                    join('tag_sets_tags')->on('media.tag_set_id', '=', 'tag_sets_tags.tag_set_id')->
                    where('tag_sets_tags.tag_id', '=', $this->pk());
        }
        
        return parent::get($column);
    }

    public function getList()
    {
        if (!is_null($this->tag_set_id)) {
            $tags = $this->tags->find_all();
            foreach ($tags as $tag) {
                $tagsArray[] = $tag->tag;
            }
        }

        return implode(',', $tagsArray);
    }
}
