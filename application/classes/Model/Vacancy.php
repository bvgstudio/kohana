<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Vacancy extends ORM
{
    protected $_belongs_to = array(
    'contact' => array(
        'model'       => 'Auth_User',
        'foreign_key' => 'contact_id'
    ));
}

