<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Jira_Item
{   
    public $key;
    public $title;
    public $description;
    public $occurrence_date;
    
    public $authors;
    public $tags;
    
    public $is_loaded = false;


    private static $_factory_map = array(
        'publish' => 'collection',
        'roadmap' => 'audio'
    );
    
    protected static $_map_jira_props = array(
        'summary' => 'title',
        'labels' => 'tags',
        'project.key' => 'type',
        'customfield_10510' => 'tags', // for disks aka PUBLISH
        'customfield_10911' => 'file_uri',
        'description' => 'description',
        'customfield_10062' => 'venue',
        'customfield_10050' => 'occurrence_date',
        'customfield_10710' => 'occurrence_date',  // for disks aka PUBLISH
        'customfield_10064' => 'duration',
        'customfield_10053.value' => 'bitrate',
        'customfield_10412' => 'authors',
        'customfield_10419' => 'authors'   // for disks aka PUBLISH
    );
    
    protected static $jql = array(
      'audio' => '(project="ROADMAP"%20and%20type="Обработанная%20лекция"%20and%20resolution="Разрешен"%20and%20"Опубликовано%20на%20сайте(техническое)"%20in%20(0,null)%20and%20"Не%20публиковать%20на%20сайте"%20is%20EMPTY)',
      'collections' => '(type=Диск%20and%20"Опубликовано%20на%20сайте(техническое)"%20in%20(0,null)%20and%20status=Closed%20and%20"Не%20публиковать%20на%20сайте"%20is%20EMPTY)'  
    );

    protected static function jira_request($jira_api_url, $method) {
        $config = Kohana::$config->load('jira');
        $url = $config->get('url').'rest/api/latest/'.$jira_api_url;
        
        $credentials = $config->get('user').':'.$config->get('password');
        
        $request = Request::factory($url)->method($method)->headers(array(
            'Authorization' => 'Basic '.base64_encode($credentials),
            'Content-Type' => 'application/json'
        ));
        
        return $request;
    }
    
    protected static function jql_request($jql_query, $fields = NULL, $props = NULL) {
        if(is_null($props))
            $props = Model_Jira_Item::$_map_jira_props;

        if(is_null($fields)) {
            $fields = array_map(function ($val) { return explode('.', $val)[0]; }, 
                                array_keys($props));
        }
        
        return Model_Jira_Item::jira_request('search?jql='.$jql_query.'&fields='.implode(',', $fields), Request::GET);
    }
    
        
    public static function all($type = NULL) {
        $items_array = array();
        $properties = null;

        switch ($type) {
            case 'collections':
                $properties = Model_Jira_Collection::get_props();
                $jql = Model_Jira_Item::$jql[$type];
                break;

            case 'audio':
                $properties = Model_Jira_Audio::get_props();
                $jql = Model_Jira_Item::$jql[$type];
                break;
            default:
                $jql = implode('%20or%20', Model_Jira_Item::$jql);
                break;
        }
        
        $request = Model_Jira_Item::jql_request($jql, null, $properties);
        
        $response = $request->execute();
                
        if($response->status() == 200) {
            $items_array = Model_Jira_Item::parse_json(json_decode($response->body(), true));
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }
        
        return $items_array;
    }

    protected static function parse_json($json) {
        if(count($json['issues']) == 0) return array();
        
        $items_array = array();
        
        foreach ($json['issues'] as $issue) {
            $item = Model_Jira_Item::factory($issue['key']);
                        
            $item->_fillItemFields($issue);            
            $items_array[] = $item;
        }
        
        return $items_array;
    }   
    
    
    public static function factory($key) {
        $jira_item_type = explode('-', $key)[0]; 
        // Set class name
        
        $model = 'Model_Jira_'.ucfirst(Model_Jira_Item::$_factory_map[strtolower($jira_item_type)]);

        return new $model($key);
    }
    
    
    
    public function __construct($key) {
        $this->key = $key;
    }
    
    public function load($props_map = NULL) {
        $request = Model_Jira_Item::jql_request('key="'.$this->key.'"', null, $props_map);
        
        $response = $request->execute();
                
        if($response->status() == 200) {
            $response_json = json_decode($response->body(), true);
            $this->_fillItemFields($response_json['issues'][0], $props_map);
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }
        
        return $this;
    }
    
    protected function _fillItemFields($issue, $props_map = NULL) {
        if (is_null($props_map))
            $props_map = Model_Jira_Item::$_map_jira_props;
        
        // Проходим по $_map_jira_props и в соответствии с ним мапим json из Джиры на свойства текущего объекта
        // Например 'customfield_10053.value' => 'bitrate' из $_map_jira_props означает,
        // что $this->bitrate будет присвоено значение из $issue[fields][customfield_10053][value]
        foreach ($props_map as $json_field => $object_field) {
            $json_field_path_array = explode('.', $json_field);
            
            $is_second_level_field = count($json_field_path_array) == 2;
            
            $has_value = $is_second_level_field ?
                         array_key_exists($json_field_path_array[0], $issue['fields']) &&
                         !is_null($issue['fields'][$json_field_path_array[0]]) &&
                         array_key_exists($json_field_path_array[1], $issue['fields'][$json_field_path_array[0]]) &&
                         !empty($issue['fields'][$json_field_path_array[0]][$json_field_path_array[1]])
                         :
                         array_key_exists($json_field_path_array[0], $issue['fields']) && !empty($issue['fields'][$json_field_path_array[0]]);
            
            if(!$has_value) continue;
            
            $this->$object_field = $is_second_level_field ?
                                   $issue['fields'][$json_field_path_array[0]][$json_field_path_array[1]]
                                   :
                                   $issue['fields'][$json_field_path_array[0]];
        }
        
        $this->_fieldsPostProcessing();
        
        $this->is_loaded = true;
    }
    
    protected function _fieldsPostProcessing() {
        if(isset($this->description)) {
            $this->description = $this->_cleanJiraMarkup($this->description);
        }
        
        if(isset($this->authors)) {
            if(is_array($this->authors)) {
                $authors_vals = array();
                foreach ($this->authors as $author) {
                    $authors_vals[] = $author['value'];
                }
                $this->authors = $authors_vals;
            }
            else if(is_string($this->authors)){
                $this->authors = explode(',', $this->authors);
            }
        }
        
        if(isset($this->file_uri)) {
            $this->file_uri = strip_tags($this->file_uri);
        }
        
        if(isset($this->tags)) {
            if(!is_array($this->tags)) {
                $this->tags = explode(',', $this->tags);
            }
            
            $normalized_tags = array();
            foreach ($this->tags as $tag) {
                $normalized_tags[] = trim(str_replace('_', ' ', $tag));
            }
            $this->tags = $normalized_tags;        
        }
    }
    
    protected function _cleanJiraMarkup($text) {
        return str_replace('{html}', '', $text);
    }
    
    public function parents() {
        // Override it
        return array();
    }
    
    public function children() {
        // Override it
        return array();
    }
    
    public function set_commited_status() {
        $request = Model_Jira_Item::jira_request('issue/'.$this->key,
                   Request::PUT);
                
        $request->body(json_encode(array(
                'fields' => array(
                    'customfield_11010' => 1
                )
        )));
        
        $response = $request->execute();
        if(floor($response->status() / 100) == 2) { // 2xx Success statuses
            return true;
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200 || 204: '.print_r($response));
            return $response->status();
        }
    }
    
    public function set_rejected_status($comment) {
        $request = Model_Jira_Item::jira_request('issue/'.$this->key,
                   Request::PUT);
                
        $request->body(json_encode(array(
                'fields' => array(
                    'customfield_11010' => 0
                )
        )));
        
        $response = $request->execute();
        if(floor($response->status() / 100) == 2) { // 2xx Success statuses
            if ($comment == NULL) return true;
            
            $comment_request = Model_Jira_Item::jira_request('issue/'.$this->key.'/comment',
                   Request::POST);

            $comment_request->body(json_encode(array('body' => $comment)));

            $response = $comment_request->execute();

            if(floor($response->status() / 100) == 2) { // 2xx Success statuses
                return true;
            }
            else {
                Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
                return $response->status();
            }
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
            return $response->status();
        }
    }

    public function push_media_object_to_db($parent_key = NULL, $map_props = NULL) {
        if(!$this->is_loaded) {
            $this->load(is_null($map_props) ? Model_Jira_Item::$_map_jira_props : $map_props);
        }
        
        $media_object = ORM::factory('Media_Object', array('jira_ref' => $this->key));
        
        $media_object->jira_ref = $this->key;
        
        $media_object->type = $this->content_type;
        $media_object->language = 'RUS';
        
        $media_object->title = $this->title;
        $media_object->body = $this->description;
        if(property_exists($this, 'teaser')) $media_object->teaser = $this->teaser;

        $media_object->occurrence_date = $this->occurrence_date;
        $media_object->issue_date = date(DATE_ATOM);
        
        // $media_object->publisher = $this->publisher;
        
        if(property_exists($this, 'file_uri')) {
            $media_object->file_uri = $this->file_uri;
            // sometimes from jira comes duration like "01:57:3912"
            $media_object->duration = strlen($this->duration) > 8 ? substr($this->duration, 0, 8) : $this->duration;
        }
        
        $isChildDisk = $this->content_type == 'collection' && !is_null($parent_key);
        $media_object->visible = !$isChildDisk;
        
        $this->_setBelongsToFields($media_object);
        
        $media_object->save();
        
        $this->_setHasManyFields($media_object);
        
        $this->_setRelations($media_object, $parent_key);
        
        return $media_object;
    }
    
    protected function _setBelongsToFields($media_object) {
        if(isset($this->tags) && is_array($this->tags)) {
            sort($this->tags);
            $tags_string = join(', ', $this->tags);

            $tag_set_model = ORM::factory('Tag_Set', array('tag_set_name' => $tags_string));
            if(!$tag_set_model->loaded()) {
                $tag_set_model = ORM::factory('Tag_Set');
                $tag_set_model->tag_set_name = $tags_string;
                $tag_set_model->save();   
                 
                foreach ($this->tags as $tag) {
                    $tag_model = ORM::factory('Tag', array('tag' => $tag));

                    if(!$tag_model->loaded()) {
                         $tag_model = ORM::factory('Tag');
                         $tag_model->tag = $tag;
                         $tag_model->save();
                    }

                    $tag_set_model->add('tags', $tag_model);
                }
            }
            $media_object->tag_set_id = $tag_set_model->pk();
        }
        else {
            $media_object->tag_set_id = NULL;
        }
        
        if(isset($this->venue)) {
            $venue_model = ORM::factory('Venue', array('name' => $this->venue));

            if(!$venue_model->loaded()) {
                 $venue_model = ORM::factory('Venue');
                 $venue_model->name = $this->venue;
                 $venue_model->save();
            }
            $media_object->venue_id = $venue_model->pk();
        }
        else {
            $media_object->venue_id = NULL;
        }
    }
    
    protected function _setHasManyFields($media_object) {
        if(isset($this->authors) && is_array($this->authors)) {
            $media_object->remove('authors');
            
            foreach ($this->authors as $author) {
                $author_model = ORM::factory('Author', array('name' => $author));

                if(!$author_model->loaded()) {
                     $author_model = ORM::factory('Author');
                     $author_model->name = $author;
                     $author_model->save();
                }

                $media_object->add('authors', $author_model);
            }
        }        
    }
    
    protected function _setRelations($media_object, $parent_key) {
        $media_object->remove('master_medias');
        
        if(!is_null($parent_key)) {
            $parent_keys = array($parent_key);
        }
        else {
            $parents = $this->parents();
            if(is_null($parents) || count($parents) == 0) return;
            
            $parent_keys = array_map(function ($val) { return $val->key; }, $parents);
        }
        
        $parent_model = ORM::factory('Media_Object')
                        ->where('jira_ref', count($parent_keys) > 1 ? 'IN' : '=', $parent_keys)->find_all();

        foreach ($parent_model as $pmodel) {
            $media_object->add('master_medias', $pmodel);
            
            if(property_exists($this, 'parent_disk_position')){
                $parent_disk_position = is_array($this->parent_disk_position) ?
                                        $this->parent_disk_position[$pmodel->jira_ref] : $this->parent_disk_position;
                
                DB::delete('collection_media_seq')->where('collection_id', '=', $pmodel->pk())
                                                  ->and_where('media_id', '=', $media_object->pk())->execute();
                
                DB::insert('collection_media_seq', array('collection_id', 'media_id', 'sequence_number'))
                                          ->values(array($pmodel->pk(), $media_object->pk(), $parent_disk_position))->execute();
            }
        }
    }
}
