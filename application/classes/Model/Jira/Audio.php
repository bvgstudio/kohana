<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Jira_Audio extends Model_Jira_Item {
    public $type = 'ROADMAP';
    public $content_type = 'audio';
    
    public $file_uri;
    
    
    protected static $_map_jira_props = array(
        'summary' => 'title',
        'labels' => 'tags',
        'project.key' => 'type',
        'customfield_10911' => 'file_uri',
        'description' => 'description',
        'customfield_10410.value' => 'content_type_jira',
        'customfield_10062' => 'venue',
        'customfield_10050' => 'occurrence_date',
        'customfield_10064' => 'duration',
        'customfield_10053.value' => 'bitrate',
        'customfield_10412' => 'authors',
        'customfield_12210' => 'video', // url to video
        'customfield_11110' => 'transcription' // transcribed class
    );
    protected static $fields = 'key,labels,summary,customfield_10911,description,customfield_10412,customfield_10410,customfield_10062,customfield_10050,customfield_10064,customfield_10053,customfield_10510,customfield_10419,customfield_10710,project';
    
    protected function _fillItemFields($issue, $props_map = NULL) { 
        parent::_fillItemFields($issue, Model_Jira_Audio::$_map_jira_props);
    }

    public static function get_props()    {
        return Model_Jira_Audio::$_map_jira_props;
    }

    public function parents() {
        $items_array = array();
        
        $request = Model_Jira_Item::jql_request('"Основан%20на"='.$this->key, array('customfield_11514', 'parent'));

        $response = $request->execute();

        if($response->status() == 200) {
            $json = json_decode($response->body(), true);

            $disks_connections_count = count($json['issues']);

            switch($disks_connections_count){
                case 0:
                    break;
                case 1:
                    $this->disk_key = $json['issues'][0]['fields']['parent']['key'];
                    $this->parent_disk_position = intval($json['issues'][0]['fields']['customfield_11514']);

                    $parent = Model_Jira_Item::factory($this->disk_key);
                    $parent->_fillItemFields($json['issues'][0]['fields']['parent'], Model_Jira_Audio::$_map_jira_props);
                    $items_array[] = $parent;
                    break;
                default:
                    foreach ($json['issues'] as $issue) {
                        $this->disk_key[] = $issue['fields']['parent']['key'];
                        $this->parent_disk_position[$issue['fields']['parent']['key']] = intval($issue['fields']['customfield_11514']);

                        $parent = Model_Jira_Item::factory($issue['fields']['parent']['key']);
                        $parent->_fillItemFields($json['issues'][0]['fields']['parent'], Model_Jira_Audio::$_map_jira_props);
                        $items_array[] = $parent;
                    }
                    break;
            }
        }            
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }

        return $items_array;
    }
    
    function fillDiskAudioFields($parent_key, $issue) {
        // load all fields for ROADMAP items 
        parent::load(Model_Jira_Audio::$_map_jira_props);
        $this->disk_key = $parent_key;
        
        // replace with values from PUBLISH item
        $this->title = $issue['fields']['summary'];
        $this->duration =  $issue['fields']['customfield_11512'];
        $this->file_uri = strip_tags($issue['fields']['customfield_11513']);
        $this->parent_disk_position = intval($issue['fields']['customfield_11514']);
        
        $this->is_loaded = true;
    }


    public function push_media_object_to_db($parent_key = NULL, $map_props = NULL)
    {
        if (!$this->is_loaded)
            $this->load(Model_Jira_Audio::$_map_jira_props);

        if(property_exists($this, 'transcription')) {
            $this->teaser = $this->description;
            $this->description = $this->transcription;
        }

        $audio_object = parent::push_media_object_to_db($parent_key, Model_Jira_Audio::$_map_jira_props);


        if(property_exists($this, 'video')) {
            // todo: проверить не существует ли уже такая запись видео в media_data
            $audio_parameters = DB::select()->
                from('media_data')->
                where('media_id', '=', $audio_object->pk())->
                execute();

            if($audio_parameters->count() == 0 )  {
                DB::insert('media_data', array('media_id', 'data_type', 'value'))
                    ->values(array($audio_object->pk(), 'video', $this->video))->execute();
            }
        }

        return $audio_object;
    }

}
