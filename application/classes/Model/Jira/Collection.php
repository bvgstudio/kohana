<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Jira_Collection extends Model_Jira_Item
{
    public $type = 'PUBLISH';
    public $content_type = 'collection';
    
    protected static $_map_jira_props = array(
        'summary' => 'title',
        'project.key' => 'type',
        'customfield_10510' => 'tags',
        'customfield_10911' => 'file_uri',
        'description' => 'description',
        'customfield_10410.value' => 'content_type',
        'customfield_10062' => 'venue',
        'customfield_10710' => 'occurrence_date',
        'customfield_10064' => 'duration',
        'customfield_10419' => 'authors'
    );    

    public static function get_props()    {
        return Model_Jira_Collection::$_map_jira_props;
    }

    protected function _fillItemFields($issue, $props_map = NULL) { 
        parent::_fillItemFields($issue, Model_Jira_Collection::$_map_jira_props);
    }
    
    // http://jira.bvgm.org:8888/browse/BVGSITE-39?focusedCommentId=15564&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-15564
    public function child_audio() {
        $items_array = array();

        $request = Model_Jira_Item::jql_request('parent='.$this->key.'&maxResults=1000', 
                                                array('summary', 'customfield_11516', 'customfield_11512', 'customfield_11513', 'customfield_11514'));
        
        $response = $request->execute();
                
        if($response->status() == 200) {
            $json = json_decode($response->body(), true);
            
            if(count($json['issues']) == 0) return array();
            
            foreach ($json['issues'] as $issue) {
                $key_field = $issue['fields']['customfield_11516'];
                $child_key = is_array($key_field) ? $key_field[0] : $key_field;
                
                $item = new Model_Jira_Audio($child_key);
                $item->fillDiskAudioFields($this->key, $issue);            
                
                if(!is_null($item->parent_disk_position)) {
                    $items_array[$item->parent_disk_position] = $item;
                }
                else {
                    $items_array[] = $item;
                }
            }
            
            ksort($items_array);
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }
        
        return $items_array;
    }
    
    public function child_disks() {        
        $items_array = array();

        $request = Model_Jira_Item::jql_request('issue+in+linkedIssues('.$this->key.')%20and%20type="Диск связанный"');
        
        $response = $request->execute();
                
        if($response->status() == 200) {
            $items_array = Model_Jira_Item::parse_json(json_decode($response->body(), true));
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }
        
        return $items_array;
    }
    
    public function children() {
        return array_merge($this->child_disks(), $this->child_audio());
    }
    
    public function parents() {
        $items_array = array();
    
        $request = Model_Jira_Item::jql_request('issue+in+linkedIssues('.$this->key.',%20"связан%20с")');

        $response = $request->execute();

        if($response->status() == 200) {
            $items_array = Model_Jira_Item::parse_json(json_decode($response->body(), true));
        }
        else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Request to JIRA returns with response code != 200: '.print_r($response));
        }

        return $items_array;
    }
    
    protected function _setRelations($media_object, $parent_key) {        
        $media_object->remove('linked_medias');
        
        $children = $this->children();
        
        if(count($children)) {
            $children_keys = array_map(function ($val) { return $val->key; }, $children);
            $children_model = ORM::factory('Media_Object')
                              ->where('jira_ref', count($children_keys) > 1 ? 'IN' : '=', $children_keys)->find_all();

            foreach ($children_model as $cmodel) {
                $media_object->add('linked_medias', $cmodel);
            }
        }
        
        parent::_setRelations($media_object, $parent_key);
    }

    public function push_media_object_to_db($parent_key = NULL, $map_props = NULL)
    {
        if (!$this->is_loaded)
            $this->load(Model_Jira_Collection::$_map_jira_props);

        return parent::push_media_object_to_db($parent_key, Model_Jira_Collection::$_map_jira_props);
    }
}
