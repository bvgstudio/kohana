<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Page extends ORM
{
    protected $_table_name = 'pages';
    
    protected $_belongs_to = array(
    'parent' => array(
        'model'       => 'Page',
        'foreign_key' => 'parent_id'
    ));
    
    protected $_has_many = array(
    'children' => array(
        'model'       => 'Page',
        'foreign_key' => 'parent_id',
    ));
}

