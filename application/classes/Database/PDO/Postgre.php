<?php defined('SYSPATH') or die('No direct script access.');

class Database_PDO_Postgre extends Database_PDO {
    public function list_columns($table, $like = NULL, $add_prefix = TRUE)
    {
            // Quote the table name
            $table = $this->quote($table);

            if (is_string($like))
            {
                    // Search for column names
                    $result = $this->query(Database::SELECT, 'SELECT * from INFORMATION_SCHEMA.COLUMNS where table_name = '.$table.' and column_name LIKE '.$this->quote($like), FALSE);
            }
            else
            {
                    // Find all column names
                    $result = $this->query(Database::SELECT, 'SELECT * from INFORMATION_SCHEMA.COLUMNS where table_name = '.$table, FALSE);
            }

            $count = 0;
            $columns = array();
            foreach ($result as $row)
            {                    
                    $type = $row['data_type'];
                    $column = $this->datatype($type);

                    $column['column_name']      = $row['column_name'];
                    $column['column_default']   = $row['column_default'];
                    $column['data_type']        = $type;
                    $column['is_nullable']      = ($row['is_nullable'] == 'YES');
                    $column['ordinal_position'] = $row['ordinal_position'];

                    switch ($type)
                    {
                            case 'float':
//                                    if (isset($length))
//                                    {
//                                            list($column['numeric_precision'], $column['numeric_scale']) = explode(',', $length);
//                                    }
                            break;
                            case 'integer':
                                $column['data_type'] = 'int';
                            break;
                            case 'character varying':
                            case 'character':
                                $column['character_maximum_length'] = $row['character_maximum_length'];
                            case 'text':
                            case 'USER-DEFINED':
                                $column['data_type'] = 'string';
                            break;
                    }

                    // MySQL attributes
//                    $column['comment']      = $row['Comment'];
//                    $column['extra']        = $row['Extra'];
//                    $column['key']          = $row['Key'];
//                    $column['privileges']   = $row['Privileges'];

                    $columns[$column['column_name']] = $column;
            }

            return $columns;
    }
    
    public function query($type, $sql, $as_object = FALSE, array $params = NULL) {
        $base_return = parent::query($type, $sql, $as_object, $params);
        
        if ($type === Database::INSERT)
        {
            // PDO_PGSQL() requires you to specify the name of a sequence object for the lastInsertId() name parameter. 
            // http://stackoverflow.com/a/10492645/186607            
            $table_matches = array();
            preg_match("/\s?INSERT INTO\s+([a-z\d_]+)/", $sql, $table_matches);
            $table_name = $table_matches[1];

            if(key_exists('id', $this->list_columns($table_name))) {
                $seq_name = $table_name.'_id'.'_seq';
                $base_return[0] = $this->_connection->lastInsertId($seq_name);
            }
        }
        
        return $base_return;
    }
}