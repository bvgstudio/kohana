<?php defined('SYSPATH') OR die('No direct access allowed.');

class View_Audio extends View_ContentItem
{
    public $file_uri;
    public $venue;
    public $verbal_size;
    
    public $is_in_collection;
    public $collections;
    public $video_version_link;
    
    public function __construct($id, $is_attached = FALSE) {
        parent::__construct(new Model_Audio($id), $is_attached);
    }

    protected function _init()
    {
        parent::_init();

        $this->file_uri = $this->_model->file_uri;
        $this->venue = $this->_model->venue;
        $this->verbal_size = $this->_get_file_size($this->_model->size);
        
        $master_collections = $this->_model->master_medias->where('type', '=', 'collection')->find_all();
        $this->is_in_collection = count($master_collections) > 0;
        
        if($this->is_in_collection) {
            foreach($master_collections as $item)
            {
                $arr_item = $item->as_array();
                $arr_item['link'] = Route::get('default')->uri(array(
                                                                'controller' => 'collection',
                                                                'id' => $item->id)
                                                                );

                $this->collections[] = $arr_item;
            }
        }
        
        $master_videos = $this->_model->master_medias->where('type', '=', 'video')->find_all();
        
        $video_version_item = count($master_videos) > 0 ? $master_videos[0] : $this->_model->linked_medias->where('type', '=', 'video')->find();
        
        if($video_version_item->loaded())
        {
            $this->video_version_link = Route::get('default')->uri(array(
                                                                        'controller' => 'video',
                                                                        'id' => $video_version_item->id)
                                                                        );
        }
    }

    protected function _get_file_size($size)
    {
        if ($size >= 1073741824)
        {
            $gbSize = $size / 1073741824;
            return sprintf("%01.2f ГБ", $gbSize);
        }
        else if ($size >= 1048576)
        {
            $mbSize = $size / 1048576;
            return sprintf("%01.2f МБ", $mbSize);
        }
        else if ($size >= 1024)
        {
            $kbSize = $size / 1024;
            return sprintf("%01.2f КБ", $kbSize);
        }
        else if ($size > 0 & $size < 1024)
        {
            return sprintf("%d байт", $size);
        }
        else
        {
            return "0 байт";
        }
    }
}
