<?php defined('SYSPATH') or die('No direct script access.');

class View_ContentItemList extends View_Base
{
    public $model;
    public $items;
            
    public function __construct($model, $offset = 0, $items_count = 20) {
        parent::__construct();
        
        if(in_array('occurrence_date', $model->table_columns()))
        {
            $model->order_by('occurrence_date', 'DESC');
        }
        
        $this->model = $model->offset($offset)->limit($items_count);
    }

    public function items()
    {
        if(!isset($this->items)) {
            $this->items = array();

            foreach($this->model->find_all() as $item)
            {
                if(($item instanceof ORM)) { // $item->media_object->published) {
                    $this->items[] = $this->_add_view_data($item);
                }
            }
        }
        return $this->items;
    }

    protected function _add_view_data($item)
    {
        $item_arr = $item->as_array();

        $item_arr['link'] = Route::get('item')->uri(array('controller' => $item->type, 'id' => $item_arr['id']));
        
        if(!is_null($item->tag_set_id)) {
            $tags = $item->tags->find_all();
            foreach ($tags as $tag)
            {
                $tag_arr = $tag->as_array();
                $tag_arr['link'] = 'catalogue/?tags='.urlencode($tag_arr['tag']);
                $item_arr['tags'][] = $tag_arr;
            }
        }

        if(array_key_exists('teaser', $item_arr) && empty($item_arr['teaser']))
        {
            $item_arr['teaser'] = $this->ellipsis(strip_tags($item_arr['body']), 1000);
        }

        if(array_key_exists('occurrence_date', $item_arr)&& $item_arr['occurrence_date'] != NULL)
        {
            $date = strtotime($item_arr['occurrence_date']);
            $item_arr['date'] = strftime('%d.%m.%Y', $date);
        }

        return $item_arr;
    }

    /**
    * Function to ellipse-ify text to a specific length
    *
    * @param string $text   The text to be ellipsified
    * @param int    $max    The maximum number of characters (to the word) that should be allowed
    * @param string $append The text to append to $text
    * @return string The shortened text
    * @author Brenley Dueck
    * @link   http://www.brenelz.com/blog/2008/12/14/creating-an-ellipsis-in-php/
    */
   protected function ellipsis($text, $max=100, $append='&hellip;')
   {
       $substitution_arr = array(
           '|<br /><br />|' => ' ',
           '|&nbsp;|' => ' ',
           '|&rsquo;|' => "'",
           '|&lsquo;|' => "'",
           '|&ldquo;|' => '"',
           '|&rdquo;|' => '"',
       );
       
       if (strlen($text) <= $max) return $text;

       $replacements = array_values($substitution_arr);

       $patterns = array_keys($substitution_arr);

       $text = preg_replace($patterns, $replacements, $text); // convert double newlines to spaces
       
       $out = substr($text, 0, $max);
       if (strpos($text, ' ') === false) return $out.$append;
       return preg_replace('/(\W)&(\W)/', '$1&amp;$2', (preg_replace('/\W+$/', ' ', preg_replace('/\w+$/', '', $out)))) . $append;
   }

}