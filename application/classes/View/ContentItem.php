<?php defined('SYSPATH') or die('No direct script access.');

class View_ContentItem extends View_Base
{
    public $is_content_page = TRUE;

    protected $_model;

    public $id;
    public $content_type;
    
    public $title;
    public $link;
    
    public $text;
    public $teaser;
    
    public $tags;
    public $authors;

    public $date;
    
    public $related_items;

    public $is_attached = false;

    public function __construct($model, $is_attached = false) {
        parent::__construct();
        
        $this->_model = $model;
        $this->is_attached = $is_attached;
        
        $this->_init();
    }
    
    protected function _init()
    {
        $this->id = $this->_model->pk();
        $this->link = Route::get('item')->uri(array('controller' => $this->_model->type, 'id' => $this->_model->pk()));
        $this->title = $this->_model->title;
        $this->content_type = $this->_model->type;
        
        if(!is_null($this->_model->tag_set_id)) {
            $tags = $this->_model->tags->find_all();
            
            foreach ($tags as $tag)
            {
                $tag_arr = $tag->as_array();
                $tag_arr['link'] = 'catalogue/?tags='.urlencode($tag_arr['tag']);
                $this->tags[] = $tag_arr;
            }
        }
        
        $this->authors = $this->_model->authors->find_all()->as_array();
        
        $this->text = $this->_model->body;
        $this->teaser = $this->_model->teaser;
        
        if(empty($this->teaser))
        {
            $this->teaser = $this->ellipsis(strip_tags($this->text), 1000);
        }

        if($this->_model->occurrence_date != NULL)
        {
            $date = strtotime($this->_model->occurrence_date);
            if ($date != strtotime('01.08.1956')) {
                $this->date = strftime('%d.%m.%Y', $date);
            }
            else {
                $this->date = 'Дата неизвестна';
            }
        }
    }
    
    public function related_items() {
        if(!isset($this->related_items))
        {
            foreach ($this->_model->related_items->limit(5)->find_all() as $item) {
                $arr_item = $item->as_array();
                $arr_item['link'] = Route::get('default')->uri(array(
                    'controller' => $item->type,
                    'id' => $item->id)
                );
                $arr_item['venue'] = $item->venue;
                $arr_item['authors'] = $item->authors->find_all()->as_array();
                
                $arr_item['is_picture_view'] = $item->type == 'book' || $item->type == 'collection';
                
                $this->related_items[] = $arr_item;
            }
        }
        
        return $this->related_items;
    }

    /**
    * Function to ellipse-ify text to a specific length
    *
    * @param string $text   The text to be ellipsified
    * @param int    $max    The maximum number of characters (to the word) that should be allowed
    * @param string $append The text to append to $text
    * @return string The shortened text
    * @author Brenley Dueck
    * @link   http://www.brenelz.com/blog/2008/12/14/creating-an-ellipsis-in-php/
    */
   protected function ellipsis($text, $max=1000, $append='&hellip;')
   {
       $substitution_arr = array(
           '|<br /><br />|' => ' ',
           '|&nbsp;|' => ' ',
           '|&rsquo;|' => "'",
           '|&lsquo;|' => "'",
           '|&ldquo;|' => '"',
           '|&rdquo;|' => '"',
       );
       
       if (strlen($text) <= $max) return $text;

       $replacements = array_values($substitution_arr);

       $patterns = array_keys($substitution_arr);

       preg_replace($patterns, $replacements, $text); // convert double newlines to spaces
       $text = strip_tags($text); // remove any html.  we *only* want text

       $out = substr($text, 0, $max);
       if (strpos($text, ' ') === false) return $out.$append;
       return preg_replace('/(\W)&(\W)/', '$1&amp;$2', (preg_replace('/\W+$/', ' ', preg_replace('/\w+$/', '', $out)))) . $append;
   }
}