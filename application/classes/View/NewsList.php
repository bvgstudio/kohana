<?php defined('SYSPATH') or die('No direct script access.');

class View_NewsList extends View_ContentItemList
{
    public $is_news = TRUE;
    
    public function __construct($lang = 'RUS', $offset = 0, $items_count = 20) {
        parent::__construct(ORM::factory('News')->order_by('date', 'DESC'), $offset, $items_count);
    }

    protected function _add_view_data($item)
    {
        $item_arr = $item->as_array();

        $item_arr['link'] = Route::get('item')->uri(array('controller' => 'news', 'id' => $item_arr['id']));

        if(key_exists('tags', $item_arr)) {
            foreach ($item_arr['tags'] as $key=>$tag)
            {
                $item_arr['tags'][$key]['link'] = 'news/?tags='.urlencode($item_arr['tags'][$key]['tag']);
            }
        }

        if(array_key_exists('teaser', $item_arr) && empty($item_arr['teaser']) && !empty($item_arr['body']))
        {
            $item_arr['teaser'] = $this->ellipsis($item_arr['body'], 1000);
        }
        
        if($item->date != NULL)
        {
            $date = strtotime($item->date);
            $item_arr['date'] = strftime('%d.%m.%Y', $date);
        }

        $major_media_object = $item->major_media_object;
        if ($major_media_object->loaded())
        {
            $renderer = Kostache::factory();

            $view_class_name = 'View_NewsList_'. ucfirst($major_media_object->type);
            $view_class = new $view_class_name($major_media_object, true);

            $item_arr['major_media_object_html'] = $renderer->render($view_class);

            if(empty($item_arr["title"]) && empty($item_arr["teaser"]))
            {
                $item_arr['object_only'] = true;
            }
        }

        if(array_key_exists('minor_media_objects', $item_arr))
        {
            foreach ($item_arr['minor_media_objects'] as $minor_item)
            {
                $minor_item['link'] = Route::get('item')->uri(array('controller' => $minor_item['type'], 'id' => $minor_item['id']));
            }
        }

        return $item_arr;
    }
}