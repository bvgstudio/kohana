<?php defined('SYSPATH') or die('No direct script access.');

class View_Page extends View_Base
{
    public $is_static_page = TRUE;

    protected $_model;

    public $alias;
    
    public $main_menu_parent_alias;
    
    public $title;
    public $subtitle;
    public $image_uri;
    
    public $children;
    
    public $teaser;
    public $body;

    public function __construct($alias) {
        $this->_model = ORM::factory('Page', array('alias' => $alias));
        
        $this->_init();
        
        parent::__construct();
    }
    
    protected function _init()
    {
        $this->alias = $this->_model->alias;

        $this->title = $this->page_title = $this->_model->title;
        
        $this->subtitle = $this->_model->subtitle;
        $this->image_uri = $this->_model->image_uri;
        
        $this->teaser = $this->_model->teaser;
        $this->body = $this->_model->body;
        
        $this->defineMainMenuPage();
        
        $this->relatedPagesList();
    }
    
    private function defineMainMenuPage() {
        $main_menu_page = $this->_model;
        while ($main_menu_page->loaded()) {
            if($main_menu_page->is_in_main_menu) {
                $this->main_menu_parent_alias = $main_menu_page->alias;
            }
            $main_menu_page = $main_menu_page->parent;
        }
    }
    
    protected function relatedPagesList() {
        if($this->_model->parent->loaded()) {
            $this->_addToRelatedArr($this->_model->parent);
        }
        
        $children = $this->_model->children->find_all();
        
        if(!$children->count() && $this->_model->parent->loaded()) {            
            $children = $this->_model->parent->children->find_all();
        }
        
        if($children->count()) {
            foreach ($children as $child)
            {
                // Doesn't allow current page to be listed in child pages
                if($child->pk() == $this->_model->pk()) continue;
                
                $this->_addToRelatedArr($child);
            }
        }
    }
    
    protected function _addToRelatedArr($page) {
        $page_arr = $page->as_array();
        $page_arr['link'] = Route::get('pages')->uri(array('alias' => urlencode($page_arr['alias'])));
        $this->children[] = $page_arr;
    }
}