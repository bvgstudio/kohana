<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_PagesList
{
    public $page_title = 'Страницы';
    public $is_pages = TRUE;
    
    public $items;
    
    protected $_model;

    public function __construct($offset = 0, $items_count = 20) {
        $this->_model = ORM::factory('Page')->offset($offset)->limit($items_count);
    }
    
    public function items()
    {
        if(!isset($this->items)) {
            $this->items = array();

            foreach($this->_model->find_all() as $item)
            {
                $item_arr = $item->as_array();
                $this->items[] = $item_arr;
            }
        }
        return $this->items;
    }
}