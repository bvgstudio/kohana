<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_News extends View_News
{
    public $is_news_page = TRUE;
    public $page_title = 'News';
    
    public $media_objects;

    public function __construct($id = NULL) {
        if(!is_null($id)) {
            parent::__construct(new Model_News($id));
        }
    }

    protected function _init()
    {
        $this->id = $this->_model->id;
        $this->title = $this->_model->title;
        
        $tags = $this->_model->tags->find_all();

        if($tags->count()) {
            foreach ($tags as $tag)
            {
                $this->tags[] = $tag->tag;
            }

            $this->tags = implode(', ', $this->tags);
        }
        
        $this->body = $this->_model->body;
        $this->teaser = $this->_model->teaser;

        if($this->_model->date != NULL)
        {
            $date = strtotime($this->_model->date);
            $this->date = strftime('%d.%m.%Y', $date);
        }

        $this->media_objects = $this->_model->media_objects->find_all()->as_array();

        foreach ($this->media_objects as $content_item)
        {
            $content_item = $content_item->as_array();
            
            $content_item['link'] = Route::get('item')->uri(array(
                                                            'controller' => $content_item['type'],
                                                            'id' => $content_item['id'])
                                                            );
        }
    }
}