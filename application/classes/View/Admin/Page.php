<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_Page
{
    public $is_page = TRUE;
    public $page_title = 'Страницы';
    
    public $id;
    public $alias;

    public $title;
    public $body;

    public $is_in_main_menu;
    
    public $main_menu_pages;

    public function __construct($id = NULL) {
        if(!is_null($id)) {
            $this->_model = ORM::factory('Page', $id);
        
            $this->_init();
        }
    }

    protected function _init()
    {
        $this->id = $this->_model->id;
        $this->alias = $this->_model->alias;

        $this->title = $this->page_title = $this->_model->title;
        $this->body = $this->_model->body;
        
        $this->is_in_main_menu = $this->_model->is_in_main_menu;
        
        $main_menu_pages = ORM::factory('Page')->where('is_in_main_menu', '=', true)
                                               ->and_where('id', '<>', $this->_model->pk())
                                               ->find_all();
        
        if($main_menu_pages->count()) {
            foreach ($main_menu_pages as $page)
            {
                $main_menu_page_arr = $page->as_array();
                
                $main_menu_page_arr['is_parent'] = $this->_model->parent_id == $page->pk();
                
                $this->main_menu_pages[] = $main_menu_page_arr;
            }
        }
    }
}