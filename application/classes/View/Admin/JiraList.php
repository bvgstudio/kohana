<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_JiraList extends View_Base
{
    public $page_title = 'Jira';
    public $is_jira = true;
    private $_items;
    public function __construct($model){
        parent::__construct();
        
        if(is_array($model)) {
            $this->_items = $model;
        }
        else if(!is_null($model)) {
            $this->_items = array($model);
        }
    }
    
    public function items() {
        if(count($this->_items) == 0) return;
        
        $this->items = $this->_items;
        
        $item_ids = array_map(function ($val) { return $val->key; }, $this->items);
        $items_media_objects = ORM::factory('Media_Object')->where('jira_ref', count($item_ids) > 1 ? 'IN' : '=', $item_ids)->find_all()->as_array('id');
        
        foreach ($this->items as $item) {
            $item->media_object_exists = key_exists($item->key, $items_media_objects);
            if($item->media_object_exists) {
                $media_object = $items_media_objects[$item->key];
                $item->media_object_link = Route::get('item')->uri(array('controller' => $media_object['type'], 'id' => $media_object['id']));
            }
            
            $item->is_collection = $item->type == 'PUBLISH';
        }
        
        return $this->items;
    }
}