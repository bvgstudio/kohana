<?php defined('SYSPATH') or die('No direct script access.');

class View_Admin_UsersList
{
//    public $page_title = 'Пользователи';
    public $page_title = 'Users';
    public $is_users = TRUE;
    
    public $items;
    
    protected $_model;

    public function __construct($search_criteria_arr, $offset = 0, $items_count = 20) {
        $this->_model = ORM::factory('Auth_User');
        
        if(key_exists('email', $search_criteria_arr))
                $this->_model->where('email', 'LIKE', '%'.$search_criteria_arr.'%');
        
        if(key_exists('roles', $search_criteria_arr))
                $this->_model->join('roles_users')->on('roles_users.user_id', '=', 'auth_user.id')->
                               join('roles')->on('roles.id', '=', 'roles_users.role_id')->
                               where('roles.name', count($search_criteria_arr['roles']) > 1 ? 'IN' : '=', $search_criteria_arr['roles']);
        
        $this->_model->offset($offset)->limit($items_count);
    }
    
    public function items()
    {
        if(!isset($this->items)) {
            $this->items = array();

            $team_role = ORM::factory('Role', array('name' => 'team'));
            $representatives_role = ORM::factory('Role', array('name' => 'representative'));
            
            foreach($this->_model->find_all() as $item)
            {
                /** @var Model_Auth_User $item */
                $item_arr = $item->as_array();
                
                $item_arr['is_team'] = $item->has('roles', $team_role);
                $item_arr['is_filial'] = $item->has('roles', $representatives_role);
                $item_arr['data'] = $item->data;

                $this->items[] = $item_arr;
            }
        }
        return $this->items;
    }
}
