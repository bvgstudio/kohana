<?php

defined('SYSPATH') or die('No direct script access.');

class View_Catalogue extends View_Base {
    private $BOOKS_COLLECTION_COLUMNS = 5;

    public $model;
    
    public $first_half_no_books_nor_collections_items;
    public $second_half_no_books_nor_collections_items;
    
    public $first_half_books_and_collections_items;
    public $second_half_books_and_collections_items;
    
    protected $content_filters;
    protected $no_books_nor_collections_model;
    protected $books_and_collections_model;
    
    protected $no_books_nor_collections_items;
    protected $books_and_collections_items;

    public $search_query;
    public $has_no_query = FALSE;
    public $is_catalogue = TRUE;

    public function __construct($content_filters = NULL, $offset = 0, $items_count = 10, $order_by = 'occurrence_date', $order_dir = 'DESC') {        
        parent::__construct();
        
        $this->content_filters = $content_filters;

        $model = ORM::factory('Media_Object')->where('visible', '=', TRUE);
        if (count($content_filters) > 0) {
            $model->search($this->content_filters);
        } else {
            $this->has_no_query = TRUE;
        }
        $this->search_query = Arr::get($_GET, 'q', NULL);       
        
        $this->model = clone $model->order_by($order_by, $order_dir);
        
        $this->no_books_nor_collections_model = $model->where('type', 'NOT IN', array('book', 'collection'))->offset($offset)->limit($items_count);
        
        $cloned_model = clone $this->model;
        $this->books_and_collections_model = $cloned_model->where('type', 'IN', array('book', 'collection'))
                                                          ->offset($this->BOOKS_COLLECTION_COLUMNS * 2 * $offset / $items_count )
                                                          ->limit($this->BOOKS_COLLECTION_COLUMNS * 2);
    }

    public function first_half_no_books_nor_collections_items() {
        if (!isset($this->no_books_nor_collections_items)) {
            $this->no_books_nor_collections_items = $this->no_books_nor_collections_model->find_all();
        }
        
        $this->first_half_no_books_nor_collections_items = array();
        for ($idx = 0; $idx < round(count($this->no_books_nor_collections_items) / 2); $idx++) {
            $item = $this->no_books_nor_collections_items[$idx];

            $renderer = Kostache::factory();

            $view_class_name = 'View_NewsList_' . ucfirst($item->type);
            $view_class = new $view_class_name($item);

            $this->first_half_no_books_nor_collections_items[] = $renderer->render($view_class);
        }
        
        return $this->first_half_no_books_nor_collections_items;
    }
    
    public function second_half_no_books_nor_collections_items() {
        if (!isset($this->no_books_nor_collections_items)) {
            $this->no_books_nor_collections_items = $this->no_books_nor_collections_model->find_all();
        }
        
        $this->second_half_no_books_nor_collections_items = array();
        for ($idx = round(count($this->no_books_nor_collections_items) / 2);
             $idx < count($this->no_books_nor_collections_items); $idx++) {
            $item = $this->no_books_nor_collections_items[$idx];

            $renderer = Kostache::factory();

            $view_class_name = 'View_NewsList_' . ucfirst($item->type);
            $view_class = new $view_class_name($item);

            $this->second_half_no_books_nor_collections_items[] = $renderer->render($view_class);
        }
        return $this->second_half_no_books_nor_collections_items;
    }
    
    public function first_half_books_and_collections_items() {
        if (!isset($this->books_and_collections_items)) {
            $this->books_and_collections_items = $this->books_and_collections_model->find_all();
        }
        
        $is_one_row = count($this->books_and_collections_items) <= $this->BOOKS_COLLECTION_COLUMNS;

        $this->first_half_books_and_collections_items = array();
        for ($idx = 0; $idx < ($is_one_row ? count($this->books_and_collections_items) 
                                           : round(count($this->books_and_collections_items) / 2)); $idx++) {
            $item = $this->books_and_collections_items[$idx];

            $view = array(
                "id" => $item->pk(),
                "content_type" => $item->type,
                "link" => Route::get('item')->uri(array('controller' => $item->type, 'id' => $item->pk())),
                "title" => $item->title,
                "cover_img_uri" => $item->img_uri
            );
            $this->first_half_books_and_collections_items[] = $view;
        }
        return $this->first_half_books_and_collections_items;
    }
    
    public function second_half_books_and_collections_items() {
        if (!isset($this->books_and_collections_items)) {
            $this->books_and_collections_items = $this->books_and_collections_model->find_all();
        }
            
        $is_one_row = count($this->books_and_collections_items) <= $this->BOOKS_COLLECTION_COLUMNS;

        $this->second_half_books_and_collections_items = array();
        if (!$is_one_row) {
            for ($idx = count($this->books_and_collections_items) / 2; $idx < count($this->books_and_collections_items); $idx++) {
                $item = $this->books_and_collections_items[$idx];

                $view = array(
                    "id" => $item->pk(),
                    "content_type" => $item->type,
                    "link" => Route::get('item')->uri(array('controller' => $item->type, 'id' => $item->pk())),
                    "title" => $item->title,
                    "cover_img_uri" => $item->img_uri
                );
                $this->second_half_books_and_collections_items[] = $view;
            }
        }
        return $this->second_half_books_and_collections_items;
    }
}
