<?php defined('SYSPATH') or die('No direct script access.');

class View_News extends View_ContentItem
{
    public $is_content_page = FALSE;
    public $is_news_page = TRUE;
    public $major_media_object_html;
    public $minor_media_objects;


    public function __construct($id) {
        parent::__construct(new Model_News($id));
    }

    protected function _init()
    {
        $this->link = Route::get('item')->uri(array('controller' => 'news', 'id' => $this->_model->pk()));
        $this->title = $this->_model->title;
        
        if(!is_null($this->_model->tag_set_id)) {
            $tags = $this->_model->tags->find_all();

            foreach ($tags as $tag)
            {
                $tag_arr = $tag->as_array();
                $tag_arr['link'] = 'news/?tags='.urlencode($tag_arr['tag']);
                $this->tags[] = $tag_arr;
            }
        }
        
        $this->body = $this->_model->body;
        $this->teaser = $this->_model->teaser;
        
        if(empty($this->teaser))
        {
            $this->teaser = $this->ellipsis(strip_tags($this->body), 1000);
        }

        if($this->_model->date != NULL)
        {
            $date = strtotime($this->_model->date);
            $this->date = strftime('%d.%m.%Y', $date);
        }

        $major_media_object = $this->_model->major_media_object;

        if ($major_media_object->loaded())
        {
            $renderer = Kostache::factory();

            $view_class_name = 'View_NewsList_' . ucfirst($major_media_object->type);
            $view_class = new $view_class_name($major_media_object, true);

            $this->major_media_object_html = $renderer->render($view_class);
        }

        $minor_media_objects = $this->_model->minor_media_objects->find_all()->as_array();

        foreach ($minor_media_objects as $minor_item)
        {
            $minor_item = $minor_item->as_array();
            $minor_item['link'] = Route::get('item')->uri(array(
                                                            'controller' => $minor_item['type'],
                                                            'id' => $minor_item['id'])
                                                            );
            $this->minor_media_objects[] = $minor_item;
        }
    }
}