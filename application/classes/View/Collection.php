<?php

defined('SYSPATH') or die('No direct script access.');

class View_Collection extends View_ContentItem {

    public $sub_collection_items;
    public $content_items;
    public $cover_img_uri;
    public $download_uri;
    public $images;

    public function __construct($id, $is_attached = FALSE) {
        parent::__construct(new Model_Collection($id), $is_attached);
    }

    protected function _init() {
        parent::_init();

        $this->cover_img_uri = $this->_model->img_uri;


        foreach ($this->_model->linked_medias->find_all()->as_array() as $item) {
            $arr_item = $item->as_array();
            
            //// TODO: sort by position in collection_media_seq
            
            switch($item->type) {
                case 'picture':
                    $this->images[] = $item->as_array();
                break;
                case 'collection':
                    $arr_item = ORM::factory('Collection', $item->pk())->as_array();
                    
                    $arr_item['link'] = Route::get('default')->uri(array(
                        'controller' => $item->type,
                        'id' => $item->id)
                    );
                    $this->sub_collection_items[] = $arr_item;                    
                break;
                default:
                    $arr_item['link'] = Route::get('default')->uri(array(
                        'controller' => $item->type,
                        'id' => $item->id)
                    );

                    $this->content_items[] = $arr_item;
            }
        }
        
        $this->download_uri = Route::get('default')->uri(array(
                                                                'controller' => 'collection',
                                                                'action' => 'download',
                                                                'id' => $this->_model->id)
                                                                );
    }

}
