<?php defined('SYSPATH') or die('No direct script access.');

class View_VideoList extends View_ContentItemList
{
    public function __construct($lang = 'RUS') {
        parent::__construct(new Model_Video($lang));
    }

    protected function _add_view_data($item)
    {
        $item_arr = parent::_add_view_data($item);

        return $this->_define_uri_type($item_arr);
    }

    protected function _define_uri_type($item_arr)
    {
        $arr = array();
        $item_arr['is_youtube'] = preg_match_all('/http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11})/', $item_arr['file_uri'], $arr, PREG_PATTERN_ORDER);
        if($item_arr['is_youtube'])
        {
            $item_arr['file_uri'] = "http://www.youtube.com/embed/".$arr[1][0];
        }
        $item_arr['is_local'] = !$item_arr['is_youtube'];

        return $item_arr;
    }
}