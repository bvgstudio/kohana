<?php defined('SYSPATH') or die('No direct script access.');

class View_DiskList extends View_ContentItemList
{
    public function __construct($lang = 'RUS') {
        parent::__construct(new Model_Disk($lang));
    }

    protected function _add_view_data($item)
    {
        $item_arr = parent::_add_view_data($item);

        $i = 0;
        foreach($item->media_objects->find_all() as $media_object)
        {
            if ($media_object->loaded())
            {
                $content_item = $media_object->item;
                $renderer = Kostache::factory();

                $view_class_name = 'View_NewsList_'.ucfirst($content_item->object_name());
                $view_class = new $view_class_name($content_item);
                $view_class->is_minimal = true;
                $item_arr['content_items'][$i]['html'] = $renderer->render($view_class);

                $i++;
            }
        }

        return $item_arr;
    }


}