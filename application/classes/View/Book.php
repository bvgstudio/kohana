<?php defined('SYSPATH') OR die('No direct access allowed.');

class View_Book extends View_ContentItem
{
    public $cover_img_uri;
    public $download_uri;

    public function __construct($id) {
        parent::__construct(new Model_Book($id));
    }

    protected function _init()
    {
        parent::_init();

        $this->cover_img_uri = $this->_model->cover_img_uri;
        $this->download_uri = $this->_model->download_uri;
    }
}

