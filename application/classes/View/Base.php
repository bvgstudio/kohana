<?php defined('SYSPATH') or die('No direct script access.');

class View_Base
{
    public $page_title;
    public $is_logged_in;
    public $user;
    
    public $main_menu_pages;

    public function __construct() {
        $this->authData();
        $this->mainMenuPages();
        $this->controllerName();
    }
    
    protected function authData() {        
        $this->user = Auth::instance()->get_user();
        $this->is_logged_in = $this->user != NULL;
    }

    protected function mainMenuPages() {                
        $main_menu_pages = ORM::factory('Page')->where('is_in_main_menu', '=', true)->find_all();
        
        if($main_menu_pages->count()) {
            foreach ($main_menu_pages as $page)
            {
                $main_menu_page_arr = $page->as_array();
                
                if($page->title == 'Шрила Бхакти Вигьяна Госвами') {
                    $main_menu_page_arr['title'] = 'О Махарадже';
                }
                
                $main_menu_page_arr['is_active'] = property_exists($this, 'main_menu_parent_alias') && $this->main_menu_parent_alias == $page->alias;
                
                $main_menu_page_arr['link'] = Route::get('pages')->uri(array('alias' => $page->alias));
                
                $this->main_menu_pages[] = $main_menu_page_arr;
            }
        }
    }
    
    protected function controllerName() {
        $trace = debug_backtrace();
        if(!count($trace)) return;
        
        for ($index = 0; $index < count($trace); $index++) {
            $macth_array = array();
            if(preg_match("/^Controller_(.*)/", $trace[$index]['class'], $macth_array)) {
                $this->controller = strtolower($macth_array[1]);
                break;
            }
        }
    }

}