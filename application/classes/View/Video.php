<?php defined('SYSPATH') OR die('No direct access allowed.');

class View_Video extends View_ContentItem
{
    public $venue;
    
    public $video_height = 360;
    public $video_width = 640;

    public $file_uri;
    public $is_youtube;
    public $is_local;
    
    public $audio_version_link;
    
    public function __construct($id) {
        if ($id instanceof ORM) {
            parent::__construct($id);
        } else {
            parent::__construct(new Model_Video($id));
        }
    }

    protected function _init()
    {
        parent::_init();

        $this->file_uri = $this->_model->file_uri;
        $this->_define_uri_type($this->file_uri);
        
        $this->venue = $this->_model->venue;
        
        $master_audios = $this->_model->master_medias->where('type', '=', 'audio')->find_all();
        
        $audio_version_item = count($master_audios) > 0 ? $master_audios[0] : $this->_model->linked_medias->where('type', '=', 'audio')->find();
        
        if($audio_version_item->loaded())
        {
            $this->audio_version_link = Route::get('default')->uri(array(
                                                                        'controller' => 'audio',
                                                                        'id' => $audio_version_item->id)
                                                                        );
        }
    }

    protected function _define_uri_type($uri)
    {
        $arr = array();
        $this->is_youtube = preg_match_all('/http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11})/', $uri, $arr, PREG_PATTERN_ORDER);
        if($this->is_youtube)
        {
            $this->file_uri = "http://www.youtube.com/embed/".$arr[1][0];
        }
        $this->is_local = !$this->is_youtube;
    }
}

