<?php defined('SYSPATH') or die('No direct script access.');

class View_ArticleList extends View_ContentItemList
{
    public function __construct($lang = 'RUS') {
        parent::__construct(new Model_Article($lang));
    }
}