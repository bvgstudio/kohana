<?php defined('SYSPATH') or die('No direct script access.');

class View_Article extends View_ContentItem
{
    public function __construct($id) {
        parent::__construct(new Model_Article($id));
    }

    protected function _init()
    {
        parent::_init();
    }


}