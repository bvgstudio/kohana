<?php defined('SYSPATH') OR die('No direct access allowed.');

class View_NewsList_Picture extends View_ContentItem
{
    public $file_uri;

    public function __construct($id) {
        parent::__construct(new Model_Picture($id));
    }

    protected function _init()
    {
        parent::_init();

        $this->file_uri = $this->_model->file_uri;
    }
}
