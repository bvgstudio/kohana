<?php defined('SYSPATH') OR die('No direct access allowed.');

class View_NewsList_Audio extends View_Audio
{
    public $first_collection;

    protected function _init()
    {   
        parent::_init();
        
        if($this->is_in_collection) {
            $this->first_collection = $this->collections[0];
        }
    }
}