<?php defined('SYSPATH') or die('No direct script access.');

class View_Studio extends View_Base 
{
    public $page_title;
    public $is_studio = TRUE;
    
    public $representatives;
    public $team;
    public $vacancies;
    public $donation_camps;

    public function __construct($team, $representatives, $vacancies, $donation_camps) {
        parent::__construct();
        
        $this->team = $team;
        $this->representatives = $representatives;
        $this->vacancies = $vacancies;
        $this->donation_camps = $donation_camps;
        
        $this->today = strftime('%d.%m.%Y', time());
    }
}
