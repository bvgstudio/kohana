<?php defined('SYSPATH') or die('No direct script access.');

class View_Main extends View_NewsList
{
    public $is_main = TRUE;
    public $is_news = FALSE;
    
    public $news_page_link;

    public function __construct($lang = 'RUS') {
        parent::__construct($lang);

        $this->news_page_link = Route::get('default')->uri(array('controller' => 'news'));
    }
}