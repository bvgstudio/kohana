<?php defined('SYSPATH') or die('No direct script access.');

class View_AudioList extends View_ContentItemList
{
    public function __construct($lang = 'RUS') {
        parent::__construct(new Model_Audio($lang));
    }
}