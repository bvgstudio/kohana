<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Studio extends Controller {

    protected $representatives = NULL;
    protected $geocoder = NULL;


    public function action_index()
    {
	$renderer = Kostache_Layout::factory();        
                        
        $team = ORM::factory('Auth_User')->
                                join('roles_users')->on('roles_users.user_id', '=', 'auth_user.id')->
                                join('roles')->on('roles.id', '=', 'roles_users.role_id')->
                                join('user_data')->on('user_data.user_id', '=', 'auth_user.id')->
                                where('roles.name', '=', 'team')->
                                order_by('user_data.rank', 'DESC')->
                                find_all()->as_array();

        $representatives = ORM::factory('Auth_User')->
                                join('roles_users')->on('roles_users.user_id', '=', 'auth_user.id')->
                                join('roles')->on('roles.id', '=', 'roles_users.role_id')->
                                join('user_data')->on('user_data.user_id', '=', 'auth_user.id')->
                                where('roles.name', '=', 'representative')->
                                order_by('user_data.rank', 'DESC')->
                                find_all()->as_array();
        
        // Fill in geo_location field if possible
        foreach ($representatives as $rpr) {
            if($rpr->data->city && !$rpr->data->geo_location) {
                $rpr->data->geo_location = $this->geocode($rpr->data->city);
                
                $rpr->data->update();
            }
        }
        
        $vacancies = ORM::factory('Vacancy')->find_all()->as_array();

        $campaigns = ORM::factory('Donation')->where('end_date', '>=', DB::expr('now()'))->order_by('(necessary_amount - amount)', 'DESC')->find_all()->as_array();

        $donation_camps = array();
        for ($index = 0; $index < count($campaigns); $index++) {
            $campaign = $campaigns[$index]->as_array();
            
            $date = strtotime($campaign['end_date']);            
            $campaign['end_date'] = strftime('%d.%m.%Y', $date);
            
            $campaign['percent'] = $campaign['amount'] ? ($campaign['amount'] / $campaign['necessary_amount']) * 100 : 0;
            $campaign['necessary_amount'] = number_format($campaign['necessary_amount'], 0, ",", " ");
            $campaign['amount'] = number_format($campaign['amount'], 0, ",", " ");

// неправильное решение. надо где то менять разделитель тысяч или правильно конвертировать полученные из ORM значения.
            $campaign['completed'] = str_replace(" ", "", $campaign['amount']) >= str_replace(" ", "", $campaign['necessary_amount']);
            
            $donation_camps[] = $campaign;
        }
            
        
        $view = new View_Studio($team, $representatives, $vacancies, $donation_camps);
        $view->page_title = "Студия";

        $this->response->body($renderer->render($view));
    }
    
    function geocode($address){
        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address 
        if($resp['status']=='OK'){
            // get the important data
            $lat = $resp['results'][0]['geometry']['location']['lat'];
            $long = $resp['results'][0]['geometry']['location']['lng'];

            // verify if data is complete
            if($lat && $long){
                return $lat.','.$long;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
 } // End
