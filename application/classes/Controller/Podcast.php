<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Podcast extends Controller {

    private $feed;

    public function action_audio()
    {
        $this->auto_render = FALSE;

        $this->feed = new ezcFeed('rss2');

        $this->feed->language = 'ru';
        $this->feed->title = 'Е.С. Бхакти Вигьяна Госвами Махарадж';
        $this->feed->description = 'Аудио лекции Е.С. Бхакти Вигьяны Госвами Махараджа';
        $link = $this->feed->add( 'link' );
        $link->href = 'http://www.goswami.ru';
        $this->feed->id = $this->request->url('http'); // atom self link

        $this->feed->icon = 'http://goswami.ru/favicon.ico';

        $author = $this->feed->add( 'author' );
        $author->name = 'Е.С. Бхакти Вигьяна Госвами Махарадж';
        $author->email = 'admin@goswami.ru';

        $this->feed->generator->version = '1.3';

        $iTunes = $this->feed->addModule( 'iTunes' );

        $iTunes->keywords = 'Кришна, Шримад Бхагаватам, Бхагават Гита, Госвами Махарадж, лекции';
        $iTunes->explicit = 'yes';
        $iTunes->subtitle = 'Аудио лекции Е.С. Бхакти Вигьяны Госвами Махараджа';
        $iTunes->author = 'Е.С. Бхакти Вигьяны Госвами Махараджа';
        $iTunes->summary = 'Аудио лекции Е.С. Бхакти Вигьяны Госвами Махараджа';

        $owner = $iTunes->add('owner');
        $owner->name = 'Е.С. Бхакти Вигьяна Госвами Махарадж';
        $owner->email = 'admin@goswami.ru';

// add an image for the podcast
        $image = $iTunes->add( 'image' );
        $image->link = 'http://goswami.ru/img/bvgm.jpg';

// add the podcast in the category
        $category = $iTunes->add( 'category' );
        $category->term = 'Religion & Spirituality';
//        $subCategory = $category->add( 'category' );
//        $subCategory->term = 'Gadgets';


        $audio_items = ORM::factory('Audio')->where('type', '=', 'audio')->and_where('podcast', '=', 'true')->find_all();

        foreach ($audio_items as $audioModel) {

		$this->addAudioItem($audioModel);
        }

        $this->response->headers('Content-Type', 'text/xml');
        $this->response->body($this->feed->generate( 'rss2' ));
    }

    private function addAudioItem($audio)   {

        $item = $this->feed->add( 'item' );

        // encode file name to avoid bad links in other devices and engines
        preg_match('/(http\:\/\/)?(.+\/)*(.+)?/', $audio->file_uri, $url_parts, PREG_OFFSET_CAPTURE);
        $itemUrl = $url_parts[1][0].$url_parts[2][0].rawurlencode(urldecode($url_parts[3][0]));

        $item->id = sha1($itemUrl);
        $item->id->isPermaLink = false;

        $item->title = $audio->title;
        $item->description = $audio->body;
        $item->published = date( 'r' , strtotime($audio->occurrence_date) );
        $item->guid = $itemUrl;

        $author = $item->add( 'author' );
        $author->name = $audio->authors->getList();
        $author->email = 'admin@goswami.ru';

        $link = $item->add( 'link' );
        $link->href = $itemUrl;

        $enclosure = $item->add( 'enclosure' );

        $enclosure->url = $itemUrl;
        $enclosure->length = $audio->size; // bytes
        $enclosure->type = 'audio/mpeg'; //'audio/x-mp3';

        $iTunes = $item->addModule( 'iTunes' );

        $iTunes->duration = $audio->duration;
        $iTunes->subtitle = $audio->title;

        if (!is_null($audio->tag_set_id)) {
            $tags = $audio->tags->find_all();
            foreach ($tags as $tag) {
                $keywords[] = $tag->tag;
            }
            if (property_exists($iTunes, 'keywords'))
                $iTunes->keywords .= implode(',', $keywords);
        }
    }


    public function action_video()
    {
    }
}
