<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $id = $this->request->param('id');
        if(!$id)
        {
            $view = $this->get_list_view();
        }
        else
        {            
            $view = new View_News($id);
            $view->page_title = $view->title;
        }
        
        $view->controller = strtolower($this->request->controller());
        
        $this->response->body($this->request->is_ajax() ?
                              $renderer->render($view, "Partials/News-list") : 
                              $renderer->render($view));
    }
    
    
    protected function get_list_view() {
        $search_criteria_arr = $this->make_search_criterias_arr();

        $pagination = Pagination::factory(array('total_items' => ORM::factory('News')->count_all(),
                                                'current_page'=> array('source' => 'query_string', 'key' => 'page')));
        
        $lang = $this->request->param('language');
        
        $view = new View_NewsList($lang, $pagination->offset, $pagination->items_per_page);

        $view->has_items = $pagination->total_items > 0;
        $view->paginator = $pagination->render();

        $view->page_title = 'Новости студии';
        
        return $view;
    }
    protected function make_search_criterias_arr() {
        
    }
 } // End
