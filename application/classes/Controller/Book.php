<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Book extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $lang = $this->request->param('language');
        $id = $this->request->param('id');
        if(!$id)
        {
            $view = new View_BookList($lang);
            $view->page_title = 'Книги';
            $view->model = $view->model->order_by(array('date', 'id'), 'desc')->limit(10);
        }
        else
        {
            $view = new View_Book($id);
            $view->page_title = 'Книга '.$view->title;
        }

        $this->response->body($renderer->render($view));
    }
 } // End
