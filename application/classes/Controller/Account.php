<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller {

    public function action_index() {
        $user = Auth::instance()->get_user();
        if($user != NULL) {
            $renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory('admin/layout');

            $this->response->body($renderer->render(new View_Profile($user)));
        }
        else {
            $this->redirect('/account/logon/?return_url='.urlencode($this->request->uri()));
        }
    }
    
    public function action_logon()
    {
        if ($this->request->method() == HTTP_Request::POST)
        {
            $post = $this->request->post();

            // Load the user
            $user = ORM::factory('Auth_User');
            $user->where('email', '~*', $post['email'])->find();
            
            $errors = array();
            
            if($user->loaded()) {
                $success = Auth::instance()->login($user, $post['password'], true);
                if ($success)
                {
                    if(!$this->request->is_ajax()) {
                        $this->redirect( $this->request->query('return_url') != NULL ? $this->request->query('return_url') : '/');
                    }
                    else if($this->request->is_ajax()) {
                        $this->response->body(Kostache::factory()->render(new View_Base(), 'partials/user-panel'));
                    }
                    return;
                }
                else
                {
                    $errors["errors"] = array('Неверный пароль');
                }
            }
            else {
                $errors["errors"] = array('Такой пользователь не зарегистрирован');
            }
            
            if($this->request->is_ajax()) {             
                $this->response->headers('Content-Type','application/json');
                $this->response->body(json_encode($errors));
            }
            else {
                $this->response->body(View::factory('pages/logon')->set('errors', $errors)->render());
            }
        }
        else {
            $this->response->body(View::factory('pages/logon')->render());
        }
    }
    
    public function action_logout()
    {
        Auth::instance()->logout();
        
        $this->redirect('/');
    }
    
    public function action_create()
    {
        $post = $this->request->post();
        foreach ($post as &$post_var) {
            if (empty($post_var)) {
                unset($post_var);
            }
        }
        
        $email = $post['email'];
        $post['username'] = $email;
        
        $existing_user = ORM::factory('Auth_User', array('email' => $email));
        
        $errors = array();
        if ($existing_user->loaded())
        {
            $errors['email'] = 'Пользователь с таким логином уже есть';
        }
        else
        {
            $errors = $this->_create_account($post);
        }
        
        if($errors && count($errors) > 0) {
            if($this->request->is_ajax()) {             
                $this->response->headers('Content-Type','application/json');
                $this->response->body(json_encode(array('errors' => $errors)));
            }
            else {
                $this->redirect('/');
            }
        }
        else {
             $this->response = Request::factory('/account/logon/')
                               ->method(Request::POST)
                               ->post($this->request->post())
                               ->requested_with($this->request->requested_with())
                               ->execute();
        }
    }
    
    
    public function action_edit()
    {
        $post = $this->request->post();
        
        $email = $post['email'];
        $post['username'] = $email;
        
        $user = Auth::instance()->get_user();
        $errors = NULL;
        
        if ($user != NULL) {
            try
            {
                $user->update_user($post, array('username', 'password','email'));

                $user_data = $user->data;
                
                $user_data->values($post,
                                   array('name', 'avatar', 'spiritual_name', 'city','birth_date', 'sex', 'email_subscriber', 'activity', 'phone'));

                $user_data->update();
            }
            catch (ORM_Validation_Exception $e)
            {
                $errors = $e->errors('controllers');
                if(key_exists('_external', $errors)) $errors = array_merge($errors, $errors['_external']);
            }
        }
        
        if($errors && count($errors) > 0) {
            if($this->request->is_ajax()) {             
                $this->response->headers('Content-Type','application/json');
                $this->response->body(json_encode(array('errors' => $errors)));

            }
            else {
                $this->redirect('/');
            }
        }
    }
    
    public function action_avatar()
    {
        if (isset($_FILES['file']))
        {
            $filename = $this->_save_image($_FILES['file']);
        }
        
        if (!$filename)
        {
            $json_response['errors'] = '';
        }
        else {
            $json_response['filename'] = $filename;
            $json_response['url'] = '/content/avatars/'.$filename;
            
            $user = Auth::instance()->get_user();
            if($user !== null) {
                $user_data = $user->data;
                $user_data->avatar = $filename;
                $user_data->update();
            }
        }
        
        $this->response->body(json_encode($json_response));
    }
    
    public function action_forgotpassword()
    {
        $user = ORM::factory('Auth_User')->where('email', '~*', $this->request->post('email'))->find();

        if ($user->loaded())
        {
            $password_reset = $this->reset_password($user, $clear_password);
            
            if ($password_reset === TRUE)
            {                
                if (!$this->_send_password_mail($user, $clear_password)) {
                    $errors = array ("Ошибка при отправке письма");
                }
            }
            else
            {
                $errors = $password_reset["_external"];
            }
        }
        else
        {
            $errors = array ("Такой пользователь не найден");
        }
        
        $json_response = array();
        if(isset($errors) > 0) {
            $json_response['errors'] = $errors;
        }
        else {
            $json_response['valid'] = true;
//            $json_response['message'] = "Вам было отправлено письмо на электронную почту с новым паролем";
            $json_response['errors'] = array("Вам было отправлено письмо на электронную почту с новым паролем");
        }
        
        $this->response->headers('Content-Type','application/json');
        $this->response->body(json_encode($json_response));


    }

    public function reset_password($user, &$clear_password)
    {
        /**
         *  Model_User::update_user() requires an array as the first argument
         */
        $update['username'] = $user->username;
        $update['email']    = $user->email;
        $update['password'] = substr(uniqid(), 0, 7);
        $update['password_confirm'] = $update['password'];
        try
        {
            $user->update_user($update, array('username', 'email', 'password'));
        }
        catch (ORM_Validation_Exception $e)
        {
            $errors = $e->errors('controllers');
        }

        $clear_password = $update['password'];
        return (isset($errors)) ? $errors : TRUE;
    }
    
    protected function _send_password_mail($user, $clear_password) {
        $msg_body = file_get_contents('content/forgot_password.tpl');
        $msg_body = str_replace('{@PASSWORD}', $clear_password, $msg_body);

// Для отправки HTML-письма должен быть установлен заголовок Content-type
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";

// Дополнительные заголовки
        $headers .= "From: admin@goswami.ru\r\n";
        $headers .= "To: $user->email\r\n";
        $headers .= "Reply-To: admin@goswami.ru\r\n";
        $subject = "=?UTF-8?B?".base64_encode("GOSWAMI.RU: новый пароль")."?=";


        $mail_send_success = mail($user->email, $subject, $msg_body, $headers);
        
        return $mail_send_success;
    }
    
    protected function _create_account($post) {
        try
        {
            $user = ORM::factory('Auth_User')->create_user($post, array('username', 'password', 'email'));                
            $user->save();

            $user->add('roles', ORM::factory('Role', array('name' => 'login')));

            $user_data = ORM::factory('Auth_User_Data');
            $user_data->user_id  = $user->pk();
            
            $user_data->values($post,
                               array('name', 'avatar', 'spiritual_name', 'city','birth_date', 'sex', 'email_subscriber', 'activity', 'phone'));

            $user_data->save();
        }
        catch (ORM_Validation_Exception $e)
        {
            $errors = $e->errors('controllers');
            if(key_exists('_external', $errors)) $errors = array_merge($errors, $errors['_external']);
            
            return $errors;
        }
        
    }
    
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'content/avatars/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
 
            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }
 } // End
