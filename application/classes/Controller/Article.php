<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Article extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $lang = $this->request->param('language');
        $id = $this->request->param('id');
        if(!$id)
        {
            $view = new View_ArticleList($lang);
            $view->page_title = 'Статьи';
            $view->model = $view->model->order_by(array('date', 'id'), 'desc')->limit(10);
        }
        else
        {
            $view = new View_Article($id);
            $view->page_title = $view->title;
        }

        $this->response->body($renderer->render($view));
    }
 } // End
