<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Donation extends Controller {

    public function action_result()
    {
	    if(Arr::get($this->request->query(), 'LMI_PREREQUEST', FALSE)) return;

        $donation_id = Arr::get($this->request->query(), 'LMI_PAYMENT_NO', NULL);
        
        if(!$donation_id) return;
        
        $donation = ORM::factory('Donation', $donation_id);
        
        $donation->amount += floatval(Arr::get($this->request->query(), 'LMI_PAYMENT_AMOUNT', 1));
        $donation->latest_recharge = date(DATE_ATOM);
        $donation->save();        
    }
 } // End
