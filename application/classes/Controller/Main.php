<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller
{
    public function action_index()
    {
	    $renderer = Kostache_Layout::factory();

        $lang = $this->request->param('language');
        if(!empty($lang)) $lang = strtoupper($lang);
        
        $view = new View_Main($lang);
        $view->page_title = 'Сайт студии Госвами Махараджа';

        $view->model = $view->model->order_by(array('date', 'id'), 'desc')->limit(5);

        $this->response->body($renderer->render($view));
    }
}