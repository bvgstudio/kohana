<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tags extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $tag = Arr::get($_GET, 'text', $this->request->param('text'));
        $content_type = Arr::get($_GET, 'content_type', $this->request->param('content_type'));

        if(!$content_type)
        {
            $view = new View_Catalogue(array('tags' => array($tag)));
            $view->page_title = 'Всё по тегу "'.$tag.'"';
            $view->model = $view->model->order_by(array('id'), 'desc')->limit(10);
        }
        else
        {
            $view = new View_Catalogue(array('tags' => array($tag), 'content_types' => array($content_type) ));
            $view->page_title = $view->content_type_name.' по тегу "'.$tag.'"';
        }

        $this->response->body($renderer->render($view));
    }
    
    public function action_list()
    {
        $term = Arr::get($_GET, 'term', $this->request->param('term'));
        $tags_arr = array();
        
        if ($term != NULL && $term != '') {
            $tags = ORM::factory('Tag')->where('tag', 'ILIKE', $term.'%')->find_all();

            foreach($tags as $tag)
            {
                $tags_arr[] = $tag->tag;
            }
        }
        $this->response->body(json_encode($tags_arr));
    }
 } // End
