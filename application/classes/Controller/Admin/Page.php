<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Page extends Controller_Admin_Base {
    public function action_index()
    {       
        $renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory('admin/layout');

        $id = $this->request->param('id');
        if(!$id)
        {
            $view = $this->_get_list_view();
            $view->is_admin = true;
            
            $this->response->body($this->request->is_ajax() ?
                                  $renderer->render($view, 'Partials/Admin/Pages-list') : 
                                  $renderer->render($view));
        }
        else
        {            
            $view = new View_Admin_Page($id);
            
            $this->response->body($renderer->render($view));
        }
    }
    
    protected function _get_list_view() {
        $search_criteria_arr = $this->make_search_criterias_arr();

        $pagination = Pagination::factory(array('total_items' => ORM::factory('Page')->count_all()));
        
        $view = new View_Admin_PagesList($pagination->offset, $pagination->items_per_page);

        $view->has_items = $pagination->total_items > 0;
        $view->paginator = $pagination->render();
        
        return $view;
    }
    
    protected function make_search_criterias_arr() {
    }
    
    public function action_create()
    {
        if($this->request->method() == Request::GET) {
            $this->response->body(Kostache_Layout::factory('admin/layout')->render(new View_Admin_Page()));
        }
        else {
            $data = $this->request->post();

            $page = ORM::factory('Page')->values($data, array('alias', 'title', 'body', 'is_in_main_menu', 'parent_id'));

            $page->create();
            
            $this->redirect('/admin/page');
        }
    }
    
    public function action_edit()
    {
        $id = $this->request->param('id');
        if (!$id) {
            throw HTTP_Exception::factory(500, 'ID needed!');
        }

        $data = $this->request->post();

        $page = ORM::factory('Page', $id);

        if ($page->loaded()) {
            $page->values($data, array('alias', 'title', 'body', 'is_in_main_menu', 'parent_id'));

            $page->save();

            $this->redirect('/admin/page');
        } else {
            throw HTTP_Exception::factory(404, 'Item not found!');
        }
    }
    
    public function action_delete()
    {
        $id = $this->request->param('id');
        if (!$id) {
            throw HTTP_Exception::factory(500, 'ID needed!');
        }

        $page = ORM::factory('Page', $id);

        if ($page->loaded()) {
            $page->delete();
        } else {
            throw HTTP_Exception::factory(404, 'Item not found!');
        }
    }
    
    public function action_upload() {
        if (isset($_FILES['file']))
        {
            $filename = $this->_save_image($_FILES['file']);
        }
        
        if (!$filename)
        {
            $json_response['errors'] = '';
        }
        else {
            $json_response['filename'] = $filename;
            $json_response['filelink'] = '/content/pages/'.$filename;
        }
        
        $this->response->body(json_encode($json_response));
    }
    
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'content/pages/';
        
        $filename = strtolower(Text::random('alnum', 20)).'.jpg';
        if (Upload::save($image, $filename, $directory))
        {
            return $filename;
        }
 
        return FALSE;
    }
    
    public function action_check_alias()
    {
        $reserved_aliases = array('catalogue', 'news', 'admin');
        
        $alias = Arr::get($_GET, 'alias', $this->request->param('alias'));
        $id = Arr::get($_GET, 'id', $this->request->param('id'));
        
        $result = array('valid' => true);

        if(!preg_match('/^[a-zA-Z1-9_-]{3,}$/', $alias)) {
            $result['valid'] = false;
            $result['message'] = 'должно быть три и более символов: латинские буквы, цифры, символ подчеркивания или минус';
        }
        else {
            $model = ORM::factory('Page');
            if($id) $model->where($model->primary_key(), '<>', $id);
            
            $aliases_arr = array_keys($model->find_all()->as_array('alias'));
            $aliases_arr = array_merge($aliases_arr, $reserved_aliases);
            
            $result['valid'] = !in_array($alias, $aliases_arr);
            $result['message'] = 'уже есть страница с таким алиасом';
        }
        $this->response->body(json_encode($result));
    }
}