<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_News extends Controller_Admin_Base {
    public function action_index()
    {       
        $renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory('admin/layout');

        $id = $this->request->param('id');
        if(!$id)
        {
            $view = $this->_get_list_view();
            $view->is_admin = true;
            
            $this->response->body($this->request->is_ajax() ?
                                  $renderer->render($view, 'Partials/Admin/News-list') : 
                                  $renderer->render($view));
        }
        else
        {            
            $view = new View_Admin_News($id);
            
            $this->response->body($renderer->render($view));
        }
    }
    
    protected function _get_list_view() {
        $search_criteria_arr = $this->make_search_criterias_arr();

        $pagination = Pagination::factory(array('total_items' => ORM::factory('News')->count_all()));
        
        $lang = $this->request->param('language');
        
        $view = new View_Admin_NewsList($lang, $pagination->offset, $pagination->items_per_page);

        $view->has_items = $pagination->total_items > 0;
        $view->paginator = $pagination->render();
        
        return $view;
    }
    
    protected function make_search_criterias_arr() {
    }
    
    public function action_create()
    {
        if($this->request->method() == Request::GET) {
            $this->response->body(Kostache_Layout::factory('admin/layout')->render(new View_Admin_News()));
        }
        else {
            $data = $this->request->post();

            $news = ORM::factory('News')->values($data, array('title', 'teaser', 'body', 'date'));

            $this->_setBelongsToFields($news, $data);

            $news->create();

            $this->_setHasManyFields($news, $data);
            
            $this->redirect('/admin/news');
        }
    }
    
    public function action_edit()
    {
        $id = $this->request->param('id');
        if (!$id) {
            throw HTTP_Exception::factory(500, 'ID needed!');
        }

        $data = $this->request->post();

        $news = ORM::factory('News', $id);

        if ($news->loaded()) {
            $news->values($data, array('title', 'teaser', 'body', 'date'));

            $this->_setBelongsToFields($news, $data);

            $news->save();

            $this->_setHasManyFields($news, $data);

            $this->redirect('/admin/news');
        } else {
            throw HTTP_Exception::factory(404, 'Item not found!');
        }
    }
    
    public function action_delete()
    {
        $id = $this->request->param('id');
        if (!$id) {
            throw HTTP_Exception::factory(500, 'ID needed!');
        }

        $news = ORM::factory('News', $id);

        if ($news->loaded()) {
            $news->remove('content_items');
            $news->delete();
        } else {
            throw HTTP_Exception::factory(404, 'Item not found!');
        }
    }
    
    public function action_upload() {
        if (isset($_FILES['file']))
        {
            $filename = $this->_save_image($_FILES['file']);
        }
        
        if (!$filename)
        {
            $json_response['errors'] = '';
        }
        else {
            $json_response['filename'] = $filename;
            $json_response['filelink'] = '/content/news/'.$filename;
        }
        
        $this->response->body(json_encode($json_response));
    }
    
    public function action_medialist() {        
        $search_criteria_arr = $_GET;

        $content_types = Arr::get($search_criteria_arr, 'content_types', null);
        if(!is_null($content_types) && strpos($content_types, ','))
        {
            $search_criteria_arr['content_types'] = explode(',', $content_types);
        }
        
        $renderer = Kostache::factory();
        
        $view = new View_ContentItemList(ORM::factory('Media_Object')->where('visible', '=', TRUE)->search($search_criteria_arr), 0, 10);
        
        $this->response->body($renderer->render($view, 'Partials/Admin/Media-list'));
    }
    
    private function _setBelongsToFields($news, $data) {
        if(array_key_exists('tags', $data)) {
            $data['tags'] = explode(',', $data['tags']);
            
            sort($data['tags']);
            $tags_string = join(', ', $data['tags']);

            $tag_set_model = ORM::factory('Tag_Set', array('tag_set_name' => $tags_string));
            if(!$tag_set_model->loaded()) {
                $tag_set_model = ORM::factory('Tag_Set');
                $tag_set_model->tag_set_name = $tags_string;
                $tag_set_model->save();   
                 
                foreach ($data['tags'] as $tag) {
                    $tag_model = ORM::factory('Tag', array('tag' => $tag));

                    if(!$tag_model->loaded()) {
                         $tag_model = ORM::factory('Tag');
                         $tag_model->tag = $tag;
                         $tag_model->save();
                    }

                    $tag_set_model->add('tags', $tag_model);
                }
            }
            $news->tag_set_id = $tag_set_model->pk();
        }
    }
    
    private function _setHasManyFields($news, $data) {
        $news->remove('content_items');
        
        if(array_key_exists('content_items', $data) && !empty($data['content_items'])) {
            $data['content_items'] = explode(',', $data['content_items']);
            
            foreach ($data['content_items'] as $id) {
                $item_model = ORM::factory('Media_Object', $id);

                if($item_model->loaded()) {
                    $news->add('content_items', $item_model);
                }
            }

            DB::update('news_media_objects')->set(array('major' => '1'))
                                            ->where('news_id', '=', $news->pk())
                                            ->and_where('media_id', '=', $data['content_items'][0])
                                            ->execute();
        }
        else
        { // no attached media objects
            DB::update('news_media_objects')->set(array('major' => '1'))
                ->where('news_id', '=', $news->pk())
                ->execute();
        }
    }    
    
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'content/news/';
        
        $filename = strtolower(Text::random('alnum', 20)).'.jpg';
        if (Upload::save($image, $filename, $directory))
        {
            return $filename;
        }
 
        return FALSE;
    }
}