<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Jira extends Controller_Admin_Base {
    public function action_index()
    {       
        $renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory('admin/layout');
        
        $content_types = $this->request->query('content_types');
        $related_to = $this->request->query('related_to');
        $key = $this->request->query('key');
        
        if(!is_null($content_types) && strpos($content_types, ','))
        {
            $content_types = explode(',', $content_types);
        }
        
        if(!is_null($related_to))
        {
           $filtering_item = Model_Jira_Item::factory($related_to)->load();
           $filtering_item->is_collection = $filtering_item->type == 'PUBLISH';
           
           $filter_result = $filtering_item->is_collection ? $filtering_item->children() : $filtering_item->parents();
           $view = new View_Admin_JiraList($filter_result);
           
           $view->filtering_item = $filtering_item;
           
        }
        else if(!is_null($key))
        {
           $view = new View_Admin_JiraList(Model_Jira_Item::factory($key)->load());
        }
        else {
            $view = new View_Admin_JiraList(Model_Jira_Item::all($content_types));
        }
        
        $this->response->body($this->request->is_ajax() ?
                              $renderer->render($view, "Partials/Admin/Jira-list") : 
                              $renderer->render($view));
    }

    public function action_accept_all()
    {
        $classes = Model_Jira_Item::all('audio');

        if($classes != NULL) {
//            throw HTTP_Exception::factory(500, 'Cannot find items in JIRA!');
            foreach ( $classes as $item)   {
                $media_object = $item->push_media_object_to_db();
                $result = $item->set_commited_status();

                $this->action_reindex_words($media_object->pk());

                if($result != TRUE) {
                    $this->response->status($result);
                }
            }
        }

        // next list of items
        $renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory('admin/layout');
        $view = new View_Admin_JiraList(Model_Jira_Item::all());

        $this->response->body($this->request->is_ajax() ?
            $renderer->render($view, "Partials/Admin/Jira-list") :
            $renderer->render($view));
    }


    public function action_accept()
    {
        $key = Arr::get($this->request->post(), 'key', $this->request->query('key'));
        if(!$key) return;
        
        $item = Model_Jira_Item::factory($key);

        if($item == NULL) {
            throw HTTP_Exception::factory(500, 'Cannot find item in JIRA!');
        }

        $media_object = $item->push_media_object_to_db();
        $result = $item->set_commited_status();

        $this->action_reindex_words($media_object->pk());

        if($result != TRUE) {
           $this->response->status($result);
        }
        
        if($this->request->post('images')) {
            $this->push_item_images_to_db($media_object, json_decode($this->request->post('images')));
        }
    }
    
    public function action_accept_self_and_children()
    {
        $key = Arr::get($this->request->post(), 'key', $this->request->query('key'));
        if(!$key) return;
        
        $item = Model_Jira_Item::factory($key);

        $result = $item->set_commited_status();

        if($result != TRUE) {
           $this->response->status($result);
           return;
        }
        
        $media_object = $item->push_media_object_to_db();
        $this->action_reindex_words($media_object->pk());

        if($this->request->post('images')) {
            $this->push_item_images_to_db($media_object, json_decode($this->request->post('images')));
        }
        
        $this->_accept_children($item);        
    }
    
    protected function push_item_images_to_db($media_object, $images_uri_arr) {
        $media_object->img_uri = $images_uri_arr[0];
        $media_object->save();

        if(count($images_uri_arr) == 1) return;

        for ($index = 1; $index < count($images_uri_arr); $index++) {
            $pic_item = ORM::factory('Picture');
            $pic_item->file_uri = $images_uri_arr[$index];
            $pic_item->save();

            $pic_item->add('master_medias', $media_object);
        }
    }
    
    public function action_decline()
    {
        $key = Arr::get($this->request->post(), 'key', $this->request->query('key'));
        if(!$key) return;
        
        $item = Model_Jira_Item::factory($key);

        if($item == NULL) {
            throw HTTP_Exception::factory(500, 'Cannot find item in JIRA!');
        }

        $result = $item->set_rejected_status($this->request->post('comment'));

        if($result != TRUE) {
            $this->response->status($result);
        }
    }
    
    private function _accept_children($item) {
        $related_items = $item->children();
        
        if(count($related_items) == 0) return;
        
        foreach ($related_items as $child) {
            $result = $child->set_commited_status();
                    
            if($result != TRUE) {
                $this->response->status($result);
                break;
            }
            
            $media_object = $child->push_media_object_to_db($item->key);
            $this->action_reindex_words($media_object->pk());
            
            if($child->content_type == 'collection') {
                $this->_accept_children($child);
                
                if(!$media_object->occurrence_date && property_exists($item, 'occurrence_date')) {
                    $media_object->occurrence_date = $item->occurrence_date;
                    $media_object->save();
                }
            }
        }
    }
    
    public function action_update() {
        $jira_key = Arr::get($this->request->query(), 'key', NULL);
        
        $media_id = Arr::get($this->request->query(), 'id', NULL);
                
        if($media_id) {
            $jira_key = ORM::factory('Media_Object', $media_id)->jira_ref;
        }
        
        if(!$jira_key) {
            $this->response->body('No Jira item found!');
            return;
        }
        
        $item = Model_Jira_Item::factory($jira_key);
        $media_object = $item->push_media_object_to_db();
        $this->action_reindex_words($media_object->pk());

        if($item->content_type == 'collection') {
            $this->_accept_children($item); 
        }
        
        $this->response->body('OK');
    }
 } // End
