<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Base extends Controller {

    public function before()
    {
        // If this user doesn't have the admin role, redirect to login
        // Longway. Because of if ($user instanceof Model_User) checking in Kohana_Auth_ORM->logged_in($role = NULL)
        $user = Auth::instance()->get_user();
        $admin_role = ORM::factory('Role', array('name' => 'admin'));

        if ($user == null || !$user->has('roles', $admin_role))
        {
            $this->redirect('/account/logon/?return_url='.urlencode($this->request->uri()));
        }
    }
    
    public function action_index()
    {
        $view = View::factory('pages/reindex_words');
	$this->response->body($view->render());
    }

    public function action_reindex_words($id = NULL)
    {
	// создаем экземпляр класса phpMorphy
        // все функции phpMorphy являются throwable т.е.
        // могут возбуждать исключения типа phpMorphy_Exception (конструктор тоже)
        try
        {
            $morphy = new phpMorphy(DOCROOT.'/vendor/nqxcode/phpmorphy/dicts', 'ru_RU', array('storage' => PHPMORPHY_STORAGE_FILE,));
        }
        catch(phpMorphy_Exception $e)
        {
            die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        }

        $media_objects = $id == NULL ? ORM::factory('Media_Object')->find_all() : array(ORM::factory('Media_Object', $id));

        foreach($media_objects as $media_object) {
            $title = mb_strtoupper(str_ireplace( "ё", "е", $media_object->title), "UTF-8");
            $teaser = mb_strtoupper(str_ireplace( "ё", "е", $media_object->teaser), "UTF-8");
            $text = mb_strtoupper(str_ireplace ("ё", "е", strip_tags($media_object->body)), "UTF-8");

            // Ассоциативный массив начальная форма слова (ключ) - вес слова (значение)
            $search_words = array ();

            // Далее три почти одинаковых цикла, потому что нам необходимо выставить разный вес словам в зависимости от их местонахождения.
            // Словам в заголовке мы задаем вес 3, словам в кратком описании — 2, в полном — 1. При повторе слова увеличиваем его вес.

            preg_match_all ('/([a-zа-яё-]+)/ui', $title, $word_pma);
            $lemmatized_words = $morphy->lemmatize ($word_pma[1]);

            foreach ( $lemmatized_words as $init_word => $lem_word ) {
                //Если не удалось получить начальную форму
                if (!$lem_word) $lem_word[0] = $init_word;

                //Индексируем только слова, длиннее двух символов
                if (mb_strlen ( $lem_word[0], "UTF-8" ) > 2) {
                    if (!isset($search_words[$lem_word[0]]))
                        $search_words[$lem_word[0]] = 0;

                    //Выставляем вес слова
                    $search_words[$lem_word[0]] += 3;
                }

            }

            preg_match_all('/([a-zа-яё]+)/ui', $teaser, $word_pma);
            $lemmatized_words = $morphy->lemmatize($word_pma[1]);

            foreach($lemmatized_words as $init_word => $lem_word) {
                if (!$lem_word)$lem_word[0] = $init_word;

                if (mb_strlen($lem_word [0], "UTF-8") > 2) {
                    if (!isset($search_words[$lem_word[0]]))
                        $search_words[$lem_word[0]] = 0;

                    $search_words[$lem_word[0]] += 2;
                }
            }

            preg_match_all('/([a-zа-яё]+)/ui', $text, $word_pma);
            $lemmatized_words = $morphy->lemmatize($word_pma[1]);

            foreach ($lemmatized_words as $init_word => $lem_word) {
                if (!$lem_word) $lem_word[0] = $init_word;

                if(mb_strlen($lem_word[0], "UTF-8") > 2) {
                    if (!isset($search_words[$lem_word[0]]))
                        $search_words[$lem_word[0]] = 0;

                    $search_words[$lem_word[0]]++;
                }
            }

            $delete_query = DB::delete('search_words')->where('media_id', '=', $media_object->pk());
            $delete_query->execute();

            $insert_query = DB::insert('search_words', array('word', 'media_id', 'weight'));

            if(count($search_words) > 0) {
                foreach ($search_words as $word => $weight) {
                    $insert_query->values(array($word, $media_object->pk(), $weight));
                }

                $insert_query->execute();
            }
        }
    }
 } // End
