<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Etc extends Controller {

    public function action_index()
    {
	$renderer = Kostache_Layout::factory();

        $view = (object) array('page_title' => 'Всего понемногу');

        $this->response->body($renderer->render($view, 'Etc'));
    }
 } // End
