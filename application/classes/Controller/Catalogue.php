<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Catalogue extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();
        
        $search_criteria_arr = $this->make_search_criterias_arr();
        
        $pagination = Pagination::factory(array('total_items' => ORM::factory('Media_Object')->where('visible', '=', TRUE)->search($search_criteria_arr)->count_all()));
        
        $view = new View_Catalogue($search_criteria_arr,
                                   $pagination->offset, $pagination->items_per_page,
                                   Arr::get($search_criteria_arr, 'order_by', 'occurrence_date'), Arr::get($search_criteria_arr, 'order_dir', 'DESC')
                                  );
        $view->has_items = $pagination->total_items > 0;
        $view->paginator = $pagination->render();
        
        if(!$this->request->is_ajax()) {
            $view->page_title = "Каталог материалов";

            $view->all_tags = ORM::factory('Tag')->find_all()->as_array(NULL, "tag");
            $view->all_authors = ORM::factory('Author')->find_all()->as_array(NULL, "name");
            $view->all_venues = ORM::factory('Venue')->find_all()->as_array(NULL, "name");
        }
        
        $this->response->body($this->request->is_ajax() ?
                              $renderer->render($view, "Partials/Search-list") : 
                              $renderer->render($view));
    }
    
    public function action_item_details()
    {
        $id = $this->request->param('id', $this->request->query('id'));
        $type = $this->request->param('type', $this->request->query('type'));
	
        $view_class_name = 'View_' . ucfirst($type);
        $view = new $view_class_name($id);
        
        $this->response->body(Kostache::factory()->render($view, "Partials/Cat-bocol-details"));
    }
    
    protected function make_search_criterias_arr() {
        $search_criteria_arr = $_GET;

        $content_types = Arr::get($search_criteria_arr, 'content_types', null);
        if(!is_null($content_types) && strpos($content_types, ','))
        {
            $search_criteria_arr['content_types'] = explode(',', $content_types);
        }

        $tags = Arr::get($search_criteria_arr, 'tags', null);
        if(!is_null($tags) && strpos($tags, ','))
        {
           $search_criteria_arr['tags'] = explode(',', $tags);
        }
        
        $authors = Arr::get($search_criteria_arr, 'authors', null);
        if(!is_null($authors) && strpos($authors, ','))
        {
           $search_criteria_arr['authors'] = explode(',', $authors);
        }
        
        $venues = Arr::get($search_criteria_arr, 'venues', null);
        if(!is_null($venues) && strpos($venues, ','))
        {
           $search_criteria_arr['venues'] = explode(',', $venues);
        }
        
        $search = Arr::get($search_criteria_arr, 'q', FALSE);
        if ($search !== FALSE AND $search != '')
        {
            $search_criteria_arr['words'] = $this->text_search($search);
            
            $unwords = array();
            preg_match_all('/(\d+\.?)+/ui', $search, $unwords);
            
            if(count($unwords)) {
                $search_criteria_arr['unwords'] = $unwords[0];            
            }
        }
        
        $from_date = Arr::get($search_criteria_arr, 'from_date', null);
        if(!is_null($from_date))
        {
            $is_year_only = preg_match('/^\d+$/', $from_date);
            
            $date = new DateTime();
            if($is_year_only)
            {
                $date->setDate($from_date, 1, 1);
            }
            else {
                $date = DateTime::createFromFormat('d.m.Y', $from_date);
            }
            $search_criteria_arr['from_date'] = $date->format('Y-m-d H:i:s');
        }
        
        $to_date = Arr::get($search_criteria_arr, 'to_date', null);
        if(!is_null($from_date))
        {
            $is_year_only = preg_match('/^\d+$/', $to_date);
            
            $date = new DateTime();
            if($is_year_only)
            {
                $date->setDate($to_date, 12, 31);
            }
            else {
                $date = DateTime::createFromFormat('d.m.Y', $to_date);
            }
            $search_criteria_arr['to_date'] = $date->format('Y-m-d H:i:s');
        }
        
        return $search_criteria_arr;
    }

    protected function text_search($search)
    {
        // создаем экземпляр класса phpMorphy
        // все функции phpMorphy являются throwable т.е.
        // могут возбуждать исключения типа phpMorphy_Exception (конструктор тоже)
        try
        {
            //$morphy = new phpMorphy(APPPATH.'/vendors/phpmorphy/dicts', 'ru_RU', array('storage' => PHPMORPHY_STORAGE_FILE,));
            $morphy = new phpMorphy(DOCROOT.'/vendor/nqxcode/phpmorphy/dicts', 'ru_RU', array('storage' => PHPMORPHY_STORAGE_FILE,));
        }
        catch(phpMorphy_Exception $e)
        {
            die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        }

        $words_pma = array();
        //Разбиваем поисковую фразу на отдельные слова
        preg_match_all('/([a-zа-яё]+)/ui', mb_strtoupper($search, 'UTF-8'), $words_pma);

        $this->text_search_magic_words($words_pma);
        
        //Приводим слова из поисковой фразы к начальной форме, аналогично как это
        //происходило в предыдущем скрипте индексации содержимого
        $words = $morphy->lemmatize ($words_pma[1]);
        $lemmatized_words = array();

        foreach($words as $k => $w)
        {
            if(!$w)
            {
                $w[0] = $k;
            }

            if(mb_strlen($w[0], "UTF-8") > 2)
            {
                array_push($lemmatized_words, $w[0]);
            }
        }

        return $lemmatized_words;
    }
    
    protected function text_search_magic_words($words_pma) {
        if(in_array("БГ", $words_pma)) {
                $words_pma[] = "Бхагавад-гита";
        }
        
        if(in_array("ШБ", $words_pma)) {
                $words_pma[] = "Шримад-бхагаватам";
        }
        
        if(in_array("ЧЧ", $words_pma)) {
                $words_pma[] = "Чайтанья-чаритамрита";
        }
        
        if(in_array("БВГМ", $words_pma) || in_array("БВГ", $words_pma)) {
            array_push($words_pma, "Бхакти", "Вигьяна", "Госвами");
        }
    }
 } // End
