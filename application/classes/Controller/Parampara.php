<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Parampara extends Controller {

    public function action_index()
    {
	$renderer = Kostache_Layout::factory();

        $view = (object) array("page_title" => "Духовные учители");

        $this->response->body($renderer->render($view, "partials/parampara_links"));
    }

    public function action_bvgm()
    {
        $id = $this->request->param('id');

	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $view = (object) array("page_title" => "Духовные учители - Бхакти Вигьяна Госвами");

        $this->response->body($renderer->render($view, "parampara/bvgm".($id ? "-".$id : "")));
    }

    public function action_rsm()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $view = (object) array("page_title" => "Духовные учители - Радханатх Свами");

        $this->response->body($renderer->render($view, "parampara/rsm"));
    }

    public function action_prabhupada()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $view = (object) array("page_title" => "Духовные учители - Шрила Прабхупада");

        $this->response->body($renderer->render($view, "parampara/prabhupada"));
    }
 } // End
