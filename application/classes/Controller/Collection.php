<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Collection extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $lang = $this->request->param('language');
        $id = $this->request->param('id');
        if(!$id)
        {
            $view = new View_CollectionList($lang);
            $view->page_title = 'Диски';
            $view->model = $view->model->order_by(array('date', 'id'), 'desc')->limit(10);
        }
        else
        {
            $view = new View_Collection($id);
            $view->page_title = 'Диск '.$view->title;
        }

        $this->response->body($renderer->render($view));
    }
    
    public function action_download()
    {
        $id = $this->request->param('id');
        $collection_model = new Model_Collection($id);
        $collection_item = $collection_model->find();
        
        $tempZip = tempnam(DOCROOT.'/content/collections/', $collection_item->pk());
        
        $zip = new ZipArchive();
        if ($zip->open($tempZip) === TRUE) {
            while (ob_get_level() > 0) {
                ob_end_clean();
            }

            $this->zipCollectionItems($zip, $collection_item);
            
            $zip->close();
            
            Kohana::$log->add(Kohana_Log::INFO, 'Zip file '.$tempZip.' for collection id='.$collection_item->pk().' succesfully created!');
            
            $this->response->send_file($tempZip, $collection_item->title.'.zip', array('delete' => TRUE));
        } else {
            Kohana::$log->add(Kohana_Log::ERROR, 'Failed to create zip file '.$tempZip.' for collection id='.$collection_item->pk().'!');
        }
    }
    
    private function zipCollectionItems($zip, $collection, $basepath = '') {
        foreach ($collection->linked_medias->find_all() as $item) {
            if($item->type == 'collection') {
                $zip->addEmptyDir($item->title);
                $child_basepath = $basepath + $item->title + '/';
                
                $zip->addFile($item->img_uri, $child_basepath.basename($item->img_uri));
                
                zipCollectionItems($zip, $item, $child_basepath);
            }

            if($item->file_uri) {
                $zip->addFile($item->file_uri, $basepath.basename($item->file_uri));
            }
        }
    }
    
    public function action_upload()
    {
        if (isset($_FILES['file']))
        {
            $filename = $this->_save_image($_FILES['file']);
        }
        
        if (!$filename)
        {
            $json_response['errors'] = '';
        }
        else {
            $json_response['filename'] = $filename;
            $json_response['url'] = '/content/img/'.$filename;
        }
        
        $this->response->body(json_encode($json_response));
    }  
    
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'content/img/';
        
        $filename = strtolower(Text::random('alnum', 20)).'.jpg';
        if (Upload::save($image, $filename, $directory))
        {
            return $filename;
        }
 
        return FALSE;
    }
 }
