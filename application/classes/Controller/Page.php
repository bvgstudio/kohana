<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page extends Controller {

    public function action_index()
    {
	$renderer = $this->request->is_ajax() ? Kostache::factory() : Kostache_Layout::factory();

        $page_alias = $this->request->param('alias');
        $view = new View_Page($page_alias);

        $this->response->body($renderer->render($view));
    }
 } // End
