<?php





header('Content-type: application/rss+xml');
ini_set('default_charset', 'utf-8');


$feed_encoding          = "utf-8";
$categories             = array( "audio", "video", "articles");


//$itunes_category[0] = "Arts"; // iTunes categories (mainCategory:subcategory)
//$itunes_category[1] = "";
//$itunes_category[2] = "";

$feed_language          = "ru";
$podcast_title          = "Е.С. Бхакти Вигьяна Госвами Махарадж";
$url                    = "http://www.goswami.ru";
$podcast_description    = "Новые лекции Е.С. Бхакти Вигьяны Госвами Махараджа";
$copyright              = "Copyrights (с) goswami.ru";
$author_name            = "Е.С. Бхакти Вигьяна Госвами Махарадж";
$author_email           = "admin@goswami.ru";
$explicit_podcast       = "no";
$duration               = "1:05";
$single_file            = "";
$text_keywordspg        = "Кришна, Шримад Бхагаватам, Бхагават Гита, Госвами Махарадж, лекции";




if (isset($_GET['category']) AND $_GET['category'] != NULL)
{
    $cat = $_GET['category'];
    // generate xml from database where pointer links to media files but filter by podcast flag
    // <rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">

/*
        <image>
        <url>$url/images/itunes_image.jpg</url>
        <title>$podcast_title</title>
        <link>$url</link>
        </image>

*/

    $head_feed ="<?xml version=\"1.0\" encoding=\"$feed_encoding\"?>
        <!-- generator=\"http://www.goswami.ru\" -->
        <rss xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" xml:lang=\"$feed_language\" version=\"2.0\">
        <channel>
        <title>$podcast_title</title>
        <generator>Студия Е.С. Бхакти Вигьяны Госвами Махараджа - http://www.goswami.ru</generator>
        <lastBuildDate>".date("r")."</lastBuildDate>
        <link>$url</link>
        <language>$feed_language</language>
        <copyright>$copyright</copyright>
        <itunes:subtitle>$podcast_description</itunes:subtitle>
        <itunes:author>$author_name</itunes:author>
        <itunes:summary>$podcast_description</itunes:summary>
        <description>$podcast_description</description>
        <itunes:owner>
        <itunes:name>$author_name</itunes:name>
        <itunes:email>$author_email</itunes:email>
        </itunes:owner>
        <itunes:image href=\"$url/images/bvgm.jpg\" />
        <itunes:explicit>$explicit_podcast</itunes:explicit>
        <itunes:category text=\"Religion &amp; Spirituality\">
        </itunes:category>";


//      foreach ($categories as &$cat)
//      {
//          $head_feed.= "<itunes:category text=\"$cat\">";
//          $head_feed.= "</itunes:category>";
//      }
//  var $db_host = "localhost";
//  var $db_user = "p_goswami";
//  var $db_password = "8hVPSAwf";
//  var $db_base = "goswami";
//                                                       

$conn = pg_pconnect("host=localhost port=5432 dbname=bvg user=goswami.ru options='--client_encoding=UTF8'") or die(pg_last_error());
if (!$conn) {
  echo "An error occurred.\n";
  exit;
}

//    mysql_connect("localhost", "goswami_ru", "8hVPSAwf") or die("Could not connect: " . mysql_error());
//    mysql_select_db("goswami_ru");

    switch ( $cat )
    {
    default:
    case "audio":
        GenerateAudioItems($single_file);
        break;
    case "video":
        break;
    case "text":
        break;
    }

//    $tail_feed ="\n<atom:link href=\"http://goswami.ru/rss.php?cat=$cat\" rel=self type=application/rss+xml />\n</channel></rss>";
    $tail_feed ="\n</channel></rss>";


    #
    #### PRINT FEED
    print  $head_feed.$single_file.$tail_feed;
    exit;
}


########
## Depurate feed Content from non accepted characters (according also to iTunes specifications)

function depurateContent($content)
{
    $content = stripslashes($content);              
    $content = str_replace('<', '&lt;', $content);
    $content = str_replace('>', '&gt;', $content);
    $content = str_replace('& ', '&amp; ', $content);
    $content = str_replace('вЂ™', '&apos;', $content);
    $content = str_replace('"', '&quot;', $content);
    $content = str_replace('В©', '&#xA9;', $content);
    $content = str_replace('&copy;', '&#xA9;', $content);
    $content = str_replace('в„—', '&#x2117;', $content);
    $content = str_replace('в„ў', '&#x2122;', $content);
    return $content;
}


function urlForItunes($url)
{
/*
Array
(
    [0] => Array
        (
            [0] => http://bvgm.org/recent/AnantaShesha.mp3
            [1] => 0
        )

    [1] => Array
        (
            [0] => http://
            [1] => 0
        )

    [2] => Array
        (
            [0] => bvgm.org/recent/
            [1] => 7
        )

    [3] => Array
        (
            [0] => AnantaShesha.mp3
            [1] => 23
        )
)

*/

    preg_match("/(http\:\/\/)?(.+\/)*(.+)?/", $url, $url_parts, PREG_OFFSET_CAPTURE);
    return $url_parts[1][0].$url_parts[2][0].rawurlencode(urldecode($url_parts[3][0]));
}

function GenerateAudioItems(&$items) 
{

    global $conn, $author_email, $author_name, $duration, $explicit_podcast, $feed_encoding, $url, $text_keywordspg;
// (`id`, `chapter_id`, `view_count`, `user_id`, `allow`, `name`, `date`, `city`, `format`, `track`, `size`, `ext_link`) VALUES 
// ('94', '487', '4', '33', '0', 'Ананта Шеша', '2009-07-12 22:19:00', 'Москва', 'mp3', '', '26214400', 'http://bvgm.org/recent/AnantaShesha.mp3'),


    $result = pg_query($conn, " SELECT m.title, m.occurrence_date, v.name, m.size, m.duration, m.file_uri, m.body FROM media m left join venues v on m.venue_id = v.id WHERE m.visible = TRUE AND m.podcast = TRUE ") or die(pg_last_error());

//    $result = mysql_query("SELECT name, date, city, size, ext_link, description FROM user_lecture where allow=1 order by date desc LIMIT 30");




    while($track_info = pg_fetch_array($result))
    {
//        $name = iconv ("cp1251", $feed_encoding, $track_info["name"]);
//        $city = iconv ("cp1251", $feed_encoding, $track_info["city"]);
//        $link = iconv ("cp1251", $feed_encoding, $track_info["ext_link"]);



        $items.="<item>
            <title>".$track_info["title"]."</title>
            <itunes:subtitle>".$track_info["name"]."</itunes:subtitle>
            <itunes:summary><![CDATA[ ".$track_info["title"]." ]]></itunes:summary>
            <description>".$track_info["body"]."</description>
            <link>".urlForItunes($track_info["file_uri"])."</link>
            <enclosure url=\"".urlForItunes($track_info["file_uri"])."\" length=\"".$track_info["size"]."\" type=\"audio/mpeg\"/>
            <guid isPermaLink=\"false\">".sha1($track_info["file_uri"])."</guid>
            <itunes:duration>".$track_info["duration"]."</itunes:duration>
            <author>$author_email ($author_name)</author><itunes:author>$author_name</itunes:author>
            <itunes:keywords>$text_keywordspg</itunes:keywords>
            <itunes:explicit>$explicit_podcast</itunes:explicit>
            <pubDate>".date( "r" , strtotime ($track_info["occurrence_date"]) )."</pubDate></item>";
    }
}


?>
